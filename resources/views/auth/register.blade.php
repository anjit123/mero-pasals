@extends('frontEnd.layouts.master')
@section('title', 'Register Page')

@section('content')


  <div class="container pt-5">
 
  
  <div class="box">

  <div class="box-header">
  <center><h1>Register New Customer</h1></center>
  </div>

  <div class="row">

  <div class="col-md-6">

  <img src="/frontEnd/images/register.png" class="w-100 ml-5 mt-5" alt="">

<br><br>
  <div class="container">
    <!-- <a href="{{url('/login/facebook')}}" class="btn btn-lg btn-social btn-facebook">
    <i class="fa fa-facebook fa-fw"></i> Sign up with Facebook
    </a> -->
</div>
<div class="container">
    <a href="{{url('auth/google')}}" class="btn btn-lg btn-social btn-facebook">
    <i class="fa fa-google"></i> Sign up with Google
    </a>
</div>
  </div>
  <div class="col-md-6">
    <form action="{{ route('register') }}" method="post" enctype="multipart/form-data"> <!-- form starts -->
  
    <input type="hidden" name="_token" value="{{csrf_token()}}">

    <div class="form-group" ><!-- form-group Starts -->

        <label for="name" class="col-form-label"> </i>{{ __(' Customer Name') }}</label>

        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}">
            @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif

    </div><!-- form-group Ends -->

    <div class="form-group" ><!-- form-group Starts -->

        <label for="email" class="col-form-label"> </i>{{ __(' Customer Email') }}</label>

        <input  id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}">
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif

    </div><!-- form-group Ends -->

    

    <div class="form-group" ><!-- form-group Starts -->

        <label for="password" class="col-form-label"><i class="fa fa-password"> </i>{{ __(' Password') }}</label>

        
<div class="input-group"><!-- input-group Starts -->

<span class="input-group-addon"><!-- input-group-addon Starts -->

<i class="fa fa-check tick1"> </i>

<i class="fa fa-times cross1"> </i>

</span><!-- input-group-addon Ends -->

<input id ="pass" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}">
      
      @if ($errors->has('password'))
          <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('password') }}</strong>
          </span>
      @endif


<span class="input-group-addon"><!-- input-group-addon Starts -->

<div id="meter_wrapper"><!-- meter_wrapper Starts -->

<span id="pass_type"> </span>

<div id="meter"> </div>

</div><!-- meter_wrapper Ends -->

</span><!-- input-group-addon Ends -->

</div><!-- input-group Ends -->


       
    </div><!-- form-group Ends -->

    <div class="form-group">
        <label for="password-confirm" class="col-form-label">{{ __('Confirm Password') }}</label>

        <div class="input-group"><!-- input-group Starts -->

            <span class="input-group-addon"><!-- input-group-addon Starts -->

            <i class="fa fa-check tick2"> </i>

            <i class="fa fa-times cross2"> </i>

            </span><!-- input-group-addon Ends -->

            <input id="con_pass" type="password" class="form-control confirm" name="password_confirmation">
            

            </div><!-- input-group Ends -->
           
    </div>
    
    <div class="form-group" ><!-- form-group Starts -->

        <label for="country" class="col-form-label"><i class="fa fa-country"> </i>{{ __(' Customer Country') }}</label>

        <input id="country" type="text" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" value="{{ old('country') }}">
            @if ($errors->has('country'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('country') }}</strong>
                </span>
            @endif

    </div><!-- form-group Ends -->

    <div class="form-group" ><!-- form-group Starts -->

        <label for="city" class="col-form-label"><i class="fa fa-city"> </i>{{ __(' Customer City') }}</label>

        <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') }}">
            @if ($errors->has('city'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('city') }}</strong>
                </span>
            @endif

    </div><!-- form-group Ends -->

    <div class="form-group" ><!-- form-group Starts -->

        <label for="address" class="col-form-label"><i class="fa fa-address"> </i>{{ __(' Customer Address') }}</label>

        <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}">
            @if ($errors->has('address'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('address') }}</strong>
                </span>
            @endif

    </div><!-- form-group Ends -->

    <div class="form-group" ><!-- form-group Starts -->

        <label for="contact" class="col-form-label"><i class="fa fa-contact"> </i>{{ __(' Customer Contact') }}</label>
        
        <input id="contact" type="text" class="form-control{{ $errors->has('contact') ? ' is-invalid' : '' }}" name="contact" value="{{ old('contact') }}">
            @if ($errors->has('contact'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('contact') }}</strong>
                </span>
            @endif

    </div><!-- form-group Ends -->

    <div class="form-group">
         <label for="image" class="col-form-label">Customer Image</label>
            <input type="file" name="image" id="image" class="form-control-file{{ $errors->has('contact') ? ' is-invalid' : '' }}">
                 @error('image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
    
    </div>
                              
    <div class="form-group">
            <div class="col-md-5 offset-md-4">
                <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="fa fa-sign-in"></i>
                    <span>{{ __('Register') }}</span>
                </button>
            </div>
            <br>
    </div> 

    
    </form>
    </div>

    </div>
    
    </div>
    </div>
    <script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
 
<script>

$(document).ready(function(){

$('.tick1').hide();
$('.cross1').hide();

$('.tick2').hide();
$('.cross2').hide();


$('.confirm').focusout(function(){

var password = $('#pass').val();

var confirmPassword = $('#con_pass').val();

if(password == confirmPassword){

$('.tick1').show();
$('.cross1').hide();

$('.tick2').show();
$('.cross2').hide();



}
else{

$('.tick1').hide();
$('.cross1').show();

$('.tick2').hide();
$('.cross2').show();


}


});


});

</script>

<script>

$(document).ready(function(){

$("#pass").keyup(function(){

check_pass();

});

});

function check_pass() {
 var val=document.getElementById("pass").value;
 var meter=document.getElementById("meter");
 var no=0;
 if(val!="")
 {
// If the password length is less than or equal to 6
if(val.length<=6)no=1;

 // If the password length is greater than 6 and contain any lowercase alphabet or any number or any special character
  if(val.length>6 && (val.match(/[a-z]/) || val.match(/\d+/) || val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)))no=2;

  // If the password length is greater than 6 and contain alphabet,number,special character respectively
  if(val.length>6 && ((val.match(/[a-z]/) && val.match(/\d+/)) || (val.match(/\d+/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) || (val.match(/[a-z]/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/))))no=3;

  // If the password length is greater than 6 and must contain alphabets,numbers and special characters
  if(val.length>6 && val.match(/[a-z]/) && val.match(/\d+/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/))no=4;

  if(no==1)
  {
   $("#meter").animate({width:'50px'},300);
   meter.style.backgroundColor="red";
   document.getElementById("pass_type").innerHTML="Very Weak";
  }

  if(no==2)
  {
   $("#meter").animate({width:'100px'},300);
   meter.style.backgroundColor="#F5BCA9";
   document.getElementById("pass_type").innerHTML="Weak";
  }

  if(no==3)
  {
   $("#meter").animate({width:'150px'},300);
   meter.style.backgroundColor="#FF8000";
   document.getElementById("pass_type").innerHTML="Good";
  }

  if(no==4)
  {
   $("#meter").animate({width:'200px'},300);
   meter.style.backgroundColor="#00FF40";
   document.getElementById("pass_type").innerHTML="Strong";
  }
 }

 else
 {
  meter.style.backgroundColor="";
  document.getElementById("pass_type").innerHTML="";
 }
}

</script>

@endsection 