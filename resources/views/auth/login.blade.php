@extends('layouts.app')

@section('content')

<div class="bd">

<div class="container" ><!-- container Starts -->

<form class="form-login" action="{{ route('login') }}" method="post" ><!-- form-login Starts -->
@csrf 

<h2 class="form-login-heading" >Admin Login</h2>

<!-- <input type="text" class="form-control" name="admin_email" placeholder="Email Address" required > -->

<!-- <input type="password" class="form-control" name="admin_pass" placeholder="Password" required > -->
<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email Address" required autocomplete="email" autofocus>

@error('email')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror
<br>
<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">

@error('password')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror

<!-- <div class="form-check">
        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
 </div> <br> -->
                            
                                <button type="submit" class="btn btn-primary btn">
                                    {{ __('Login') }}
                                </button>

                                <!-- @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif -->
                            
                        


</form><!-- form-login Ends -->

</div><!-- container Ends -->

</div>

@endsection
