
@extends('backEnd.layouts.master')
@section('title','Reviews Page')
@section('content')

<div class="container">
<br><br><br><br><br>
<?php
      function detect_sentiment($string){
      $string = urlencode($string);
      $api_key = "4194cf28398f7f0e26dbdc4dc61cb5";
      $url = 'https://api.paysify.com/sentiment?api_key='.$api_key.'&string='.$string.'';
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $result = curl_exec($ch);
      $response = json_decode($result,true);
      curl_close($ch);
      return $response;
      }
	  
      $string = "{$sentiment->message}";
      $data = detect_sentiment($string);
    //   echo "<pre>";
    //   print_r($data);
     
    echo $data['data']['string'];
    echo "<br>";
    echo "Result    := ";
    echo $data['data']['state'];
    echo "<br>";
    echo "Negative";
    echo $data['data']['scores']['neg'];
    echo "<br>";
    echo "Positive";
    echo $data['data']['scores']['pos'];
    echo "<br>";
    echo "Neutral";
    echo $data['data']['scores']['neu'];
    echo "<br>";
    echo "Compound";

    echo $data['data']['scores']['compound'];
?>
</div>
@endsection