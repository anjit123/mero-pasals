@extends('backEnd.layouts.master')
@section('title','Reviews Page')
@section('content')


<div class="container">
<div class="row"><!-- 1 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<ol class="breadcrumb"><!-- breadcrumb Starts -->

<li class="active">

<i class="fa fa-dashboard"></i> Dashboard / View Reviews

</li>

</ol><!-- breadcrumb Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 1 row Ends -->

<div class="row"><!-- 2 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<div class="panel panel-default"><!-- panel panel-default Starts -->

<div class="panel-heading"><!-- panel-heading Starts -->

<h3 class="panel-title">

<i class="fa fa-money fa-fw"></i> View Reviews

</h3>

</div><!-- panel-heading Ends -->
@if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                <strong>Well done!</strong> {{Session::get('message')}}
            </div>
        @endif
<div class="panel-body"><!-- panel-body Starts -->

<div class="table-responsive"><!-- table-responsive Starts --->

<table class="table table-bordered table-hover table-striped"><!-- table table-bordered table-hover table-striped Starts -->

<thead><!-- thead Starts -->

<tr>

<th>Id:</th>
<th>Product ID:</th>
<th>Customer Name:</th>
<th>Customer Email:</th>
<th>Customer Reviews:</th>
<th>Rating:</th>
<th>Sentiment:</th>
<th>Action:</th>

</tr>

</thead><!-- thead Ends -->

<tbody><!-- tbody Starts -->

@foreach($reviews as $review)

<tr>

<td>{{$review->id}}</td>
<td>{{$review->product_id}}</td>
<td>{{$review->name}}</td>
<td>{{$review->email}}</td>
<td>{{$review->message}}</td>
<td>{{$review->rating}}</td>
<td>
<a href="{{route('customer-reviews.show',$review->id)}}"> Check Sentiment </a>


</td>
<td>

<a href="javascript:" rel="{{$review->id}}" rel1="delete-customer-reviews" class="btn-mini deleteRecord pull-left"> 
 <i class="fa fa-trash-o" > </i> Delete</a>

</td>



</tr>

@endforeach



</div>


@endsection
@section('jsblock')
<script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/jquery.ui.custom.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.uniform.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/matrix.js')}}"></script>
    <script src="{{asset('js/matrix.tables.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script>
        $(".deleteRecord").click(function () {
           var id=$(this).attr('rel');
           var deleteFunction=$(this).attr('rel1');
           swal({
               title:'Are you sure?',
               text:"You won't be able to revert this!",
               type:'warning',
               showCancelButton:true,
               confirmButtonColor:'#3085d6',
               cancelButtonColor:'#d33',
               confirmButtonText:'Yes, delete it!',
               cancelButtonText:'No, cancel!',
               confirmButtonClass:'btn btn-success',
               cancelButtonClass:'btn btn-danger',
               buttonsStyling:false,
               reverseButtons:true
           },function () {
              window.location.href="/admin/"+deleteFunction+"/"+id;
           });
        });
    </script>
@endsection