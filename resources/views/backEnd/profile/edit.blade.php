@extends('backEnd.layouts.master')
@section('title','Edit User')
@section('content')

<div class="container">
<div class="row" ><!-- 1  row Starts -->

<div class="col-lg-12" ><!-- col-lg-12 Starts -->

<ol class="breadcrumb" ><!-- breadcrumb Starts -->

<li class="active" >

<i class="fa fa-dashboard" ></i> Dashboard / Edit Profile

</li>



</ol><!-- breadcrumb Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 1  row Ends -->

<div class="row" ><!-- 2 row Starts -->

<div class="col-lg-12" ><!-- col-lg-12 Starts -->

<div class="panel panel-default" ><!-- panel panel-default Starts -->

<div class="panel-heading" ><!-- panel-heading Starts -->

<h3 class="panel-title" >

<i class="fa fa-money fa-fw" ></i> Edit Profile

</h3>


</div><!-- panel-heading Ends -->


<div class="panel-body"><!-- panel-body Starts -->

<form class="form-horizontal" action="{{ route('createuser.update',$edit_user->id) }}" method="post" enctype="multipart/form-data"><!-- form-horizontal Starts -->
@csrf 

{{method_field("PUT")}}

<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label">User Name: </label>

<div class="col-md-6"><!-- col-md-6 Starts -->

<input type="text" name="name" class="form-control" required value="{{$edit_user->name}}">

</div><!-- col-md-6 Ends -->

</div><!-- form-group Ends -->


<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label">{{ __(' Customer Email:') }} </label>

<div class="col-md-6"><!-- col-md-6 Starts -->

<input type="text" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{$edit_user->email}}"  required>
@error('email')
        <span class="invalid-feedback" role="alert" style="color:red">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
</div><!-- col-md-6 Ends -->

</div><!-- form-group Ends -->


<!-- form-group Starts -->
<!-- <div class="form-group">

<label class="col-md-3 control-label">User Password: </label>

<div class="col-md-6"> -->
<!-- col-md-6 Starts -->

<!-- <input type="password" name="password" class="form-control" required> -->

<!-- </div> -->
<!-- col-md-6 Ends -->

<!-- </div> -->
<!-- form-group Ends -->

<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label">User Country: </label>

<div class="col-md-6"><!-- col-md-6 Starts -->

<input type="text" name="country" class="form-control" value="{{$edit_user->country}}" required>

</div><!-- col-md-6 Ends -->

</div><!-- form-group Ends -->

<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label">User City: </label>

<div class="col-md-6"><!-- col-md-6 Starts -->

<input type="text" name="city" class="form-control" value="{{$edit_user->city}}" required>

</div><!-- col-md-6 Ends -->

</div><!-- form-group Ends -->

<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label">User Address: </label>

<div class="col-md-6"><!-- col-md-6 Starts -->

<input type="text" name="address" class="form-control" value="{{$edit_user->address}}" required>

</div><!-- col-md-6 Ends -->

</div><!-- form-group Ends -->

<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label">User Contact: </label>

<div class="col-md-6"><!-- col-md-6 Starts -->

<input type="text" name="contact" class="form-control" value="{{$edit_user->contact}}" required>

</div><!-- col-md-6 Ends -->

</div><!-- form-group Ends -->

<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label">User Image: </label>

<div class="col-md-6"><!-- col-md-6 Starts -->

<input type="file" name="image" class="form-control" required>
<img src='/storage/{{ $edit_user->image }}' width="100" height="100" class="img-responsive" >

</div><!-- col-md-6 Ends -->

</div><!-- form-group Ends -->

<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label">User Job: </label>

<div class="col-md-6"><!-- col-md-6 Starts -->

<input type="text" name="user_job" class="form-control" value="{{$edit_user->user_job}}" required>

</div><!-- col-md-6 Ends -->

</div><!-- form-group Ends -->



<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label">User About: </label>

<div class="col-md-6"><!-- col-md-6 Starts -->

<textarea name="about_user" class="form-control" rows="3"> {{$edit_user->about_user}} </textarea>

</div><!-- col-md-6 Ends -->

</div><!-- form-group Ends -->



<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label"></label>

<div class="col-md-6"><!-- col-md-6 Starts -->

    <input type="submit" name="submit" value="Update User" class="btn btn-primary form-control">

</div><!-- col-md-6 Ends -->

</div><!-- form-group Ends -->



</form><!-- form-horizontal Ends -->

</div><!-- panel-body Ends -->

</div><!-- panel panel-default Ends -->

</div><!-- col-lg-12 Ends -->


</div><!-- 2 row Ends -->
</div>
@endsection
