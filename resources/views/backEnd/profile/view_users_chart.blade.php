

@extends('backEnd.layouts.master')
@section('title','View User chart')
@section('content')

<br><br><br>



<div class="container">

<?php
 
$dataPoints = array(
	array("y" => $current_month_users, "label" => "Current Month"),
	array("y" => $last_month_users, "label" => "Last Month"),
	array("y" => $last_to_last_month_users, "label" => "Last to last Month"),
	
);
 
?>

<script>
window.onload = function () {
 
var chart = new CanvasJS.Chart("chartContainer", {
	title: {
		text: "Users Reporting"
	},
	axisY: {
		title: "Number of Users"
	},
	data: [{
		type: "line",
		dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
	}]
});
chart.render();
 
}
</script>

<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

</div>

@endsection