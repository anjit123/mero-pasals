@extends('backEnd.layouts.master')
@section('title','View Boxes Sections')
@section('content')

<div class="container">


<div class="row"><!-- 1 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<ol class="breadcrumb"><!-- breadcrumb Starts -->

<li class="active" >

<i class="fa fa-dashboard"></i> Dashboard / View Boxes

</li>

</ol><!-- breadcrumb Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 1 row Ends -->


<div class="row"><!-- 2 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<div class="panel panel-default"><!-- panel panel-default Starts --> 

<div class="panel-heading"><!-- panel-heading Starts -->

<h3 class="panel-title">

<i class="fa fa-money fa-fw"></i> View Boxes

</h3>

</div><!-- panel-heading Ends -->
@if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                <strong>Slide</strong> {{Session::get('message')}}
            </div>
 @endif



<div class="panel-body"><!-- panel-body Starts -->

@foreach($box_sections as $box)
<div class="col-lg-4 col-md-4"><!-- col-lg-4 col-md-4 Starts -->


<div class="panel panel-primary"><!-- panel panel-primary Starts -->


<div class="panel-heading"><!-- panel-heading Starts -->


<h3 class="panel-title" align="center"><!-- panel-title Starts -->

{{ $box->box_title }}

</h3><!-- panel-title Ends -->

</div><!-- panel-heading Ends -->

<div class="panel-body"><!-- panel-body Starts -->

{{ $box->box_desc }}

</div><!-- panel-body Ends -->

<div class="panel-footer"><!-- panel-footer Starts -->



<a href="javascript:" rel="{{$box->id}}" rel1="delete-box" class="btn-mini deleteRecord pull-left"> 
 <i class="fa fa-trash-o" > </i> Delete</a>

 <a href="{{route('box.edit',$box->id)}}" class="pull-right" >

<i class="fa fa-pencil" ></i> Edit

</a>

<div class="clearfix"></div>

</div><!-- panel-footer Ends -->

</div><!-- panel panel-primary Ends -->

</div><!-- col-lg-4 col-md-4 Ends -->
@endforeach



</div><!-- panel-body Ends -->

</div><!-- panel panel-default Ends --> 

</div><!-- col-lg-12 Ends -->

</div><!-- 2 row Ends -->





</div>



@endsection 
@section('jsblock')
<script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/jquery.ui.custom.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.uniform.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/matrix.js')}}"></script>
    <script src="{{asset('js/matrix.tables.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script>
        $(".deleteRecord").click(function () {
           var id=$(this).attr('rel');
           var deleteFunction=$(this).attr('rel1');
           swal({
               title:'Are you sure?',
               text:"You won't be able to revert this!",
               type:'warning',
               showCancelButton:true,
               confirmButtonColor:'#3085d6',
               cancelButtonColor:'#d33',
               confirmButtonText:'Yes, delete it!',
               cancelButtonText:'No, cancel!',
               confirmButtonClass:'btn btn-success',
               cancelButtonClass:'btn btn-danger',
               buttonsStyling:false,
               reverseButtons:true
           },function () {
              window.location.href="/admin/"+deleteFunction+"/"+id;
           });
        });
    </script>
@endsection