@extends('backEnd.layouts.master')
@section('title','View Product Bundle Relations')
@section('content')

<div class="container">
    
<div class="row"><!-- 1 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<ol class="breadcrumb"><!-- breadcrumb Starts --->

<li class="active">

<i class="fa fa-dashboard"></i> Dashboard / View Relations

</li>

</ol><!-- breadcrumb Ends --->

</div><!-- col-lg-12 Ends -->

</div><!-- 1 row Ends -->


<div class="row"><!-- 2 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<div class="panel panel-default"><!-- panel panel-default Starts -->

<div class="panel-heading"><!-- panel-heading Starts -->

<h3 class="panel-title">

<i class="fa fa-money fa-fw"></i>  <a href="{{route('relation.create')}}">Create Relations</a> / View Relations  

</h3>

</div><!-- panel-heading Ends -->
@if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                <strong>Well done!</strong> {{Session::get('message')}}
            </div>
        @endif
<div class="panel-body"><!-- panel-body Starts -->

<div class="table-responsive"><!-- table-responsive Starts -->


<table class="table table-bordered table-hover table-striped"><!-- table table-bordered table-hover table-striped Starts -->


<thead><!-- thead Starts -->

<tr>

<th>Relation Id:</th>

<th>Relation Title:</th>

<th>Relation Product:</th>

<th>Product Bundle:</th>
<th>Relation Bundle:</th>

<th>Delete Relation:</th>

<th>Edit Relation:</th>


</tr>

</thead><!-- thead Ends -->

<tbody><!-- tbody Starts -->

@foreach($p_b_rels as $p_b_rel)

<tr>

<td> {{$p_b_rel->id}}</td>

<td> {{$p_b_rel->rel_title}} </td>

@foreach($products as $product)
<td> {{$product->product_title}} </td>
@endforeach

@foreach($bundles as $bundle)
<td> {{$bundle->product_title}} </td>
@endforeach
<td>

<a href="javascript:" rel="{{$p_b_rel->id}}" rel1="delete-relation" class="btn-mini deleteRecord">  <i class="fa fa-trash-o" > </i> <strong>Delete</strong> </a>


</td>

<td>

<a href="{{route('relation.edit',$p_b_rel->id)}}"><i class="fa fa-pencil" > </i> <strong>Edit</strong>  </a>


</td>



</tr>

@endforeach
</tbody><!-- tbody Ends -->

</table><!-- table table-bordered table-hover table-striped Ends -->

</div><!-- table-responsive Ends -->

</div><!-- panel-body Ends -->

</div><!-- panel panel-default Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 2 row Ends -->




</div>

@endsection

@section('jsblock')
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/jquery.ui.custom.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.uniform.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/matrix.js')}}"></script>
    <script src="{{asset('js/matrix.tables.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script>
        $(".deleteRecord").click(function () {
           var id=$(this).attr('rel');
           var deleteFunction=$(this).attr('rel1');
           swal({
               title:'Are you sure?',
               text:"You won't be able to revert this!",
               type:'warning',
               showCancelButton:true,
               confirmButtonColor:'#3085d6',
               cancelButtonColor:'#d33',
               confirmButtonText:'Yes, delete it!',
               cancelButtonText:'No, cancel!',
               confirmButtonClass:'btn btn-success',
               cancelButtonClass:'btn btn-danger',
               buttonsStyling:false,
               reverseButtons:true
           },function () {
              window.location.href="/admin/"+deleteFunction+"/"+id;
           });
        });
    </script>
@endsection
