
@extends('backEnd.layouts.master')
@section('title','Insert Product Relation')
@section('content')


<div class="container">

<div class="row"><!-- 1 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<ol class="breadcrumb"><!-- breadcrumb Starts -->

<li class="active">

<i class="fa fa-dashboard"></i> Dashboard / Insert Relation

</li>

</ol><!-- breadcrumb Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 1 row Ends -->


<div class="row"><!-- 2 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<div class="panel panel-default"><!-- panel panel-default Starts -->

<div class="panel-heading"><!-- panel-heading Starts -->

<h3 class="panel-title"><!-- panel-title Starts -->

<i  class="fa fa-money fa-fw"></i>  Insert Relation 

</h3><!-- panel-title Ends -->

</div><!-- panel-heading Ends -->

<div class="panel-body"><!-- panel-body Starts -->

<form class="form-horizontal"  method="post" action="{{route('relation.store')}}" enctype="multipart/form-data"><!-- form-horizontal Starts -->

<input type="hidden" name="_token" value="{{csrf_token()}}">

<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label"> Relation Title  </label>

<div class="col-md-6">

<input type="text" name="rel_title" class="form-control">

</div>

</div><!-- form-group Ends -->

<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label"> Select Product  </label>

<div class="col-md-6">

<select name="product_id" class="form-control">

<option> Select Product </option>
@foreach($products as $key=>$value)
         <option value="{{$key}}">{{$value}}</option>
                                    


@endforeach



</select>

</div>

</div><!-- form-group Ends -->

<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label"> Select Bundle  </label>

<div class="col-md-6">

<select name="bundle_id" class="form-control">

<option> Select Bundle </option>
@foreach($bundles as $key=>$value)
         <option value="{{$key}}">{{$value}}</option>
                                    


@endforeach


</select>

</div>

</div><!-- form-group Ends -->


<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label"> </label>

<div class="col-md-6">

<input type="submit" name="submit" class="btn btn-primary form-control" value="Insert Relation">

</div>

</div><!-- form-group Ends -->

</form><!-- form-horizontal Ends -->

</div><!-- panel-body Ends -->

</div><!-- panel panel-default Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 2 row Ends -->


</div>

@endsection