@extends('backEnd.layouts.master')
@section('title','Add Category')
@section('content')
    <!-- <div id="breadcrumb"> <a href="{{url('/admin')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{route('category.index')}}">Categories</a> <a href="{{route('category.create')}}" class="current">Add New Category</a> </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                    <h5>Add New Category</h5>
                </div>
                <div class="widget-content nopadding">
                    <form class="form-horizontal" method="post" action="{{route('category.store')}}" name="basic_validate" id="basic_validate" novalidate="novalidate">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="control-group{{$errors->has('name')?' has-error':''}} col-md-12">
                            <label class="control-label">Category Name :</label>
                            <div class="controls">
                                <input type="text" class="form-control col-md-12" name="name" id="name" value="{{old('name')}}" required>
                                <span class="text-danger" id="chCategory_name" style="color: red;">{{$errors->first('name')}}</span>
                            </div>
                        </div>
                        
                        <div class="control-group col-md-12">
                            <label class="control-label">Category Lavel :</label>
                            <div class="controls col-md-10" >
                                <select name="parent_id" id="parent_id" class="form-control col-md-12">
                                        @foreach($cate_levels as $key=>$value)
                                            <option value="{{$key}}">{{$value}}</option>
                                            <?php
                                                // if($key!=0){
                                                //     $subCategory=DB::table('categories')->select('id','name')->where('parent_id',$key)->get();
                                                //     if(count($subCategory)>0){
                                                //         foreach ($subCategory as $subCate){
                                                //             echo '<option value="'.$subCate->id.'">&nbsp;&nbsp;--'.$subCate->name.'</option>';
                                                //         }
                                                //     }
                                                // }
                                            ?>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="control-group col-md-12">
                            <label class="control-label">Description :</label>
                            <div class="controls">
                                <textarea name="description" id="description" class="form-control col-md-12 rows="5">{{old('description')}}</textarea>
                            </div>
                        </div>
                        <div class="control-group{{$errors->has('url')?' has-error':''}} col-md-12">
                            <label class="control-label">URL (Start with http://) :</label>
                            <div class="controls">
                                <input type="text" name="url" id="url" class="form-control col-md-12">
                                <span class="text-danger">{{$errors->first('url')}}</span>
                            </div>
                        </div>
                        <div class="control-group{{$errors->has('status')?' has-error':''}} col-md-12">
                            <label class="control-label">Enable :</label>
                            <div class="controls">
                                <input type="checkbox" class="form-control col-md-1" name="status" id="status" value="1">
                                <span class="text-danger">{{$errors->first('status')}}</span>
                            </div>
                        </div>
                        <div class="control-group col-md-12  ">

                            <label for="control-label"></label>

                            <div class="controls">
                           <button type="submit" class="btn btn-primary btn-block">{{ __('Add New Category') }}</button>
                            
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>

        
    </div>
 -->

<div class="container" >
<div class="row"><!-- 1 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<ol class="breadcrumb"><!-- breadcrumb Starts -->

<li>

<i class="fa fa-dashboard"></i>  <a href="{{url('/admin')}}" title="Go to Home" class="tip-bottom"> Dashboard / </a> <a href="{{route('category.index')}}">Categories / </a> <a href="{{route('category.create')}}" class="current">Add New Category</a>

</li>

</ol><!-- breadcrumb Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 1 row Ends -->


<div class="row"><!-- 2 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<div class="panel panel-default"><!-- panel panel-default Starts -->

<div class="panel-heading"><!-- panel-heading Starts -->

<h3 class="panel-title"><!-- panel-title Starts -->

<i class="fa fa-money fa-fw"></i> Insert Category  /

</h3><!-- panel-title Ends -->

</div><!-- panel-heading Ends -->

<div class="panel-body" ><!-- panel-body Starts -->

<form class="form-horizontal" action="{{route('category.store')}}" method="post" enctype="multipart/form-data"><!-- form-horizontal Starts -->
<input type="hidden" name="_token" value="{{csrf_token()}}">

<div class="form-group {{$errors->has('cat_title')?' has-error':''}} "><!-- form-group Starts -->

    <label class="col-md-3 control-label">Category Title</label>

    <div class=" controls col-md-6">

    <input type="text" class="form-control col-md-12" name="name" id="name" value="{{old('name')}}">
    <span class="text-danger" id="" style="color: red;">{{$errors->first('name')}}</span>
    

</div>

</div><!-- form-group Ends -->

<div class="form-group {{$errors->has('parent_id')?' has-error':''}} "><!-- form-group Starts -->

    
    <label class="col-md-3 control-label">Category Level</label>

    <div class=" controls col-md-6">

    <select name="parent_id" id="parent_id" >
    @foreach($cate_levels as $key=>$value)
                                            <option value="{{$key}}">{{$value}}</option>
                                            <?php
                                                if($key!=0){
                                                    $subCategory=DB::table('categories')->select('id','name')->where('parent_id',$key)->get();
                                                    if(count($subCategory)>0){
                                                        foreach ($subCategory as $subCate){
                                                            echo '<option value="'.$subCate->id.'">&nbsp;&nbsp;--'.$subCate->name.'</option>';
                                                        }
                                                    }
                                                }
                                            ?>
                                        @endforeach
                                </select>
    

</div>

</div><!-- form-group Ends -->


<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label">Show as Category Top</label>

<div class=" controls col-md-6">

<input type="radio" name="cat_top" value="yes">

<label>Yes</label>

<input type="radio" name="cat_top" value="no">

<label>No</label>

</div>

</div><!-- form-group Ends -->


<label class="col-md-3 control-label">Select Category Image</label>

<div class=" controls col-md-6">

<input type="file" name="cat_image" class="form-control" value="{{old('cat_image')}}" >
<span class="text-danger" id="" style="color: red;">{{$errors->first('cat_image')}}</span>

</div>

</div><!-- form-group Ends -->



<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label"></label>

<div class="col-md-6">

<input type="submit" name="submit" value="Insert Category" class="btn btn-primary form-control">

</div>
<br> <br>

</div><!-- form-group Ends -->

</form><!-- form-horizontal Ends -->

</div><!-- panel-body Ends -->

</div><!-- panel panel-default Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 2 row Ends -->
</div>

@endsection
@section('jsblock')
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery.ui.custom.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery.uniform.js') }}"></script>
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/matrix.js') }}"></script>
    <script src="{{ asset('js/matrix.form_validation.js') }}"></script>
    <script src="{{ asset('js/matrix.tables.js') }}"></script>
    <script src="{{ asset('js/matrix.popover.js') }}"></script>
@endsection