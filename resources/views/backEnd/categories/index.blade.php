@extends('backEnd.layouts.master')
@section('title','List Categories')
@section('content')
   <div class="container">
   <div id="breadcrumb"> <a href="{{url('/admin')}}" title="Go to Home" class="tip-bottom"><i class="fa fa-dashboard" ></i> Dashboard</a>  / <a href="{{route('category.index')}}" class="current">Categories</a></div>
    <div class="container-fluid">
        @if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                <strong>Well done!</strong> {{Session::get('message')}}
            </div>
        @endif
        <div class="widget-box">
            <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                <h5>List Categories</h5>
            </div>

<form class="navbar-form" method="post" action=""><!-- navbar-form Starts -->
        <!-- {{csrf_field() }} -->
        <div class="input-group"><!-- input-group Starts -->

            <input class="form-control" type="text" placeholder="Search" name="product" placeholder="Keyword"  required>

            <span class="input-group-btn"><!-- input-group-btn Starts -->

                    <button type="submit" value="Search" name="search" class="btn btn-primary">

                    <i class="fa fa-search"></i>

                    </button>

            </span><!-- input-group-btn Ends -->

        </div><!-- input-group Ends -->

    </form><!-- navbar-form Ends -->







            <div class="table">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>Category ID</th>
                        <th>Category Name</th>
                        <th>Created At</th>
                        <!-- <th>Category Image</th> -->
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $category)
                            <?php
                                $parent_cates = DB::table('categories')->select('name')->where('id',$category->parent_id)->get();
                            ?>
                            <tr>
                                <td>{{$category->id}}</td>
                                <td>
                                    <!-- @foreach($parent_cates as $parent_cate)
                                        {{$parent_cate->name}}
                                    @endforeach -->
                                    {{$category->name}}
                                </td>
                             
                                <td style="text-align: center;">{{$category->created_at->diffForHumans()}}</td>
                                <!-- <td style="text-align: center;">
                                    <img src='/storage/{{ $category->cat_image }}' class='img-responsive'>
                                </td> -->
                                <td style="text-align: center;">
                                    <!-- <a href="{{route('category.edit',$category->id)}}" class="btn btn-primary btn-mini">Edit</a>
                                    <a href="javascript:" rel="{{$category->id}}" rel1="delete-category" class="btn btn-danger btn-mini deleteRecord">Delete</a> -->
                                    <a href="{{route('category.edit',$category->id)}}"><i class="fa fa-pencil" > </i> Edit </a>
                                    <a href="javascript:" rel="{{$category->id}}" rel1="delete-category" class="btn-mini deleteRecord">  <i class="fa fa-trash-o" > </i> Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$categories->links()}}
            </div>
        </div>
    </div>
   </div>



@endsection
@section('jsblock')
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/jquery.ui.custom.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.uniform.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/matrix.js')}}"></script>
    <script src="{{asset('js/matrix.tables.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script>
        $(".deleteRecord").click(function () {
           var id=$(this).attr('rel');
           var deleteFunction=$(this).attr('rel1');
           swal({
               title:'Are you sure?',
               text:"You won't be able to revert this!",
               type:'warning',
               showCancelButton:true,
               confirmButtonColor:'#3085d6',
               cancelButtonColor:'#d33',
               confirmButtonText:'Yes, delete it!',
               cancelButtonText:'No, cancel!',
               confirmButtonClass:'btn btn-success',
               cancelButtonClass:'btn btn-danger',
               buttonsStyling:false,
               reverseButtons:true
           },function () {
              window.location.href="/admin/"+deleteFunction+"/"+id;
           });
        });
    </script>
@endsection