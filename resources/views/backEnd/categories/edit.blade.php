@extends('backEnd.layouts.master')
@section('title','Add Category')
@section('content')

<div class="container" >
<div class="row"><!-- 1 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<ol class="breadcrumb"><!-- breadcrumb Starts -->

<li>

<i class="fa fa-dashboard"></i>  <a href="{{url('/admin')}}" title="Go to Home" class="tip-bottom"> Dashboard / </a> <a href="{{route('category.index')}}">Categories / </a> <a href="{{route('category.create')}}" class="current">Add New Category</a>

</li>

</ol><!-- breadcrumb Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 1 row Ends -->


<div class="row"><!-- 2 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<div class="panel panel-default"><!-- panel panel-default Starts -->

<div class="panel-heading"><!-- panel-heading Starts -->

<h3 class="panel-title"><!-- panel-title Starts -->

<!-- <i class="fa fa-money fa-fw"></i> Update Category  -->
<i class="fa fa-dashboard"></i>  <a href="{{url('/admin')}}" title="Go to Home" class="tip-bottom"> Dashboard / </a> <a href="{{route('category.index')}}">Categories / </a> <a href="{{route('category.create')}}" class="current">Insert Category / </a> <a href="" class="current">Update Category </a>


</h3><!-- panel-title Ends -->

</div><!-- panel-heading Ends -->

<div class="panel-body" ><!-- panel-body Starts -->

<form class="form-horizontal" action="{{route('category.update',$edit_category->id)}}" method="post" enctype="multipart/form-data"><!-- form-horizontal Starts -->
<input type="hidden" name="_token" value="{{csrf_token()}}">
{{method_field("PUT")}}
<div class="form-group {{$errors->has('cat_title')?' has-error':''}} "><!-- form-group Starts -->

    <label class="col-md-3 control-label">Category Title</label>

    <div class="col-md-6">

    <input type="text" class="form-control col-md-12" name="name" id="name" value="{{$edit_category->name}}" required>
    <span class="text-danger" id="chCategory_name" style="color: red;">{{$errors->first('name')}}</span>
    

</div>

</div><!-- form-group Ends -->

<div class="form-group {{$errors->has('name')?' has-error':''}} "><!-- form-group Starts -->

    <label class="col-md-3 control-label">Category Level</label>

    <div class="col-md-6">

    <select name="parent_id" id="parent_id" >
    {{--@foreach($cate_levels as $key=>$value)
                                            <option value="{{$key}}" {{($edit_category->parent_id==$key)?' selected':''}}>{{$value}}</option>
                                        @endforeach--}}

                                        @foreach($cate_levels as $key=>$value)
                                            <option value="{{$key}}"{{($edit_category->parent_id==$key)?' selected':''}}>{{$value}}</option>
                                            <?php
                                            if($key!=0){
                                                $subCategory=DB::table('categories')->select('id','name')->where('parent_id',$key)->get();
                                                if(count($subCategory)>0){
                                                    foreach ($subCategory as $subCate){
                                                        echo '<option value="'.$subCate->id.'">&nbsp;&nbsp;--'.$subCate->name.'</option>';
                                                    }
                                                }
                                            }
                                            ?>
                                        @endforeach
                                </select>
    

</div>

</div><!-- form-group Ends -->


<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label">Show as Category Top</label>

<div class="col-md-6">


<input type="radio" name="cat_top" value="yes" {{ $edit_category->cat_top == 'yes' ? 'checked' : '' }} >


<label>Yes</label>

<input type="radio" name="cat_top" value="no"{{ $edit_category->cat_top == 'no' ? 'checked' : '' }} >


<label>No</label>

</div>

</div><!-- form-group Ends -->


<label class="col-md-3 control-label">Select Category Image</label>

<div class="col-md-6">

<input type="file" name="cat_image" class="form-control" value="" required>
<img src='/storage/{{ $edit_category->cat_image }}' width="100" height="100" class="img-responsive" >
<span class="text-danger" id="chCategory_image" style="color: red;">{{$errors->first('cat_image')}}</span>

</div>

</div><!-- form-group Ends -->



<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label"></label>

<div class="col-md-6">

<input type="submit" name="submit" value="Update Category" class="btn btn-primary form-control">

</div>
<br> <br>

</div><!-- form-group Ends -->

</form><!-- form-horizontal Ends -->

</div><!-- panel-body Ends -->

</div><!-- panel panel-default Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 2 row Ends -->
</div>

@endsection
