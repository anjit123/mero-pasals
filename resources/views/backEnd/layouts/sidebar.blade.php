
<div class="collapse navbar-collapse navbar-ex1-collapse"><!-- collapse navbar-collapse navbar-ex1-collapse Starts -->

<ul class="nav navbar-nav side-nav"><!-- nav navbar-nav side-nav Starts -->

        <li class=""><!-- li Starts -->

            <a href=" {{url('/admin')}}">

                <i class="fa fa-fw fa-dashboard"></i> Dashboard

            </a>

        </li><!-- li Ends -->

<li class=""><!-- Products li Starts -->

<a  href="" data-toggle="collapse" data-target="#products">

<i class="fa fa-fw fa-table"></i> Products

<i class="fa fa-fw fa-caret-down"></i>


</a>

<ul id="products" class="collapse">

<li>
<a href="{{url('/admin/product/create')}}"> Insert Products </a>
</li>

<li>
<a href="{{route('product.index')}}"> View Products </a>
</li>


</ul>

</li><!-- Products li Ends -->

<li class=""><!-- Bundles Li Starts --->

<a href="" data-toggle="collapse" data-target="#bundles">

<i class="fa fa-fw fa-edit"></i> Bundles

<i class="fa fa-fw fa-caret-down"></i>

</a>

<ul id="bundles" class="collapse">

<li>
<a href="{{url('/admin/bundle')}}"> Insert Bundle </a>
</li>

<li>
<a href="{{url('/admin/bundles')}}"> View Bundles </a>
</li>

</ul>

</li><!-- Bundles Li Ends --->

<li><!-- relations li Starts -->

<a href="#" data-toggle="collapse" data-target="#relations"><!-- anchor Starts -->

<i class="fa fa-fw fa-retweet"></i> Assign Products To Bundles Relations

<i class="fa fa-fw fa-caret-down"></i>

</a><!-- anchor Ends -->

<ul id="relations" class="collapse"><!-- collapse Starts -->

<li>

<a href="{{url('/admin/relation/create')}}"> Insert Relation </a>

</li>


<li>

<a href="{{route('relation.index')}}"> View Relations </a>

</li>

</ul><!-- collapse Ends -->


</li><!-- relations li Ends -->



<li><!-- manufacturer li Starts -->

<a href="#" data-toggle="collapse" data-target="#manufacturers"><!-- anchor Starts -->

<i class="fa fa-fw fa-briefcase"></i> Manufacturers

<i class="fa fa-fw fa-caret-down"></i>


</a><!-- anchor Ends -->

<ul id="manufacturers" class="collapse"><!-- ul collapse Starts -->

<li>
<a href="{{url('/admin/manufacturer/create')}}"> Insert Manufacturer </a>
</li>

<li>
<a href="{{route('manufacturer.index')}}"> View Manufacturers </a>
</li>

</ul><!-- ul collapse Ends -->


</li><!-- manufacturer li Ends -->


<li><!-- li Starts -->

<a href="#" data-toggle="collapse" data-target="#p_cat">

<i class="fa fa-fw fa-pencil"></i> Products Categories

<i class="fa fa-fw fa-caret-down"></i>


</a>

<ul id="p_cat" class="collapse">

<li>
<a href="{{url('/admin/productcategory/create')}}"> Insert Product Category </a>
</li>

<li>
<a href="{{route('productcategory.index')}}"> View Products Categories </a>
</li>


</ul>

</li><!-- li Ends -->


<li><!-- li Starts -->

<a href="#" data-toggle="collapse" data-target="#cat">

<i class="fa fa-fw fa-arrows-v"></i> Categories

<i class="fa fa-fw fa-caret-down"></i>

</a>

<ul id="cat" class="collapse">

<li>
<a href="{{url('/admin/category/create')}}"> Insert Category </a>
</li>

<li>
<a href="{{route('category.index')}}"> View Categories </a>
</li>


</ul>

</li><!-- li Ends -->


<li><!-- boxes section li Starts -->

<a href="#" data-toggle="collapse" data-target="#boxes"><!-- anchor Starts -->

<i class="fa fa-fw fa-arrows"></i> Boxes Section

<i class="fa fa-fw fa-caret-down"></i>

</a><!-- anchor Ends -->

<ul id="boxes" class="collapse">

<li>

<a href="{{url('/admin/box/create')}}"> Insert Box </a>

</li>


<li>

<a href="{{route('box.index')}}"> View Boxes </a>

</li>

</ul>

</li><!--boxes section li Ends -->

<!-- services section li Starts -->
<!-- <li>

<a href="#" data-toggle="collapse" data-target="#services">

<i class="fa fa-fw fa-briefcase"></i> Services

<i class="fa fa-fw fa-caret-down"></i>

</a>

<ul id="services" class="collapse">

<li>
<a href=""> Insert Service </a>
</li>

<li>
<a href=""> View Services </a>
</li>

</ul>

</li> -->
<!-- services section li Ends -->


<!-- contact us li Starts -->
<!-- <li>

<a href="#" data-toggle="collapse" data-target="#contact_us">


<i class="fa fa-fw fa-pencil"> </i> Contact Us Section

<i class="fa fa-fw fa-caret-down"></i>

</a> -->
<!-- anchor Ends -->
<!-- 
<ul id="contact_us" class="collapse">

<li>

<a href=""> Edit Contact Us </a>

</li>

<li>

<a href=""> Insert Enquiry Type </a>

</li>

<li>

<a href=""> View Enquiry Types </a>

</li>

</ul> -->

<!-- </li> -->
<!-- contact us li Ends -->

<!-- <li> -->
<!-- about us li Starts -->

<!-- <a href="">

<i class="fa fa-fw fa-edit"></i> Edit About Us Page

</a>

</li> -->
<!-- about us li Ends -->


<li><!-- Coupons Section li Starts -->

<a href="#" data-toggle="collapse" data-target="#coupons"><!-- anchor Starts -->

<i class="fa fa-fw fa-arrows-v"></i> Coupons

<i class="fa fa-fw fa-caret-down"></i>

</a><!-- anchor Ends -->

<ul id="coupons" class="collapse"><!-- ul collapse Starts -->

<li>
<a href="{{url('/admin/coupon/create')}}"> Insert Coupon </a>
</li>

<li>
<a href="{{route('coupon.index')}}"> View Coupons </a>
</li>

</ul><!-- ul collapse Ends -->

</li><!-- Coupons Section li Ends -->

<li><!-- slide li Starts -->

<a href="#" data-toggle="collapse" data-target="#slides">

<i class="fa fa-fw fa-gear"></i> Slides

<i class="fa fa-fw fa-caret-down"></i>


</a>

<ul id="slides" class="collapse">

<li>
<a href="{{url('/admin/slide/create')}}"> Insert Slide </a>
</li>

<li>
<a href="{{route('slide.index')}}"> View Slides </a>
</li>


</ul>

<!-- slide li Ends -->
</li>

<!-- terms li Starts -->

<li>

<a href="{{url('/admin/showuser')}}">

<i class="fa fa-fw fa-edit"></i> View Customers

</a>

</li>




<li><!-- terms li Starts -->

<a href="#" data-toggle="collapse" data-target="#reports" ><!-- anchor Starts -->

<i class="fa fa-fw fa-table"></i> Reports

<i class="fa fa-fw fa-caret-down"></i>

</a><!-- anchor Ends -->

<ul id="reports" class="collapse"><!-- ul collapse Starts -->



<li>
<a href="{{route('report.index')}}"> View Report </a> 
</li>

<li>

<a href="{{route('order')}}">

<i class="fa fa-fw fa-list"></i> View Orders

</a>

</li>
<li>

<a href="{{url('admin/view-order-charts')}}">

<i class="fa fa-fw fa-edit"></i> Order Report Chart

</a>

</li>

</ul><!-- ul collapse Ends -->


</li><!-- terms li Ends -->


<li>

<a href="{{route('customer-reviews.index')}}">

<i class="fa fa-fw fa-pencil"></i> Manage Reviews

</a>

</li>

<li><!-- li Starts -->

<a href="#" data-toggle="collapse" data-target="#users">

<i class="fa fa-fw fa-gear"></i> Users

<i class="fa fa-fw fa-caret-down"></i>


</a>

<ul id="users" class="collapse">

<li>
<a href="{{url('/admin/createuser/create')}}"> Insert User </a>
</li>

<li>
<a href="{{url('/admin/showuser')}}"> Show Users </a>
</li>


<li>
<a href="{{url('/admin/role/create')}}"> Insert Roles </a>
</li>

<li>
<a href="{{url('/admin/roleuser/create')}}"> Insert User Roles </a>
</li>
<li>
<a href="{{url('admin/view-user-charts')}}"> User Register Charts </a>
</li>

</ul>

</li><!-- li Ends -->

<li><!-- li Starts -->

<li class="">
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                <i class="fa fa-fw fa-power-off"></i>{{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>

        </li>

</li><!-- li Ends -->

</ul><!-- nav navbar-nav side-nav Ends -->

</div><!-- collapse navbar-collapse navbar-ex1-collapse Ends -->

</nav><!-- navbar navbar-inverse navbar-fixed-top Ends -->
<div id="page-wrapper"><!-- page-wrapper Starts -->



</div><!-- page-wrapper Ends -->

</div><!-- wrapper Ends -->
