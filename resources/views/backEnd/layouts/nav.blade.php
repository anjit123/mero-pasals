<div id="wrapper"><!-- wrapper Starts -->
<nav class="navbar navbar-inverse navbar-fixed-top" ><!-- navbar navbar-inverse navbar-fixed-top Starts -->

<div class="navbar-header" ><!-- navbar-header Starts -->

<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse" ><!-- navbar-ex1-collapse Starts -->


<span class="sr-only" >Toggle Navigation</span>

<span class="icon-bar" ></span>

<span class="icon-bar" ></span>

<span class="icon-bar" ></span>


</button><!-- navbar-ex1-collapse Ends -->

<a class="navbar-brand" href="" >Admin Panel</a>
<a class="navbar-brand" href="/" >Front End</a>


</div><!-- navbar-header Ends -->

<ul class="nav navbar-right top-nav" ><!-- nav navbar-right top-nav Starts -->

<li class="dropdown" ><!-- dropdown Starts -->

<a href="#" class="dropdown-toggle" data-toggle="dropdown" ><!-- dropdown-toggle Starts -->



@if(Auth::check())           
<i class="fa fa-user" >  {{ Auth::user()->name }}</i>
 @else
 <i class="fa fa-user" ></i>
 @endif

  <b class="caret" ></b>


</a><!-- dropdown-toggle Ends -->

<ul class="dropdown-menu" ><!-- dropdown-menu Starts -->

<li><!-- li Starts -->

<a href="{{route('profile_update')}}" >

<i class="fa fa-fw fa-user" ></i> Profile


</a>

</li><!-- li Ends -->

<li><!-- li Starts -->

<a href="{{route('product.index')}}" >

<i class="fa fa-fw fa-envelope" ></i> Products 

<span class="badge" ></span>


</a>

</li><!-- li Ends -->

<li><!-- li Starts -->

<a href="{{url('/admin/showuser')}}" >

<i class="fa fa-fw fa-gear" ></i> Customers

<span class="badge" ></span>


</a>

</li><!-- li Ends -->

<li><!-- li Starts -->

<a href="{{route('productcategory.index')}}" >

<i class="fa fa-fw fa-gear" ></i> Product Categories

<span class="badge" ></span>


</a>

</li><!-- li Ends -->

<li class="divider"></li>

<li><!-- li Starts -->


</li><!-- li Ends -->
<li class="submenu {{$menu_active ==5? ' active':''}}">
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                <i class="fa fa-fw fa-power-off"></i>{{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>

        </li>

</ul><!-- dropdown-menu Ends -->




</li><!-- dropdown Ends -->


</ul><!-- nav navbar-right top-nav Ends -->



