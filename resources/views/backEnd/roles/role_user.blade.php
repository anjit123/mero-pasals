@extends('backEnd.layouts.master')
@section('title','Insert User Role')
@section('content')

<!-- <script src="//cdn.tinymce.com/4/tinymce.min.js"></script> -->
  <!-- <script>tinymce.init({ selector:'#product_desc,#product_features' });</script> -->
<div class="container">

<div class="row"><!-- row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<ol class="breadcrumb"><!-- breadcrumb Starts -->

<li class="active">

<i class="fa fa-dashboard"> </i> Dashboard / Insert User Role

</li>

</ol><!-- breadcrumb Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- row Ends -->


<div class="row"><!-- 2 row Starts --> 

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<div class="panel panel-default"><!-- panel panel-default Starts -->

<div class="panel-heading"><!-- panel-heading Starts -->

<h3 class="panel-title">

<i class="fa fa-money fa-fw"></i> Insert User Role

</h3>

</div><!-- panel-heading Ends -->
@if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                <strong>Well done!</strong> {{Session::get('message')}}
            </div>
        @endif

<div class="panel-body"><!-- panel-body Starts -->

<form class="form-horizontal" method="POST" action="{{route('roleuser.store')}}" enctype="multipart/form-data"><!-- form-horizontal Starts -->

<input type="hidden" name="_token" value="{{csrf_token()}}">

<!-- 1 form-group Starts -->
<!-- <div class="form-group" >

<label class="col-md-3 control-label">Role Name:</label>

<div class="col-md-6">

<input type="text" name="name" class="form-control" required>

</div>

</div> -->

<!-- 1 form-group Ends -->



<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Roles </label>

<div class="col-md-6" >

<select name="role_id" class="form-control" >

<option> Select  a  Role </option>
@foreach($roles as $key=>$value)
    <option value="{{$key}}">{{$value}}</option>

@endforeach

</select>

</div>

</div><!-- form-group Ends -->



<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > User </label>

<div class="col-md-6" >

<select name="user_id" class="form-control" >

<option> Select  a  User </option>
@foreach($users as $key=>$value)
    <option value="{{$key}}">{{$value}}</option>

@endforeach

</select>

</div>

</div><!-- form-group Ends -->


<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" ></label>

<div class="col-md-6" >

<input type="submit" name="submit" value="Insert Role" class="btn btn-primary form-control" >

</div>

</div><!-- form-group Ends -->

</form><!-- form-horizontal Ends -->

</div><!-- panel-body Ends -->

</div><!-- panel panel-default Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 2 row Ends --> 

</div>


@endsection