@extends('backEnd.layouts.master')
@section('title','Insert Slide')
@section('content')

<div class="container">
    This is slide insert page


    

<div class="row" ><!-- 1 row Starts -->

<div class="col-lg-12" ><!-- col-lg-12 Starts --> 

<ol class="breadcrumb"><!-- breadcrumb Starts -->

<li class="active">

<i class="fa fa-dashboard" ></i> Dashboard / Insert Slide

</li>

</ol><!-- breadcrumb Ends -->



</div><!-- col-lg-12 Ends --> 

</div><!-- 1 row Ends -->

<div class="row" ><!-- 2 row Starts -->

<div class="col-lg-12" ><!-- col-lg-12 Starts -->

<div class="panel panel-default" ><!-- panel panel-default Starts -->

<div class="panel-heading" ><!-- panel-heading Starts -->

<h3 class="panel-title" >

<i class="fa fa-money fa-fw"></i> Insert Slide / <a href="{{route('slide.index')}}">Slides </a>

</h3>

</div><!-- panel-heading Ends -->

<div class="panel-body" ><!-- panel-body Starts -->

<form class="form-horizontal" action="{{route('slide.store')}}" method="post" enctype="multipart/form-data" ><!-- form-horizontal Starts -->
<input type="hidden" name="_token" value="{{csrf_token()}}">

<div class="form-group" ><!-- 1 form-group Starts -->

<label class="col-md-3 control-label">Slide Name:</label>

<div class="col-md-6">

<input type="text" name="slide_name" class="form-control" required>

</div>

</div><!-- 1 form-group Ends -->

<div class="form-group" ><!-- 2 form-group Starts -->

<label class="col-md-3 control-label">Slide Image:</label>

<div class="col-md-6">

<input type="file" name="slide_image" class="form-control" required>

</div>

</div><!-- 2 form-group Ends -->


<!-- 3 form-group Starts -->
<!-- <div class="form-group" > -->

<!-- <label class="col-md-3 control-label">Slide Url:</label>

<div class="col-md-6">

<input type="text" name="slide_url" class="form-control" required>

</div>

</div> -->
<!-- 3 form-group Ends -->

<div class="form-group" ><!-- 4 form-group Starts -->

<label class="col-md-3 control-label"></label>

<div class="col-md-6">

<input type="submit" name="submit" value="Add Slide" class=" btn btn-primary form-control" >

</div>

</div><!--4 form-group Ends -->


</form><!-- form-horizontal Ends -->

</div><!-- panel-body Ends -->


</div><!-- panel panel-default Ends -->

</div><!-- col-lg-12 Ends -->


</div><!-- 2 row Ends -->

</div>



@endsection