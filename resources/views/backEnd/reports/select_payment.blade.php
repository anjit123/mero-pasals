@extends('backEnd.layouts.master')
@section('title','View Order chart')
@section('content')

<br><br><br>



<div class="container">

<div class="row">
    <div class="col-md-12">
    
    @if($orders->count() > 0)  

<div class="table" ><!-- table-responsive Starts -->

<table class="table table-bordered table-responsive" ><!-- table table-bordered table-hover Starts -->

<thead><!-- thead Starts -->

<tr>
<td>Order ID:</td>
<td>Email</td>
<td>Ordered Products:</td>
<td>Payment Method</td>
<td>Grand Total</td>
<td>Status</td>
<td>Created on</td>
<!-- <td>Ac</td> -->
<!-- <td>Action</td> -->

</tr>

</thead><!-- thead Ends -->

<tbody><!--- tbody Starts --->


@foreach($orders as $order)
<tr><!-- tr Starts -->

<td>{{$order->id}}</td>
  
    <td>{{ $order->users_email}}</td>
<td>
    @foreach($order->orderss as $pro)
        <a href="{{url('/orders/'.$order->id) }}">{{$pro->product_code}}</a> <br>
        {{$pro->product_title}} <br>
        {{$pro->product_size}} <br>
        <!-- {{$pro->product_qty}} <br> -->
    @endforeach
</td>

<td>{{$order->payment_method}}</td>
<td>{{$order->grand_total}}</td>
<td>{{$order->order_status}}</td>
<td>{{ $order->created_at->format ('l j F Y')}}</td>


<!-- <td>

</td>
<td>

</td> -->


</tr><!-- tr Ends -->
@endforeach
</tbody><!--- tbody Ends --->


</table><!-- table table-bordered table-hover Ends -->
<h3> Grand Total: {{$grand_total}}</h3>
{{$orders->links()}}


</div><!-- table-responsive Ends -->

          </div>
          @else
            <div class="alert alert-danger" role="alert">
                There is no Order Report
            </div>

        @endif
    </div>
</div>



</div>

@endsection