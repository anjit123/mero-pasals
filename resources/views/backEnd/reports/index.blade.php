@extends('backEnd.layouts.master')
@section('title','View Order chart')
@section('content')

<br><br><br>



<div class="container">


<div class="row">

    <div class="col-md-12">


    <h3>Custom Report Search</h3>

    <form action="admin/reprot/show" method="get">
    
    
        <div class="form-group">
            <div class="input-group date" id="date-start" data-target-input="nearest">
                    <input type="text" name="dateStart" class="form-control datetimepicker-input" data-target="#date-start"/>
                    <div class="input-group-append" data-target="#date-start" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>

        <div class="form-group">
           <div class="input-group date" id="date-end" data-target-input="nearest">
                <input type="text" name="dateEnd" class="form-control datetimepicker-input" data-target="#date-end"/>
                <div class="input-group-append" data-target="#date-end" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
        </div>

        <input type="submit"  class="btn btn-primary" value="Show Report">

    </form>

    </div>

</div>



<br>


<br><br><br>
<div class="row">
<div class="col-md-4">
<form action="/admin/report/check" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <h3>Search By Status</h3>
    
        <div class="form-group" >
            <label class="control-label" >Take a Status</label>
            <!-- <div class="" >
                <input type="date" class="form-control" name="date" id="date" required>
            </div> -->
            <select class="form-control" name="status" id="status">
                <option value="New">New</option>
                <option value="Pending">Pending</option>
                <!-- <option value="New">New</option> -->

            </select>

        </div>
        <div class="form-group" >
           <button class="btn btn-primary" type="submit">Search By Status</button>
        </div>

    </form>
    
   
</div>
<div class="col-md-4">

<form action="/admin/report/check" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <h3>Search By Payment Method</h3>
    
        <div class="form-group" >
            <label class="control-label" >Take a Payment Method</label>
            <!-- <div class="" >
                <input type="date" class="form-control" name="date" id="date" required>
            </div> -->
            <select class="form-control" name="payment" id="payment">
                <option value="COD">COD</option>
                <option value="Paypal">Paypal</option>
                <!-- <option value="New">New</option> -->

            </select>

        </div>
        <div class="form-group" >
           <button class="btn btn-primary" type="submit">Search By Payment Method</button>
        </div>

    </form>
    




<!-- 
    <form action="" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{csrf_token()}}">

    <h3>Search By Month</h3>

        <div class="form-group">
            <label for="month" class="control-label">Select Month</label>
            <select class="form-control" name="month" id="month">
                <option value="January">January</option>
                <option value="February">February</option>
                <option value="March">March</option>
                <option value="April">April</option>
                <option value="May">May</option>
                <option value="June">June</option>
                <option value="July">July</option>
                <option value="August">August</option>
                <option value="September">September</option>
                <option value="October">October</option>
                <option value="November">November</option>
                <option value="December">December</option>
            </select>
        </div>
        <div class="form-group">
            <label for="year" class="control-label">Select Your Year</label>

            <select name="year" id="year" class="form-control">
                <option value="2020">2020</option>
                <option value="2021">2021</option>
            </select>
        </div>

        <div class="form-group">
            <button type="submit">Search</button>
        </div>

    
    
    
    </form> -->


</div>
<div class="col-md-4">

<!-- <form action="" method="post">

<div class="form-group">
            <label for="year" class="control-label">Select Your Year</label>

            <select name="year" id="year" class="form-control">
                <option value="2020">2020</option>
                <option value="2021">2021</option>
            </select>
        </div>

        <div class="form-group">
            <button type="submit">Search</button>
        </div>
</form> -->



</div>

</div>

</div>


</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>

<script type="text/javascript">
    $(function () {
        $('#date-start').datetimepicker({
            format : 'L'
        });
        $('#date-end').datetimepicker({
            format : 'L',
            useCurrent: false
        });
        $("#date-start").on("change.datetimepicker", function (e) {
            $('#date-end').datetimepicker('minDate', e.date);
        });
        $("#date-end").on("change.datetimepicker", function (e) {
            $('#date-start').datetimepicker('maxDate', e.date);
        });
    });
</script>
@endsection