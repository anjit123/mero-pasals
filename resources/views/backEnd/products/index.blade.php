@extends('backEnd.layouts.master')
@section('title','View Products')
@section('content')


<div class="container">

<div class="row"><!--  1 row Starts -->

<div class="col-lg-12" ><!-- col-lg-12 Starts -->

<ol class="breadcrumb" ><!-- breadcrumb Starts -->

<li class="active" >

<i class="fa fa-dashboard"></i> Dashboard / <a href="{{url('/admin/bundles')}}" > View Bundles </a>

</li>

</ol><!-- breadcrumb Ends -->

</div><!-- col-lg-12 Ends -->

</div><!--  1 row Ends -->

<div class="row" ><!-- 2 row Starts -->

<div class="col-lg-12" ><!-- col-lg-12 Starts -->

<div class="panel panel-default" ><!-- panel panel-default Starts -->

<div class="panel-heading" ><!-- panel-heading Starts -->

<h3 class="panel-title" ><!-- panel-title Starts -->

<i class="fa fa-money fa-fw" ></i> View Products / <a href="{{url('/admin/bundles')}}" > View Bundles </a>

</h3><!-- panel-title Ends -->


</div><!-- panel-heading Ends -->

<div class="panel-body" ><!-- panel-body Starts -->


<form class="navbar-form" method="post" action="{{ route('product.search') }}"><!-- navbar-form Starts -->
        {{csrf_field() }}
        <div class="input-group"><!-- input-group Starts -->

            <input class="form-control" type="text" placeholder="Search" name="product" placeholder="Keyword"  required>

            <span class="input-group-btn"><!-- input-group-btn Starts -->

                    <button type="submit" value="Search" name="search" class="btn btn-primary">

                    <i class="fa fa-search"></i>

                    </button>

            </span><!-- input-group-btn Ends -->

        </div><!-- input-group Ends -->

    </form><!-- navbar-form Ends -->


<div class="table" ><!-- table-responsive Starts -->

<table class="table table-bordered table-responsive" ><!-- table table-bordered table-hover table-striped Starts -->

<thead>

<tr>
<th>ID</th>
<th>Product Image</th>
<th>Title</th>
<th>Category</th>
<th>Product Category</th>
<th>Price|Sale</th>
<th>Add Image</th>
<th>Add Attribute</th>
<th>Actions</th>



</tr>

</thead>

<tbody>

@foreach($products as $product)

<?php $i++; ?>
<tr>

<td style="vertical-align: middle; text-align: center;">{{$i}}</td>


<td style="vertical-align: center;text-align: center;"><img src="{{url('products/small',$product->image)}}" width="60" height="60"></td>

<td style="vertical-align: middle;text-align: center;"> {{$product->product_title}} </td>


<td style="vertical-align: middle; text-align: center;">{{$product->category->name}} </td>



<td style="vertical-align: middle;text-align: center;">{{$product->productcategory->p_cat_name}} </td>


<td style="vertical-align: middle;text-align: center;"> 
${{$product->product_price}} |
${{$product->psale_price}}
 </td>

 <td style="vertical-align: middle;text-align: center;"><a href="{{route('image-gallery.show',$product->id)}}" class="btn btn-default btn-mini">Add Images</a></td>
 <td style="vertical-align: middle;text-align: center;"><a href="{{route('product_attr.show',$product->id)}}" class="btn btn-success btn-mini">Add Attr</a></td>


<td style="text-align: center; vertical-align: middle;">
        <a href="#exampleModal" data-toggle="modal" data-target="#exampleModal" class="btn-mini"> <strong> View </strong> | </a>
        <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
            View
        </button> -->
        <a href="{{route('product.edit',$product->id)}}"><i class="fa fa-pencil" > </i> <strong>Edit</strong> |  </a>
        <a href="javascript:" rel="{{$product->id}}" rel1="delete-product" class="btn-mini deleteRecord">  <i class="fa fa-trash-o" > </i> <strong>Delete</strong> </a>
</td>

</tr>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$product->product_title}} </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="text-center"><img src="{{url('products/small',$product->image)}}" width="100" alt="{{$product->product_title}}"></div>
                                <p class=""> <strong> Product Description:</strong> {{$product->product_desc}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

@endforeach
</tbody>


</table><!-- table table-bordered table-hover table-striped Ends -->

{{$products->links()}}
</div><!-- table-responsive Ends -->

</div><!-- panel-body Ends -->

</div><!-- panel panel-default Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 2 row Ends -->

</div>

@endsection

@section('jsblock')
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/jquery.ui.custom.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.uniform.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/matrix.js')}}"></script>
    <script src="{{asset('js/matrix.tables.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script>
        $(".deleteRecord").click(function () {
           var id=$(this).attr('rel');
           var deleteFunction=$(this).attr('rel1');
           swal({
               title:'Are you sure?',
               text:"You won't be able to revert this!",
               type:'warning',
               showCancelButton:true,
               confirmButtonColor:'#3085d6',
               cancelButtonColor:'#d33',
               confirmButtonText:'Yes, delete it!',
               cancelButtonText:'No, cancel!',
               confirmButtonClass:'btn btn-success',
               cancelButtonClass:'btn btn-danger',
               buttonsStyling:false,
               reverseButtons:true
           },function () {
              window.location.href="/admin/"+deleteFunction+"/"+id;
           });
        });
    </script>
@endsection
