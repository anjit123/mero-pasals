@extends('backEnd.layouts.master')
@section('title','Insert Slide')
@section('content')

<!-- <script src="//cdn.tinymce.com/4/tinymce.min.js"></script> -->
  <!-- <script>tinymce.init({ selector:'#product_desc,#product_features' });</script> -->
<div class="container">

<div class="row"><!-- row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<ol class="breadcrumb"><!-- breadcrumb Starts -->

<li class="active">

<i class="fa fa-dashboard"> </i> Dashboard / Update Products

</li>

</ol><!-- breadcrumb Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- row Ends -->


<div class="row"><!-- 2 row Starts --> 

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<div class="panel panel-default"><!-- panel panel-default Starts -->

<div class="panel-heading"><!-- panel-heading Starts -->

<h3 class="panel-title">

<i class="fa fa-money fa-fw"></i> Update Products 

</h3>

</div><!-- panel-heading Ends -->
@if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                <strong>Well done!</strong> {{Session::get('message')}}
            </div>
        @endif

<div class="panel-body"><!-- panel-body Starts -->

<form class="form-horizontal" method="POST" action="{{route('product.update',$edit_product->id)}}" enctype="multipart/form-data"><!-- form-horizontal Starts -->

<input type="hidden" name="_token" value="{{csrf_token()}}">
{{method_field("PUT")}}
<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Title </label>

<div class="col-md-6" >

<input type="text" name="product_title" class="form-control" value="{{$edit_product->product_title}}" required >

</div>

</div><!-- form-group Ends -->


<!-- form-group Starts -->
<div class="form-group" >

<label class="col-md-3 control-label" > Product Color </label>

<div class="col-md-6" >

<input type="text" name="product_color" class="form-control" value="{{$edit_product->product_color}}" required >

<br>

<!-- <p style="font-size:15px; font-weight:bold;">

Product Url Example : navy-blue-t-shirt

</p> -->

</div>

</div>
<!-- form-group Ends -->


<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Select A Manufacturer </label>

<div class="col-md-6" >

<select class="form-control" name="manufacturer_id"><!-- select manufacturer Starts -->

<option> Select  a Product Category </option>

@foreach($manufacturers as $key=>$value)
         <option value="{{$key}}"{{$edit_manufacturer->id==$key?' selected':''}}>{{$value}}</option>
                                    


@endforeach






</select><!-- select manufacturer Ends -->

</div>

</div><!-- form-group Ends -->


<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Category </label>

<div class="col-md-6" >

<select name="p_cat_id" class="form-control" >

<option> Select  a Product Category </option>
@foreach($product_category as $key=>$value)
                                    <option value="{{$key}}"{{$edit_p_cats->id==$key?' selected':''}}>{{$value}}</option>
                                    


@endforeach






</select>

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Category </label>

<div class="col-md-6" >


<select name="cat_id" class="form-control" >
<option> Select  a  Category </option>
<option>
@foreach($categories as $key=>$value)
                                    <option value="{{$key}}"{{$edit_category->id==$key?' selected':''}}>{{$value}}</option>
                                    <?php
                                        if($key!=0){
                                            $sub_categories=DB::table('categories')->select('id','name')->where('parent_id',$key)->get();
                                            if(count($sub_categories)>0){
                                                foreach ($sub_categories as $sub_category){
                                                    echo '<option value="'.$sub_category->id.'">&nbsp;&nbsp;--'.$sub_category->name.'</option>';
                                                }
                                            }
                                        }
                                    ?>
                                @endforeach

</option>


                               
                         
</select>

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Image  </label>

<div class="col-md-6" >

<input type="file" name="image" class="form-control">
<span class="text-danger">{{$errors->first('image')}}</span>
                            @if($edit_product->image!='')
                                &nbsp;&nbsp;&nbsp;
                                <a href="javascript:" rel="{{$edit_product->id}}" rel1="delete-image" class="btn btn-danger btn-mini deleteRecord">Delete Old Image</a>
                                <img src="{{url('products/small/',$edit_product->image)}}" width="35" alt="">
                            @endif
</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Price </label>

<div class="col-md-6" >

<input type="text" name="product_price" class="form-control" value="{{$edit_product->product_price}}" required >

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Sale Price </label>

<div class="col-md-6" >

<input type="text" name="psale_price" class="form-control" value="{{$edit_product->psale_price}}" required >

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Keywords </label>

<div class="col-md-6" >

<input type="text" name="product_keyword" class="form-control" value="{{$edit_product->product_keyword}}" required >

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Description </label>

<div class="col-md-6" >

<textarea name="product_desc" class="form-control" rows="5" column="5" id="product_desc"  required> {{$edit_product->product_desc}}</textarea>

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Feature </label>

<div class="col-md-6" >

<textarea name="product_feature" class="form-control" rows="5" column="5" id="product_feature" required> {{$edit_product->product_feature}} </textarea>
</div>

</div><!-- form-group Ends -->





<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Label </label>

<div class="col-md-6" >

<input type="text" name="product_label" class="form-control" value="{{$edit_product->product_label}}" required >

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" ></label>

<div class="col-md-6" >

<input type="submit" name="submit" value="Update Product" class="btn btn-primary form-control" >

</div>

</div><!-- form-group Ends -->

</form><!-- form-horizontal Ends -->

</div><!-- panel-body Ends -->

</div><!-- panel panel-default Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 2 row Ends --> 

</div>


@endsection


@section('jsblock')
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/jquery.ui.custom.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-colorpicker.js')}}"></script>
    <script src="{{asset('js/jquery.toggle.buttons.js')}}"></script>
    <script src="{{asset('js/masked.js')}}"></script>
    <script src="{{asset('js/jquery.uniform.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script src="{{asset('js/matrix.js')}}"></script>
    <script src="{{asset('js/matrix.form_common.js')}}"></script>
    <script src="{{asset('js/wysihtml5-0.3.0.js')}}"></script>
    <script src="{{asset('js/jquery.peity.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-wysihtml5.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script>
        $(".deleteRecord").click(function () {
            var id=$(this).attr('rel');
            var deleteFunction=$(this).attr('rel1');
            swal({
                title:'Are you sure?',
                text:"You won't be able to revert this!",
                type:'warning',
                showCancelButton:true,
                confirmButtonColor:'#3085d6',
                cancelButtonColor:'#d33',
                confirmButtonText:'Yes, delete it!',
                cancelButtonText:'No, cancel!',
                confirmButtonClass:'btn btn-success',
                cancelButtonClass:'btn btn-danger',
                buttonsStyling:false,
                reverseButtons:true
            },function () {
                window.location.href="/admin/"+deleteFunction+"/"+id;
            });
        });
        $('.textarea_editor').wysihtml5();
    </script>
@endsection