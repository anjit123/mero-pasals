@extends('backEnd.layouts.master')
@section('title','Add Coupon')

@section('content')
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->

<div class="container">


<div class="row"><!-- 1 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<ol class="breadcrumb"><!-- breadcrumb Starts -->

<li class="active">

<i class="fa fa-dashboard"> </i> Dashboard / Insert Coupon

</li>

</ol><!-- breadcrumb Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 1 row Ends -->

<div class="row"><!-- 2 row Starts --->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<div class="panel panel-default"><!-- panel panel-default Starts -->

<div class="panel-heading"><!-- panel-heading Starts -->

<h3 class="panel-title"><!-- panel-title Starts -->

<i class="fa fa-money fa-fw"> </i> Insert Coupon

</h3><!-- panel-title Ends -->

</div><!-- panel-heading Ends -->

<div class="panel-body"><!--panel-body Starts -->

<form class="form-horizontal" action="{{route('coupon.update',$couponDetails->id)}}" method="post" ><!-- form-horizontal Starts -->
<input type="hidden" name="_token" value="{{csrf_token()}}">
{{method_field("PUT")}}

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label"> Coupon Code </label>

<div class="col-md-6">

<input type="text" name="coupon_code" class="form-control" minlength="5" maxlength="15" value="{{$couponDetails->coupon_code}}" required >

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label"> Coupon Amount </label>

<div class="col-md-6">

<input type="number" name="amount" class="form-control" value="{{$couponDetails->amount}}" required>

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label"> Amount Type </label>

<div class="col-md-6">
<select name="amount_type" class="form-control">

<option @if($couponDetails->amount_type=="Percentage") selected @endif value="Percentage" > Percentage </option>

<option @if($couponDetails->amount_type=="Fixed") selected @endif value="Fixed">Fixed</option>
</select>
</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label"> Expiry Date </label>

<div class="col-md-6">

<input value="{{$couponDetails->expiry_date}}" type="Date" name="expiry_date" id="expiry_date" class="form-control ">

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label">Enable</label>

<div class="col-md-6">
<input type="checkbox" name="status" value="1" class="control-group" @if($couponDetails->status=="1") checked @endif>

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label"> </label>

<div class="col-md-6">

<input type="submit" name="submit" class=" btn btn-primary form-control" value=" Update Coupon ">

</div>

</div><!-- form-group Ends -->

</form><!-- form-horizontal Ends -->

</div><!--panel-body Ends -->

</div><!-- panel panel-default Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 2 row Ends --->



</div>

@endsection