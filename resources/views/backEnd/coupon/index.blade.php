@extends('backEnd.layouts.master')
@section('title','View Coupon')

@section('content')

<div class="container">

    
<div class="row"><!-- 1 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<ol class="breadcrumb"><!-- breadcrumb Starts -->

<li class="active">

<i class="fa fa-dashboard"></i> Dashboard / View Coupons

</li>

</ol><!-- breadcrumb Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 1 row Ends -->

<div class="row" ><!-- 2 row Starts -->

<div class="col-lg-12" ><!-- col-lg-12 Starts -->

<div class="panel panel-default"><!-- panel panel-default Starts -->

<div class="panel-heading"><!-- panel-heading Starts -->

<h3 class="panel-title"><!-- panel-title Starts -->

<i class="fa fa-money fa-fw"></i> View Coupons

</h3><!-- panel-title Ends -->

</div><!-- panel-heading Ends -->

@if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                <strong>Well done!</strong> {{Session::get('message')}}
            </div>
        @endif
<div class="panel-body"><!-- panel-body Starts -->

<div class="table-responsive"><!-- table-responsive Starts -->

<table class="table table-bordered table-hover table-striped"><!-- table table-bordered table-hover table-striped Starts -->

<thead><!-- thead Starts -->

<tr>

<th>Coupon Id : </th>
<th>Coupon Code : </th>
<th>Product Amount : </th>
<th>Coupon Amount type : </th>
<th>Coupon Expiry Date : </th>
<th>Coupon Created Date : </th>
<th>Coupon status : </th>
<th>Delete Coupon : </th>
<th>Edit Coupon : </th>

</tr>

</thead><!-- thead Ends -->

<tbody><!-- tbody Starts -->


@foreach($coupons as $coupon)
<tr>

<td>{{$coupon->id}}</td>

<td>{{$coupon->coupon_code}}</td>

<td>{{$coupon->amount}}
@if($coupon->amount_type=="Percentage") 
    % 
@else 
    NRS @endif
</td>

<td>{{$coupon->amount_type}}
</td>

<td>{{$coupon->expiry_date}}</td>

<td>{{$coupon->created_at}}</td>
<td>
@if($coupon->status==1) Active @else Inactive @endif
</td>


<td>

<a href="javascript:" rel="{{$coupon->id}}" rel1="delete-coupon" class="btn-mini deleteRecord">  <i class="fa fa-trash-o" > </i> Delete</a>


</td>

<td>

<a href="{{route('coupon.edit',$coupon->id)}}"><i class="fa fa-pencil" > </i> Edit </a>


</td>

</tr>

@endforeach

</tbody><!-- tbody Ends -->

</table><!-- table table-bordered table-hover table-striped Ends -->

</div><!-- table-responsive Ends -->

</div><!-- panel-body Ends -->

</div><!-- panel panel-default Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 2 row Ends -->




</div>

@endsection

@section('jsblock')
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/jquery.ui.custom.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.uniform.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/matrix.js')}}"></script>
    <script src="{{asset('js/matrix.tables.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script>
        $(".deleteRecord").click(function () {
           var id=$(this).attr('rel');
           var deleteFunction=$(this).attr('rel1');
           swal({
               title:'Are you sure?',
               text:"You won't be able to revert this!",
               type:'warning',
               showCancelButton:true,
               confirmButtonColor:'#3085d6',
               cancelButtonColor:'#d33',
               confirmButtonText:'Yes, delete it!',
               cancelButtonText:'No, cancel!',
               confirmButtonClass:'btn btn-success',
               cancelButtonClass:'btn btn-danger',
               buttonsStyling:false,
               reverseButtons:true
           },function () {
              window.location.href="/admin/"+deleteFunction+"/"+id;
           });
        });
    </script>
@endsection