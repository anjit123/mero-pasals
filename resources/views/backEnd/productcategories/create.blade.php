@extends('backEnd.layouts.master')
@section('title','Add Product Category')
@section('content')

<div class="container">


<div class="row"><!-- 1 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<ol class="breadcrumb"><!-- breadcrumb Starts -->

<li>

<i class="fa fa-dashboard"></i> Dashboard / Insert Products Category

</li>

</ol><!-- breadcrumb Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 1 row Ends -->

<div class="row"><!-- 2 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<div class="panel panel-default"><!-- panel panel-default Starts -->

<div class="panel-heading" ><!-- panel-heading Starts -->

<h3 class="panel-title" ><!-- panel-title Starts -->

<i class="fa fa-money fa-fw" ></i> Insert Product Category

</h3><!-- panel-title Ends -->


</div><!-- panel-heading Ends -->


<div class="panel-body" ><!-- panel-body Starts -->

<form class="form-horizontal" action="{{route('productcategory.store')}}" method="post" enctype="multipart/form-data" ><!-- form-horizontal Starts -->
<input type="hidden" name="_token" value="{{csrf_token()}}">

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" >{{ __(' Product Category Title') }}</label>

<div class="controls col-md-6" >

<input type="text" name="p_cat_name" class="form-control @error('p_cat_name') is-invalid @enderror" value="{{ old('p_cat_name') }}" autofocus>

                @error('p_cat_name')
                                    <span class="invalid-feedback" role="alert" style="color:red">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" >Show as Top Product Category</label>

<div class="controls col-md-6" >

<input type="radio" name="p_cat_top" value="yes" >

<label> Yes </label>

<input type="radio" name="p_cat_top" value="no" >

<label> No </label>

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Select Product Category Image</label>

<div class="controls col-md-6" >

<input type="file" name="p_cat_image" class="form-control" >
<span class="text-danger" id="" style="color: red;">{{$errors->first('p_cat_image')}}</span>

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" ></label>

<div class="controls col-md-6" >

<input type="submit" name="submit" value="Insert Product Category" class="btn btn-primary form-control" >

</div>

</div><!-- form-group Ends -->

</form><!-- form-horizontal Ends -->

</div><!-- panel-body Ends -->


</div><!-- panel panel-default Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 2 row Ends -->




</div>

@endsection