@extends('backEnd.layouts.master')
@section('title','View Orders')
@section('content')
<br><br><br>

<div class="container">


<h1>Order Number {{$order->id}} </h1>
<br><br><br>
    <div class="row">
    @if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                <strong></strong> {{Session::get('message')}}
            </div>
        @endif
    
        <div class="col-sm-5">
        
        <table class="table table-responsive">
    <tbody>
        <tr>
          <th> <h1>Order Details</h1> </th>
           <th></th> 
          	
        </tr>
        <tr>
          <th>Order ID</th>
            <td>{{$order->id}}</td>
        </tr>
        <tr>
          <th>Order Date </th>
            <td>{{$order->created_at}}</td>
        </tr>
        <tr>
          <th>Order Status</th>
            <td>{{$order->order_status}}</td>
        </tr>
        <tr>
          <th>Order Total</th>
            <td>$ {{$order->grand_total}}</td>
        </tr>
        <tr>
          <th>Shipping Charges</th>
            <td>$ {{$order->shipping_charges}}</td>
        </tr>
        <tr>
          <th>Coupon Code</th>
            <td>{{$order->coupon_code}}</td>
        </tr>
        <tr>
          <th>Coupon Amount</th>
            <td>$ {{$order->coupon_amount}}</td>
        </tr>
        <tr>
          <th>Payment Method</th>
            <td>{{$order->payment_method}}</td>
        </tr>
       
      
    </tbody>
</table>

    <table class="table table-responsive">
        <thead>
            <th></th>
        </thead>
        <tbody>
        <tr></tr>
        <tr>
       
            <td><br>
            

            
            </td>
        </tr>
        
        </tbody>
    
    </table>
        
        

        </div>

        <div class="col-sm-2">
        </div>
        <div class="col-sm-5">
        
            <div class="table-responsive">
                <table class="table">
                    <tbody>
                        <tr>
                        <th> <h1>Customer Details</h1> </th>
                        <th></th>
                            
                        </tr>

                        <tr>
                            <th>Customer Name</th>
                            <td>{{$order->name}}</td>
                        </tr>

                        <tr>
                            <th>Customer Email</th>
                            <td>{{$order->users_email}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="container">
            <h3>Update Order Status</h3>
        </div>
        <form action="{{route('orders.update',$order->id)}}" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
       
{{method_field("PUT")}}
        <div class="form-group">
        <select name="order_status" id="order_status" class="form-control" required>
                <option value="New">New</option>
                <option value="Pending">Pending</option>
                <option value="In Process">In Process</option>
                <option value="Cancelled">Cancelled</option>
                <option value="Shipped">Shipped</option>
                <option value="Delivered">Delivered</option>
                
            </select>
        </div>
           
            <div class="form-group">
                 <input type="submit" class="btn btn-success" value="Update Status"> 
            </div>        
        </form>
        </div>
        
        </div>
    
    </div>


</div>

@endsection