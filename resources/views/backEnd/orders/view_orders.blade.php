@extends('backEnd.layouts.master')
@section('title','View Orders')
@section('content')

<br><br><br>
<div class="container">


<div class="row" ><!-- box Starts -->



<center><!-- center Starts -->

<h1>Customer Orders</h1>

<p class="lead"> <h2>All orders on one place.</h2> </p>

<p class="text-muted" >


</p>


</center><!-- center Ends -->
@if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                <strong>Well done!</strong> {{Session::get('message')}}
            </div>
        @endif
<hr>




<form class="navbar-form" method="post" action="{{ route('admin.search.order') }}"><!-- navbar-form Starts -->
        {{csrf_field() }}
        <div class="input-group"><!-- input-group Starts -->

            <input class="form-control" type="text" placeholder="Search" name="order" placeholder="Keyword"  required>

            <span class="input-group-btn"><!-- input-group-btn Starts -->

                    <button type="submit" value="Search" name="search" class="btn btn-primary">

                    <i class="fa fa-search"></i>

                    </button>

            </span><!-- input-group-btn Ends -->

        </div><!-- input-group Ends -->

    </form><!-- navbar-form Ends -->

<!-- 
   <div class="date-range">
        <div class="row">
        
            <div class="col-md-4">
            <label for="start_date" class="control-label">Start</label>
                <input type="date" name="start_date" id="start_date" class="form-control">
            </div>
            <div class="col-md-4">
                <label for="end_date" class="control-label">End</label>
                <input type="date" name="end_date" id="end_date" class="form-control">
            </div>
            <div class="col-md-4">
            <label for="" class="control-label"></label>
                <button type="submit" class="form-control btn btn-primary">Filter</button>
            </div>
        
        </div>
   </div> -->

<br><br>
<div class="table" ><!-- table-responsive Starts -->

<table class="table table-bordered table-responsive" ><!-- table table-bordered table-hover Starts -->

<thead><!-- thead Starts -->

<tr>
<td>Order ID:</td>
<td>Email</td>
<td>Ordered Products:</td>
<td>Payment Method</td>
<td>Grand Total</td>
<td>Status</td>
<td>Created on</td>
<td>Action</td>
<td>Action</td>

</tr>

</thead><!-- thead Ends -->

<tbody><!--- tbody Starts --->


@foreach($orders as $order)
<tr><!-- tr Starts -->

<td>{{$order->id}}</td>
  
    <td>{{ $order->users_email}}</td>
   
<td>
    @foreach($order->orderss as $pro)
        <a href="{{url('/orders/'.$order->id) }}">{{$pro->product_code}}</a> <br>
        {{$pro->product_title}} <br>
        {{$pro->product_size}} <br>
        <!-- {{$pro->product_qty}} <br> -->
    @endforeach
</td>
<td>{{$order->payment_method}}</td>
<td>{{$order->grand_total}}</td>
<td>{{$order->order_status}}</td>
<td>{{ $order->created_at->format ('l j F Y')}}</td>


<td>
<a href="javascript:" rel="{{$order->id}}" rel1="delete-order" class="btn-mini deleteRecord">  <i class="fa fa-trash-o" > </i> Delete</a>

<a href="{{route('orders.edit',$order->id)}}"><i class="fa fa-pencil" > </i> Edit </a>

</td>
<td>
<a href="{{route('order.invoice',$order->id)}}" target="blank" class="btn btn-info btn-sm" > View Ordered Invoice </a>

</td>


</tr><!-- tr Ends -->
@endforeach

</tbody><!--- tbody Ends --->


</table><!-- table table-bordered table-hover Ends -->
{{$orders->links()}}
</div><!-- table-responsive Ends -->
</div>



</div><!-- box Ends -->



</div>



@endsection

@section('jsblock')
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/jquery.ui.custom.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.uniform.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/matrix.js')}}"></script>
    <script src="{{asset('js/matrix.tables.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script>
        $(".deleteRecord").click(function () {
           var id=$(this).attr('rel');
           var deleteFunction=$(this).attr('rel1');
           swal({
               title:'Are you sure?',
               text:"You won't be able to revert this!",
               type:'warning',
               showCancelButton:true,
               confirmButtonColor:'#3085d6',
               cancelButtonColor:'#d33',
               confirmButtonText:'Yes, delete it!',
               cancelButtonText:'No, cancel!',
               confirmButtonClass:'btn btn-success',
               cancelButtonClass:'btn btn-danger',
               buttonsStyling:false,
               reverseButtons:true
           },function () {
              window.location.href="/admin/"+deleteFunction+"/"+id;
           });
        });
    </script>
    <script>
        $('.datepicker').datepicker({
weekdaysShort: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
showMonthsShort: true
})
    </script>
@endsection