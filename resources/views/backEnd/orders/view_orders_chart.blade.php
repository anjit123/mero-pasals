<?php 

    $current_month = date('M');
    $last_month = date('M', strtotime("-1 month"));
    $last_to_last_month = date('M', strtotime("-2 month"));
?>

@extends('backEnd.layouts.master')
@section('title','View Order chart')
@section('content')

<br><br><br>



<div class="container">

    
<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	theme: "light2", // "light1", "light2", "dark1", "dark2"
	title:{
		text: "Orders Reporting"
	},
	axisY: {
		title: "Numbers of Orders"
	},
	data: [{        
		type: "column",  
		showInLegend: true, 
		legendMarkerColor: "grey",
		legendText: "Last 3 Months",
		dataPoints: [      
			{ y: <?php echo $current_month_orders; ?>, label: "<?php echo $current_month; ?>" },
			{ y: <?php echo $last_month_orders; ?>,  label: "<?php echo $last_month; ?>" },
			{ y: <?php echo $last_to_last_month_orders; ?>,  label: "<?php echo $last_to_last_month; ?>" },
		
		]
	}]
});
chart.render();

}
</script>

<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>


</div>
@endsection