<?php 
use App\Product;
$cartCount = Product::cartCount();
?>
<header id="header"><!--header-->
    <div class="header_top" style="background:black;"><!--header_top-->
    <div class="container">
 <div class="row">
                <div class="col-sm-6">
                    <div class="contactinfo">
                        <ul class="nav nav-pills">
                            <li><a href="#"><i class="fa fa-phone"></i> +977 9810220552</a></li>
                            <li><a href="#"><i class="fa fa-envelope"></i> meropasal@gmail.com</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                <div class="social-icons pull-right">
                <p class="social-icons"><!-- social Starts --->

                <a href="https://www.facebook.com" style="margin:5px;"><i class="fa fa-facebook"></i></a>
                <a href="https://www.twitter.com"  style="margin:5px;"><i class="fa fa-twitter"></i></a>
                <a href="https://www.instagram.com"  style="margin:5px;"><i class="fa fa-instagram"></i></a>
                <a href="https://www.google.com"  style="margin:5px;"><i class="fa fa-google-plus"></i></a>
                <a href="https://mail.google.com"  style="margin:5px;"><i class="fa fa-envelope"></i></a>

                </p><!-- social Ends --->
                    </div>
                </div>
            </div>
            </div>
            </div>
</div>

           
<nav class="navbar navbar-expand-md navbar-light bg-light shadow mb-3">
    <div class="container-fluid">
        <!-- <a href="#" class="navbar-brand mr-3">Mero Pasal</a> -->
        <div class="logo pull-left pr-3">
                        <a href="{{url('/')}}"><img src="{{asset('frontEnd/images/logo.png')}}" alt=""  style="width:120px; background:#ffffff"/></a>
                    </div>
         @if(Auth::check())           
            <a href="#" class="btn btn-info btn-sm" disabled>Welcome :  {{ Auth::user()->name }}</a>
        @else
            <a href="#" class="btn btn-info btn-sm" disabled>Welcome :Guest</a>
        @endif

        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav">
               
            </div>
            <div class="navbar-nav ml-auto pull-right">
                
               
                @if(Auth::check())
                <a href="#" class="nav-item nav-link active">Home |</a>
                <a href="{{route('wish_list')}}" class="nav-item nav-link">MyWishList |</a>
                <a href="{{route('user_account')}}" class="nav-item nav-link ">My Account |</a>
                <a href="{{url('/service')}}" class="nav-item nav-link">Services |</a>
                <a href="{{url('/about')}}" class="nav-item nav-link">About |</a>
                <a href="{{url('/contact/create')}}" class="nav-item nav-link">Contact |</a>
                
                <a href="{{ route('user_logout') }}" class="nav-item nav-link"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                @else
                <a href="#" class="nav-item nav-link active">Home |</a>
                <!-- <a href="{{ route('user_register') }}" class="nav-item nav-link">Register</a> -->
                <a href="{{ route('register') }}" class="nav-item nav-link">Register  |</a>
                <a href="{{ route('user_login') }}" class="nav-item nav-link"><i class="fa fa-sign-in" aria-hidden="true"></i>Login</a>
                @endif

               
              
            </div>

        </div>
    </div> 
   
    
</nav>

<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation"  >

<span class="sr-only" >Toggle Navigation </span>

<i class="fa fa-align-justify"></i>

</button>

<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#search" >

<span class="sr-only" >Toggle Search</span>

<i class="fa fa-search" ></i>

</button>


</div><!-- navbar-header Ends -->

<div class="navbar-collapse collapse" id="navigation" ><!-- navbar-collapse collapse Starts -->

<div class="padding-nav" ><!-- padding-nav Starts -->

<ul class="nav navbar-nav navbar-left"><!-- nav navbar-nav navbar-left Starts -->

<div class="padding-nav pull-left">
    <ul class="nav navbar-nav navbar-left pl-5">
    <div class="logo pull-left pr-3 pb-3" id="nab">
                        <a href="{{url('/')}}"><img src="{{asset('frontEnd/images/logo.png')}}" alt=""  style="width:120px; background:#ffffff"/></a>
                        <a href="{{url('/')}}" class="pl-4" style="font-size:15px; "> HOME</a>
                        <a href="{{route('shop.index')}}" class="pl-4" style="font-size:15px; "> SHOP</a>
                        <a href="{{route('user_account')}}" class="pl-4" style="font-size:15px; "> MY ACCOUNT</a>
                        <a href="{{url('/viewcart')}}" class="pl-4" style="font-size:15px; "> SHOPPING CART</a>
                        <a href="{{url('/about')}}" class="pl-4" style="font-size:15px; "> ABOUT US</a>
                        <a href="{{url('/service')}}" class="pl-4" style="font-size:15px; "> SERVICES</a>
                        <a href="{{route('contact.create')}}" class="pl-4" style="font-size:15px; "> CONTACT US</a>
                        <a href="{{route('wish_list')}}"> <i class="fa fa-heart"></i> MY WISH LIST </a>

                        
            
                    </div>
    </ul>

   
</div>
</ul><!-- nav navbar-nav navbar-left Ends -->

</div><!-- padding-nav Ends -->

<a class="btn btn-primary navbar-btn right" href="{{url('/viewcart')}}"><!-- btn btn-primary navbar-btn right Starts -->


          

<span> {{ $cartCount }} <i class="fa fa-shopping-cart"></i>Carts </span>

</a><!-- btn btn-primary navbar-btn right Ends -->

<div class="navbar-collapse collapse right"><!-- navbar-collapse collapse right Starts -->

<button class="btn navbar-btn btn-primary" type="button" data-toggle="collapse" data-target="#search">

<span class="sr-only">Toggle Search</span>

<i class="fa fa-search"></i>

</button>

</div><!-- navbar-collapse collapse right Ends -->

<div class="collapse clearfix" id="search"><!-- collapse clearfix Starts -->

<form class="navbar-form" method="post" action="{{ route('search') }}"><!-- navbar-form Starts -->
{{csrf_field() }}
<div class="input-group"><!-- input-group Starts -->

<input class="form-control" type="text" placeholder="Search" name="product" placeholder="Keyword"  required>

<span class="input-group-btn"><!-- input-group-btn Starts -->

<button type="submit" value="Search" name="search" class="btn btn-primary">

<i class="fa fa-search"></i>

</button>

</span><!-- input-group-btn Ends -->
<span class="text-danger" id="" style="color: red;">{{$errors->first('product')}}</span>

</div><!-- input-group Ends -->

</form><!-- navbar-form Ends -->

</div><!-- collapse clearfix Ends -->

</div><!-- navbar-collapse collapse Ends -->

</div><!-- container Ends -->
</div><!-- navbar navbar-default Ends -->





