
<div id="footer"><!-- footer Starts -->
<div class="container"><!-- container Starts -->

<div class="row" ><!-- row Starts -->

<div class="col-md-3 col-sm-6" ><!-- col-md-3 col-sm-6 Starts -->

<h4>Pages</h4>

<ul><!-- ul Starts -->

<li><a href="{{url('/viewcart')}}">Shopping Cart</a></li>

<li><a href="{{route('contact.create')}}">Contact Us</a></li>

<li><a href="{{route('shop.index')}}">Shop</a></li>

<li>



</li>


</ul><!-- ul Ends -->



<ul><!-- ul Starts -->

<li>



</li>

<li><a href="{{ route('register') }}">Register</a></li>

<li><a href="#">Terms And Conditions </a></li>



</ul><!-- ul Ends -->

<hr class="hidden-md hidden-lg hidden-sm" >

</div><!-- col-md-3 col-sm-6 Ends -->

<div class="col-md-3 col-sm-6"><!-- col-md-3 col-sm-6 Starts -->

<h4> Top Products Categories </h4>

<?php $categories = DB::table('categories')->where('cat_top','yes')->orderBy('name','ASC')->get(); ?>
@foreach($categories as $category)
<ul><!-- ul Starts -->
    {{$category->name}}

</ul><!-- ul Ends -->
@endforeach
<hr class="hidden-md hidden-lg">

</div><!-- col-md-3 col-sm-6 Ends -->


<div class="col-md-3 col-sm-6"><!-- col-md-3 col-sm-6 Starts -->

<h4>Where to find us</h4>

<p><!-- p Starts -->
<strong>Shrestha Pasal pvt.ltd</strong>
<br>Tumlingtar-09
<br>Sankhuwasava
<br>+977-9810220552
<br>Shresthaanjit@gmail.com
<br>
<strong>Anjit Shrestha</strong>

</p><!-- p Ends -->

<a href="{{route('contact.create')}}">Go to Contact us page</a>

<hr class="hidden-md hidden-lg">

</div><!-- col-md-3 col-sm-6 Ends -->

<div class="col-md-3 col-sm-6"><!-- col-md-3 col-sm-6 Starts -->

<h4>Get the news</h4>

<p class="text-muted">

</p>

<form action="https://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('https://feedburner.google.com/fb/a/mailverify?uri=computerfever', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true"><!-- form Starts -->

<div class="input-group"><!-- input-group Starts -->

<input type="text" class="form-control" name="email">

<input type="hidden" value="computerfever" name="uri"/>
<input type="hidden" name="loc" value="en_US"/>

<span class="input-group-btn"><!-- input-group-btn Starts -->

<input type="submit" value="subscribe" class="btn btn-default">

</span><!-- input-group-btn Ends -->

</div><!-- input-group Ends -->

</form><!-- form Ends -->

<hr>

<h4> Stay in touch </h4>

<p class="social"><!-- social Starts --->

<a href="https://www.facebook.com"><i class="fa fa-facebook"></i></a>
<a href="https://www.twitter.com"><i class="fa fa-twitter"></i></a>
<a href="https://www.instagram.com"><i class="fa fa-instagram"></i></a>
<a href="https://www.google.com"><i class="fa fa-google-plus"></i></a>
<a href="https://mail.google.com"><i class="fa fa-envelope"></i></a>

</p><!-- social Ends --->

</div><!-- col-md-3 col-sm-6 Ends -->

</div><!-- row Ends -->

</div><!-- container Ends -->
</div><!-- footer Ends -->

<div id="copyright"><!-- copyright Starts -->

<div class="container" ><!-- container Starts -->

<div class="col-md-6" ><!-- col-md-6 Starts -->

<p class="pull-left"> &copy; 2020 Anjit Shrestha</p>

</div><!-- col-md-6 Ends -->

<div class="col-md-6" ><!-- col-md-6 Starts -->

<p class="pull-right" >

All Rights reserverd <a href="{{url('/')}}" >Mero Pasal</a>

</p>


</div><!-- col-md-6 Ends -->

</div><!-- container Ends -->

</div><!-- copyright Ends -->


