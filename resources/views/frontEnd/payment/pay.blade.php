@extends('frontend.layouts.master')
@section('title','Paypal Order')
@section('content')
<?php use App\Order;?>

 <?php 
  $orderDetails = Order::getOrderDetails(Session::get('order_id'));
//   $orderDetails = json_decode(json_encode($orderDetails));
//   echo "<pre>"; print_r($orderDetails); die;
    $nameArr = explode(' ',$orderDetails->name);
  ?>
<form action="https:://perfectmoney.is/api/step1.asp" method="post" class="payment-form">

    <input type="hidden" name="firstname" value="{{ $nameArr[0] }}">
    <input type="hidden" name="lastname" value="{{ $nameArr[1] }}">
    <input type="hidden" name="email" value="{{ $orderDetails->users_email }}">
    <input type="hidden" name="userId" value="{{ $orderDetails->users_id }}">
    <!-- <input type="hidden" name="countryId" value="USD"> -->
    <!-- <input type="hidden" name="address1" value="{{ $orderDetails->address }}"> -->
    <input type="hidden" name="city" value="{{ $orderDetails->city }}">
    <input type="hidden" name="state" value="{{ $orderDetails->state }}">
    <input type="hidden" name="total" value="{{ round(Session::get('grand_total'),2)}}">
    <!-- <input type="hidden" name="zip" value="{{ $orderDetails->pincode }}"> -->
    <input type="hidden" name="country" value="{{ $orderDetails->country }}">
    <input type="hidden" name="phone" value="{{ $orderDetails->contact }}">

@foreach($data as $n=$v)

<input type="hidden" name="{{$n}}" value="{{$v}}">
@endforeach

<input type="submit"  style='display:none'>

</form>

<script>
var form=document.querySelector('form.payment-form');
form.submit();

</script>


@endsection