
@extends('frontend.layouts.master')
@section('title','Paypal Order')
@section('content')
<?php use App\Order;?>
<div class="container">
<div class="jumbotron text-center">
  <h1 class="display-3">Your Order has Placed</h1>
  <p class="lead"><strong>Please check your email</strong> for further instructions on how to complete your account setup.</p>
  <p class="lead"><strong> Your Order number is {{Session::get('order_id')}}</strong> and total paypable about in $
  {{Session::get('grand_total')}}
  </p>
  <hr>
  <p>
    Having trouble? <a href="{{url('/contact/create')}}">Contact us</a>
  </p>
  <p>Please make payment by Clicking on below Payment Button</p>
  <?php 
  $orderDetails = Order::getOrderDetails(Session::get('order_id'));
  // $orderDetails = json_decode(json_encode($orderDetails));
  // echo "<pre>"; print_r($orderDetails); die;
    // $nameArr = explode(' ',$orderDetails->name);
  ?>
  <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post"><!-- form Starts -->


    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" value="ecom-store@business.com">

    <input type="hidden" name="hosted_button_id" value="6RNT8A4HBBJRE">

    <input type="hidden" name="item_name" value="{{ Session::get('order_id')}}">
    <!-- <input type="hidden" name="item_number" value="{{ Session::get('order_id')}}"> -->
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="amount" value="{{ round(Session::get('grand_total'),2)}}">
   
    <input type="hidden" name="address1" value="{{ $orderDetails->address }}">
    <!-- <input type="hidden" name="address2" value=""> -->
    <input type="hidden" name="city" value="{{ $orderDetails->city }}">
    <input type="hidden" name="state" value="{{ $orderDetails->state }}">
    <input type="hidden" name="zip" value="{{ $orderDetails->pincode }}">
    <input type="hidden" name="email" value="{{ $orderDetails->users_email }}">
    <input type="hidden" name="country" value="{{ $orderDetails->country }}">
    <input type="hidden" name="return" value="{{ url('paypal/thanks')}}">
    <input type="hidden" name="cancel_return" value="{{ url('paypal/cancel')}}">

      <!-- <script src="https://www.paypal.com/sdk/js?client-id=sb"></script>
      <script>paypal.Buttons().render('body');</script> -->


    <!-- <input type="image" src="" alt="Buy Now"> -->
    <button type="submit">Buy Now (PayPal)</button>
    <img src="https://paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1" alt="">
    
   
    <!-- <input type="submit" value="Pay Now"> -->
    

</form>
  <p class="lead">
    <a class="btn btn-primary btn-sm" href="https://bootstrapcreative.com/" role="button">Continue to homepage</a>
  </p>
  
</div>

</div>

@endsection
<?php 
Session::forget('grand_total');
Session::forget('order_id');

?>


<!-- MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=d0dcfca533bba2
MAIL_PASSWORD=3270fe5b94b2ae
MAIL_FROM_ADDRESS=from@example.com
MAIL_FROM_NAME="${APP_NAME}" -->