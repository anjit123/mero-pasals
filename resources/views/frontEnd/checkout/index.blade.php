@extends('frontend.layouts.master')
@section('title','CheckOut')
@section('content')

<div class="container">
@if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                {{Session::get('message')}}
            </div>
        @endif

        <div class="row form-control">
            <form action="{{url('/submit-checkout')}}" method="post" class="form-horizontal">
                <div class="col-sm-4 col-sm-offset-1">
                    <div class="login-form"><!--login form-->
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <legend>Billing To</legend>
                        <div class="form-group {{$errors->has('billing_name')?'has-error':''}}">
                            <input type="text" class="form-control" name="billing_name" id="billing_name" value="{{$user_login->name}}" placeholder="Billing Name">
                            <span class="text-danger">{{$errors->first('billing_name')}}</span>
                        </div>
                        <div class="form-group {{$errors->has('billing_address')?'has-error':''}}">
                            <input type="text" class="form-control" value="{{$user_login->address}}" name="billing_address" id="billing_address" placeholder="Billing Address">
                            <span class="text-danger">{{$errors->first('billing_address')}}</span>
                        </div>
                        <div class="form-group {{$errors->has('billing_city')?'has-error':''}}">
                            <input type="text" class="form-control" name="billing_city" value="{{$user_login->city}}" id="billing_city" placeholder="Billing City">
                            <span class="text-danger">{{$errors->first('billing_city')}}</span>
                        </div>
                        <div class="form-group {{$errors->has('billing_state')?'has-error':''}}">
                            <input type="text" class="form-control" name="billing_country" value="{{$user_login->country}}" id="billing_country" placeholder=" Billing Country">
                            <span class="text-danger">{{$errors->first('billing_country')}}</span>
                        </div>
                        
                        <div class="form-group {{$errors->has('billing_mobile')?'has-error':''}}">
                            <input type="text" class="form-control" name="billing_contact" value="{{$user_login->contact}}" id="billing_contact" placeholder="Billing Contact">
                            <span class="text-danger">{{$errors->first('billing_mobile')}}</span>
                        </div>

                        <div class="form-check">
                        <span>
                        <!-- <input type="checkbox" value="" class="checkbox" name="checkme" id="bill2ship">Shipping Address same as Billing Address -->
                        </span>
                        
                        </div>
                    </div><!--/login form-->
                </div>
                <div class="col-sm-1">

                </div>
                <div class="col-sm-6">
                    <div class="signup-form"><!--sign up form-->
                        <legend>Shipping To</legend>
                        <div class="form-group {{$errors->has('shipping_name')?'has-error':''}}">
                            <input type="text" class="form-control" name="shipping_name" id="shipping_name" value="{{$user_login->name}}" placeholder="Shipping Name">
                            <span class="text-danger">{{$errors->first('shipping_name')}}</span>
                        </div>
                        <div class="form-group {{$errors->has('shipping_address')?'has-error':''}}">
                            <input type="text" class="form-control" value="" name="shipping_address" id="shipping_address" value="{{$user_login->address}}" placeholder="Shipping Address">
                            <span class="text-danger">{{$errors->first('shipping_address')}}</span>
                        </div>
                        <div class="form-group {{$errors->has('shipping_city')?'has-error':''}}">
                            <input type="text" class="form-control" name="shipping_city" value="" id="shipping_city" placeholder="Shipping City">
                            <span class="text-danger">{{$errors->first('shipping_city')}}</span>
                        </div>
                        <div class="form-group {{$errors->has('shipping_state')?'has-error':''}}">
                            <input type="text" class="form-control" name="shipping_state" value="" id="shipping_state" placeholder="Shipping State">
                            <span class="text-danger">{{$errors->first('shipping_state')}}</span>
                        </div>
                        <div class="form-group {{$errors->has('shipping_country')?'has-error':''}}">
                            <input type="text" class="form-control" name="shipping_country" value="" id="shipping_country" placeholder="Shipping Country">
                            <span class="text-danger">{{$errors->first('shipping_country')}}</span>
                        </div>

                        <div class="form-group {{$errors->has('shipping_pincode')?'has-error':''}}">
                            <input type="text" class="form-control" name="shipping_pincode" value="" id="shipping_pincode" placeholder="Shipping Pincode/Zipcode">
                            <span class="text-danger">{{$errors->first('shipping_pincode')}}</span>
                        </div>
                        <div class="form-group {{$errors->has('shipping_mobile')?'has-error':''}}">
                            <input type="text" class="form-control" name="shipping_contact" value="" id="shipping_contact" placeholder="Shipping Contact">
                            <span class="text-danger">{{$errors->first('shipping_mobile')}}</span>
                        </div>
                        <!-- <div class="form-group {{$errors->has('shipping_mobile')?'has-error':''}}">
                            <input type="date" class="form-control" name="order_date" value="" id="order_date" placeholder="Shipping Date">
                            <span class="text-danger">{{$errors->first('shipping_mobile')}}</span>
                        </div> -->
                        <button type="submit" class="btn btn-primary" style="float: right;">CheckOut</button>
                    </div><!--/sign up form-->
                </div>
            </form>
        </div>
    </div>
    <div style="margin-bottom: 20px;"></div>

</div>

<script>


</script>


@endsection