@extends('frontend.layouts.master')
@section('title','mero-Pasal')
@section('content')


<div class="container">
    <div class="row mb-3">
        <img src="/frontEnd/images/1.jpg" class="w-100 ml-5 mt-3" alt="">
    </div>

    <div class="row mb-3">
    <div class="col">
        <h1 class="display-4 text-center">About</h1> <br>
        <h4>Designed and Developed  By Anjit Shrestha</h4>
        <h4 class="text-muted">mero-Pasal is a web based application that is developed in Laravel Framework. It is used to analysis the product review using the Sentiment Analysis.</h4>
    </div>

    </div>

    <div class="row mb-3">
        <img src="/frontEnd/images/2.jpg" class="w-100 ml-5 mt-3" alt="">
    </div>

    <div class="row mb-3">
        <img src="/frontEnd/images/3.jpg" class="w-100 ml-5 mt-3" alt="">
    </div>

    <div class="row mb-3">
        <img src="/frontEnd/images/4.jpg" class="w-100 ml-5 mt-3" alt="">
    </div>

    <div class="row mb-3">
        <img src="/frontEnd/images/5.jpg" class="w-100 ml-5 mt-3" alt="">
    </div>

    <div class="row mb-3">
        <img src="/frontEnd/images/6.jpg" class="w-100 ml-5 mt-3" alt="">
    </div>
</div>



@endsection
