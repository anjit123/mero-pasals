@extends('frontend.layouts.master')
@section('title','mero-Pasal')
@section('content')


<div class="container-fluid">

        <div class="container" id="slider"><!-- container Starts -->

            <div class="col-md-12"><!-- col-md-12 Starts -->

                <div id="myCarousel" class="carousel slide" data-ride="carousel"><!-- carousel slide Starts --->

                    <ol class="carousel-indicators"><!-- carousel-indicators Starts -->

                         <li data-target="#myCarousel" data-slide-to="0" class="active"></li>

                         <li data-target="#myCarousel" data-slide-to="1"></li>

                         <li data-target="#myCarousel" data-slide-to="2"></li>

                         <li data-target="#myCarousel" data-slide-to="3"></li>

                     </ol><!-- carousel-indicators Ends -->

                <div class="carousel-inner"><!-- carousel-inner Starts -->
                    @foreach($slides as $key => $slide)

                    <div class="item @if($key==0) active @endif">

                        <a href=''><img src='/storage/{{$slide->slide_image}}' height="600"></a>
                            <div class="carousel-caption d-none d-md-block">
                                 <h1>mero-Pasal</h1>
                                     <!-- <p>Shrestha Pasal</p> -->
                            </div>
                    </div>

                    @endforeach
                </div><!-- carousel-inner Ends -->

                    <a class="left carousel-control" href="#myCarousel" data-slide="prev"><!-- left carousel-control Starts -->

                        <span class="glyphicon glyphicon-chevron-left"> </span>

                        <span class="sr-only"> Previous </span>

                    </a><!-- left carousel-control Ends -->

                    <a class="right carousel-control" href="#myCarousel" data-slide="next"><!-- right carousel-control Starts -->

                        <span class="glyphicon glyphicon-chevron-right"> </span>

                         <span class="sr-only"> Next </span>

                    </a><!-- right carousel-control Ends -->

                </div><!-- carousel slide Ends --->
            </div><!-- col-md-12 Ends -->
        </div><!-- container Ends -->

        <!-- <h1>This is Home Page</h1> -->

            <div id="advantages" class="shadow"><!-- advantages Starts -->
                <div class="container"><!-- container Starts -->
                    @foreach($box_sections as $box_section)
                    <div class="same-height-row shadow"><!-- same-height-row Starts -->

                    <div class="col-sm-4"><!-- col-sm-4 Starts -->

                        <div class="box same-height shadow" ><!-- box same-height Starts -->

                             <div class="icon">

                            <!-- <i class="fas fa-shopping-cart" ></i> -->
                            <!-- <i class="fas fa-shopping-cart"></i> -->
                            <i class="fa fa-thumbs-up"></i>

                </div>

            <h3>{{ $box_section->box_title}}</h3>

            <p>
            {{$box_section->box_desc}}
            </p>


            </div><!-- box same-height Ends -->

            </div><!-- col-sm-4 Ends -->


            </div><!-- same-height-row Ends -->
            @endforeach
            </div><!-- container Ends -->
            </div><!-- advantages Ends -->

            <div id="hot"><!-- hot Starts -->

        <div class="box"> <!-- box Starts -->

        <div class="container"><!-- container Starts -->

        

</div><!-- container Ends -->

</div><!-- box Ends -->

</div><!-- hot Ends -->

<div id="content" class="container"><!-- container Starts -->



@foreach($products as $product)

<div class='col-md-4 col-sm-6 single' >
    <div class="row"><!-- row Starts -->

    <a class='label sale' href='#' style='color:black;'>

<div class='thelabel'> 
        {{$product->product_label}}
 </div>

<div class='label-background'> </div>

</a>

    <div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
                                    <div class='product' >
                                        <a href="">
										<img src="{{url('products/small',$product->image)}}" alt="" />
										</a>
                                        <div class='text' >
                

                                                            <center>
                                                        
                                                          <p class='btn btn-primary' style="color:white;">{{$product->manufacturer->manufacturer_name}} </p>

                                                        
                                                            </center>

                                            </div>
                                            <hr>

                                            <h3><a href='' >{{$product->product_title}}</a></h3>
                                            
                                            <p class='buttons' >

                                                    <a href="{{url('/product-detail',$product->id)}}" class='btn btn-info' >View details</a>

                                                    <a href='' class='btn btn-primary'>

                                                    <i class='fa fa-shopping-cart'></i> Add to cart

                                                    </a>

                                                    </p>
									</div>
                                    
									<div class="product-overlay">
										<div class="overlay-content">
                                        <img src="{{url('products/small',$product->image)}}" alt="" />
                                        <hr>
                                        <h3>{{$product->product_title}}</h3>
                                            
                                        <p class='price'> $ {{$product->product_price}} </p>
                                        <p class='buttons' >
											<a href="{{url('/product-detail',$product->id)}}" class="btn btn-info"><i class=""></i>View Details</a>
											<a href="#" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                        </p>
                                        </div>
									</div>
								</div>
                               </div>
								<!-- <div class="choose">
									<ul class="nav nav-pills nav-justified">
										<li><a href=""><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
										<li><a href=""><i class="fa fa-plus-square"></i>Add to compare</a></li>
									</ul>
								</div> -->
							</div>

                                    
    </div><!-- row Ends -->
    </div>

@endforeach
</div><!-- container Ends -->



{{$products->links()}}
</div>

<script>
    var msg = '{{Session::get('message')}}';
    var exist = '{{Session::has('message')}}';
    if(exist){
      alert(msg);
    }
  </script>
@endsection