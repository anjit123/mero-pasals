@extends('frontend.layouts.master')
@section('title','Cart Page')
@section('content')

<?php 
use App\Product;
$cartCount = Product::cartCount();
?>

    
<div class="container" ><!-- container Starts -->

<div class="col-md-12" ><!--- col-md-12 Starts -->

<ul class="breadcrumb" ><!-- breadcrumb Starts -->

<li>
<a href="/">Home</a>
</li>

<li>Cart</li>

</ul><!-- breadcrumb Ends -->



</div><!--- col-md-12 Ends -->


<div class="col-md-9" id="cart" ><!-- col-md-9 Starts -->

<div class="box" ><!-- box Starts -->

<form action="{{url('cart/apply-coupon')}}" method="post" enctype="multipart-form-data" ><!-- form Starts -->
<input type="hidden" name="_token" value="{{csrf_token()}}">

<!-- {{ method_field('PUT') }} -->
<center><h1> Shopping Cart </h1></center>



<p class="text-muted" > You currently have {{ $cartCount }} item(s) in your cart. </p>

<div class="table-responsive" ><!-- table-responsive Starts -->

@if(Session::has('message'))
                <div class="alert alert-success text-center" role="alert">
                    {{Session::get('message')}}
                </div>
            @endif
<table class="table" ><!-- table Starts -->

<thead><!-- thead Starts -->

<tr>

<th colspan="2" >Product</th>

<th>Quantity</th>

<th>Unit Price</th>

<th>Size</th>
<th>Color</th>

<th colspan="1">Delete</th>

<th colspan="2"> Sub Total </th>


</tr>

</thead><!-- thead Ends -->

<tbody><!-- tbody Starts -->

             @foreach($cart_datas as $cart_data)
                    <?php
                         $image_products=DB::table('products')->select('image')->where('id',$cart_data->products_id)->get();
                    ?>
                 <tr><!-- tr Starts -->

                        <td>
                        @foreach($image_products as $image_product)
                        <img src="{{url('products/small',$image_product->image)}}" alt="" style="width: 80px;">
                        @endforeach 
                        </td>

                        <td>

                        <a href="#" > {{$cart_data->product_name}} </a>

                        </td>

                        <td class="cart_quantity">
                        <div class="cart_quantity_button">
                                <a class="cart_quantity_left" href="{{url('/cart/update-quantity/'.$cart_data->id.'/1')}}"> <strong>+Add</strong> </a>
                                <input type="text" name="quantity" id="quantity" value="{{$cart_data->quantity}}" data-product_id="" class="quantity form-control" autocomplete="off"  size="4">
                                @if($cart_data->quantity>1)
                                                    <a class="cart_quantity_right" href="{{url('/cart/update-quantity/'.$cart_data->id.'/-1')}}"> <strong>-sub</strong> </a>
                                @endif
                        </div>
                        </td>

                        <td>

                        ${{$cart_data->price}}.00

                        </td>

                        <td>

                        {{$cart_data->size}}

                        </td>
                        <td>

                        {{$cart_data->color}}

                        </td>

                        <td>
                        <!-- <input type="checkbox" name="remove[]" value=""> -->
                        <a class="cart_quantity_delete" href="{{url('/cart/deleteItem',$cart_data->id)}}"><i class="fa fa-times">Delete</i></a>

                        </td>

                        <td>

                        ${{$cart_data->price*$cart_data->quantity}}.00

                        </td>

                </tr><!-- tr Ends -->


                @endforeach      
</tbody><!-- tbody Ends -->

<tfoot><!-- tfoot Starts -->
@if(!empty(Session::get('CouponAmount')))
    <tr>
    <th colspan="5"> Total </th>
    <th colspan="2"> ${{$total_amount}}.00 </th>
    </tr>
    <tr>
    <th colspan="5"> Coupon discount </th>
    <th colspan="2"> $ <?php echo Session::get('CouponAmount') ?>.00  </th>
    </tr>
    <tr>
    <th colspan="5"> Grand Total </th>
    <th colspan="2"> $ <?php echo $total_amount - Session::get('CouponAmount') ?>  </th>
    </tr>
@else
    <tr>
    <th colspan="5"> Grand Total </th>
    <th colspan="2"> ${{$total_amount}}.00 </th>
    </tr>
@endif

</tfoot><!-- tfoot Ends -->

</table><!-- table Ends -->

<div class="form-inline pull-right"><!-- form-inline pull-right Starts -->

<div class="form-group"><!-- form-group Starts -->

<label>
    <!-- Coupon Code : -->
     </label>

<input type="text" name="coupon_code" class="form-control" required>

<input class="btn btn-primary" type="submit" name="apply_coupon" value="Apply Coupon Code" >
</div><!-- form-group Ends -->


</div><!-- form-inline pull-right Ends -->

</div><!-- table-responsive Ends -->
</form><!-- form Ends -->

<div class="box-footer"><!-- box-footer Starts -->

<div class="pull-left"><!-- pull-left Starts -->

<a href="{{route('shop.index')}}" class="btn btn-default">

<i class="fa fa-chevron-left"></i> Continue Shopping

</a>

</div><!-- pull-left Ends -->

<div class="pull-right"><!-- pull-right Starts -->


<a href="{{ route('resetCart')}}">

<button class="btn btn-default" type="submit" name="update" value="Update Cart">

<i class="fa fa-refresh"></i> Update Cart

</button>
</a>

<a href="{{url('/check-out')}}" class="btn btn-primary">

Proceed to checkout <i class="fa fa-chevron-right"></i>

</a>

</div><!-- pull-right Ends -->

</div><!-- box-footer Ends -->



</div><!-- box Ends -->





<div id="row same-height-row"><!-- row same-height-row Starts -->




</div><!-- row same-height-row Ends -->


</div><!-- col-md-9 Ends -->

<div class="col-md-3"><!-- col-md-3 Starts -->

<div class="box" id="order-summary"><!-- box Starts -->

<div class="box-header"><!-- box-header Starts -->

<h3>Order Summary</h3>

</div><!-- box-header Ends -->

<p class="text-muted">
Shipping and additional costs.
</p>

<div class="table-responsive"><!-- table-responsive Starts -->

<table class="table"><!-- table Starts -->

<tbody><!-- tbody Starts -->

<tr>
@if(!empty(Session::get('CouponAmount')))

        <td> Order Subtotal </td>

        <th> ${{$total_amount}}.00 </th>

        </tr>

        <tr>

        <td> Coupon Discount </td>

        <th>$<?php echo Session::get('CouponAmount') ?>.00</th>

        </tr>

        <tr>

        <td>Tax</td>

        <th>NRS0.00</th>

        </tr>

        <tr class="total">

        <td> Grand Total</td>

        <th>$<?php echo $total_amount - Session::get('CouponAmount')?>.00</th>

        </tr>

@else
    <td> Order Subtotal </td>

    <th> ${{$total_amount}}.00 </th>

    </tr>

    <tr>

    <td> Coupon Discount </td>

    <th>NRS0.00</th>

    </tr>

    <tr>

    <td>Tax</td>

    <th>NRS0.00</th>

    </tr>

    <tr class="total">

    <td> Grand Total</td>

    <th>${{$total_amount}}.00</th>

    </tr>


@endif



</tbody><!-- tbody Ends -->

</table><!-- table Ends -->

</div><!-- table-responsive Ends -->

</div><!-- box Ends -->

</div><!-- col-md-3 Ends -->

</div><!-- container Ends -->






<script>

$(document).ready(function(data){

$(document).on('keyup', '.quantity', function(){

var id = $(this).data("product_id");

var quantity = $(this).val();

if(quantity  != ''){

$.ajax({

url:"change.php",

method:"POST",

data:{id:id, quantity:quantity},

success:function(data){

$("body").load('cart_body.php');

}




});


}




});




});

</script>





@endsection 