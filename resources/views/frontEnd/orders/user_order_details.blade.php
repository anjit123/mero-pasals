@extends('frontEnd.layouts.master')
@section('title', 'User Account')
@section('content')
<div id="content" ><!-- content Starts -->
<div class="container" ><!-- container Starts -->

<div class="col-md-12" ><!--- col-md-12 Starts -->

<ul class="breadcrumb" ><!-- breadcrumb Starts -->

<li><a href="">Home</a></li>

<li> <a href="{{route('user_account')}}">My Account</a> </li>
<li class="active">{{$orderDetails->id}}</li>

</ul><!-- breadcrumb Ends -->
@if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                {{Session::get('message')}}
            </div>
        @endif


</div><!--- col-md-12 Ends -->

<div class="col-md-12"><!-- col-md-12 Starts -->


<div class="alert alert-danger"><!-- alert alert-danger Starts -->

<strong> Warning! </strong> Please Confirm Your Email and if you have not received your confirmation email

<a href="" class="alert-link"> 

Send Email Again

</a>

</div><!-- alert alert-danger Ends -->


</div><!-- col-md-12 Ends -->

<div class="col-md-3"><!-- col-md-3 Starts -->

<div class="panel panel-default sidebar-menu"><!-- panel panel-default sidebar-menu Starts -->

<div class="panel-heading"><!-- panel-heading Starts -->



<center>

<img src='/storage/{{ $accounts->image }}' class='img-responsive'>

</center>

<br>

<h3 align='center' class='panel-title'> Name : {{ $accounts->name }} </h3>


</div><!-- panel-heading Ends -->

<div class="panel-body"><!-- panel-body Starts -->

<ul class="nav nav-pills nav-stacked"><!-- nav nav-pills nav-stacked Starts -->

<li class="active">

<a href="{{route('user_account')}}"> <i class="fa fa-list"> </i> My Orders </a>

</li>

<li class="">

<a href="{{route('pay_offline')}}"> <i class="fa fa-bolt"></i> Pay Offline </a>

</li>

<li class="">

<a href="{{route('update_profile')}}"> <i class="fa fa-pencil"></i> Edit Account </a>

</li>

<li class="">

<a href="{{route('change_p')}}"> <i class="fa fa-user"></i> Change Password </a>

</li>

<li class="">

<a href=""> <i class="fa fa-heart"></i> My WishList </a>

</li>

<li class="">

<a href=""> <i class="fa fa-trash-o"></i> Delete Account </a>

</li>

<li>

<a href="{{ route('logout') }}"> <i class="fa fa-sign-out"></i> Logout

 </a>

</li>


</ul><!-- nav nav-pills nav-stacked Ends -->

</div><!-- panel-body Ends -->

</div><!-- panel panel-default sidebar-menu Ends -->
</div><!-- col-md-3 Ends -->

<div class="col-md-9" ><!--- col-md-9 Starts -->

<div class="box" ><!-- box Starts -->


<center><!-- center Starts -->

<h1>My Orders</h1>

<p class="lead"> Your orders on one place.</p>

<p class="text-muted" >

If you have any questions, please feel free to <a href="" > contact us,</a> our customer service center is working for you 24/7.


</p>


</center><!-- center Ends -->

<hr>

<div class="table-responsive" ><!-- table-responsive Starts -->

<table class="table table-bordered table-hover" ><!-- table table-bordered table-hover Starts -->

<thead><!-- thead Starts -->

<tr>
<td>Product Code:</td>
<td>Product Name:</td>
<td>Product Size</td>
<td>Product Price</td>
<td>Product Quantity</td>


</tr>

</thead><!-- thead Ends -->

<tbody><!--- tbody Starts --->


@foreach($orderDetails->orderss as $pro)
<tr><!-- tr Starts -->

<td>{{$pro->product_code}}</td>
<td>{{$pro->product_title}}</td>
<td>{{$pro->product_size}}</td>
<td>{{$pro->product_price}}</td>
<td>{{$pro->product_qty}}</td>






</tr><!-- tr Ends -->
@endforeach

</tbody><!--- tbody Ends --->


</table><!-- table table-bordered table-hover Ends -->

</div><!-- table-responsive Ends -->



</div><!-- box Ends -->


</div><!--- col-md-9 Ends -->

</div><!-- container Ends -->
</div><!-- content Ends -->

@endsection



