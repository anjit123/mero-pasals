<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Order</title>
</head>
<body>
    
    <table width='700px'>
        <tr><td>&nbsp;</td></tr>
        <tr><td> <img src="{{ asset('frontEnd/images/logo.png')}}" alt=""> </td></tr>
        <tr><td>Hello {{$name}}</td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td>Thank you for shopping with us. Your Order details are as below:</td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td>Order No: {{$order_id}}</td></tr>
    

        <td><tr>

        <table>
            <tr>
                <td>Product Name</td>
                <td>Product Code</td>
                <td>Size</td>
                <td>Color</td>
                <td>Quantity</td>
                <td>Unit Price</td>
            </tr>
            @foreach($productDetails['orderss'] as $product)
            <tr>
                <td>{{ $product['product_title']}}</td>
                <td>{{ $product['product_code']}}</td>
                <td>{{ $product['product_size']}}</td>
                <td>{{ $product['product_color']}}</td>
                <td>{{ $product['product_qty']}}</td>
                <td>{{ $product['product_price']}}</td>
            </tr>
            @endforeach
        </table>
        </td> </tr>
    </table>
</body>
</html>