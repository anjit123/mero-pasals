@extends('frontend.layouts.master')
@section('title','mero-Pasal')
@section('content')


<div class="container">
<div class="row">
<div class="col-md-12">

<div class="col-md-6">
<img src="/frontEnd/images/contact.jpg" class="w-100" height="500" alt="">

</div>

<div class="col-md-6">
<h2>Contact Us</h2>

<form action="{{route('contact.store')}}" method="post" enctype="multipart/form-data">
<input type="hidden" name="_token" value="{{csrf_token()}}">

<div class="form-group" >

<label>Name</label>

<input type="text" class="form-control" name="name" required>

</div>

<div class="form-group">

<label>Email</label>

<input type="text" class="form-control" name="email" required>

</div>

<div class="form-group">

<label> Subject </label>

<input type="text" class="form-control" name="subject" required>

</div>

<div class="form-group">

<label> Message </label>

<textarea class="form-control" name="message"> </textarea>

</div>


<div class="form-group">

<label> Select Enquiry Type </label>


<select name="enquiry_type" class="form-control"><!-- select Starts -->

<option value="Order and Delivery Support">Order and Delivery Support</option>
<option value="Technical Support">Technical Support</option>
<option value="Price Concern">Price Concern</option>



</select><!-- select Ends -->

</div>


<div class="text-center"><!-- text-center Starts -->

<button type="submit" name="submit" class="btn btn-primary">

<i class="fa fa-user-md"></i> Send Message

</button>

</div><!-- text-center Ends -->

</form><!-- form Ends -->

</div>

</div>

</div>




</div>



@endsection