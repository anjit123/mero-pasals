@extends('frontend.layouts.master')
@section('title','Search List')
@section('content')


<div id="content" >
<!-- content Starts -->

<div class="container">
        <div class="col-md-12" ><!--- col-md-12 Starts -->

                <ul class="breadcrumb" ><!-- breadcrumb Starts -->

                <li>
                <a href="index.php">Home</a>
                </li>

                <li>Search List</li>

                </ul><!-- breadcrumb Ends -->



        </div><!--- col-md-12 Ends -->

        <div class="col-md-3"><!-- col-md-3 Starts -->

        @include('frontEnd.shop.sidebar')

        </div><!-- col-md-3 Ends -->

<div class="col-md-9" ><!-- col-md-9 Starts --->

        <div class='box'>

            <h1>Shop</h1>

            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using '</p>

        </div>



            <div class="row" id="Products" ><!-- row Starts -->
             

                    @foreach($products as $product)
                    <div id="content" class="container"><!-- container Starts -->

                        <div class='col-md-4 col-sm-5 single' >
                                <div class="row"><!-- row Starts -->

                                    <a class='label sale' href='#' style='color:black;'>

                                        <div class='thelabel'> 
                                                {{$product->product_label}}
                                        </div>

                                        <div class='label-background'> </div>

                                    </a>
                                    <div class='product' >

                                        <a href='' >

                                            <img src="{{url('products/small',$product->image)}}" class='img-responsive'  width="250px" >

                                        </a>

                                        <div class='text' >
                            

                                                <center>
                                    

                                                    <p class='btn btn-primary'>{{$product->manufacturer->manufacturer_name}} </p>
                                            
                                                </center>

                                                <hr>

                                                <h3><a href='' >{{$product->product_title}}</a></h3>

                                                <p class='price' > NRS {{$product->product_price}} </p>

                                                <p class='buttons' >

                                                    <a href="{{url('/product-detail',$product->id)}}" class='btn btn-default' >View details</a>

                                                    <a href='' class='btn btn-primary'>

                                                    <i class='fa fa-shopping-cart'></i> Add to cart

                                                    </a>


                                                </p>

                                            </div>


                        </div>

                    </div>

            </div><!-- row Ends -->
            @endforeach

</div><!-- container Ends -->

</div><!-- row Ends -->

<center><!-- center Starts -->

<ul class="pagination" ><!-- pagination Starts -->



</ul><!-- pagination Ends -->

</center><!-- center Ends -->



</div><!-- col-md-9 Ends --->

<div id="wait" style="position:absolute;top:40%;left:45%;padding:100px;padding-top:200px;"><!--- wait Starts -->

</div><!--- wait Ends -->

</div><!-- container Ends -->
</div><!-- content Ends -->

</div>
<!-- </div> -->
</div>







@endsection
