@extends('frontend.layouts.master')
@section('title','Product Details Page')
@section('content')

<div class="container">

      <div id="content" ><!-- content Starts -->
          <div lass="container" ><!-- container Starts -->

              <div class="col-md-12" ><!--- col-md-12 Starts -->
                    <ul class="breadcrumb" ><!-- breadcrumb Starts -->
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li><a href="{{route('shop.index')}}">Shop</a></li>
                        <li>Product Details</li>
                        <li> {{$detail_product->product_title}} </li>
                        <li> {{$detail_product->id}} </li>
                    </ul><!-- breadcrumb Ends -->
              </div><!--- col-md-12 Ends -->

      
              <div class="col-md-12"><!-- col-md-12 Starts -->
                      <div class="col-sm-6">
                              <div class="view-product">
                                  <div class="easyzoom easyzoom--overlay easyzoom--with-thumbnails">
                                      <a href="{{url('products/large',$detail_product->image)}}">
                                          <img  style="width:400px;" src="{{url('products/small',$detail_product->image)}}" alt="" id="dynamicImage"/>
                                      </a>
                                  </div>
                              </div>
                              <div id="similar-product" class="carousel slide" data-ride="carousel">
                                  <div class="carousel-inner">
                                      <div class="item active thumbails">
                                      @foreach($imagesGalleries as $imagesGallery)
                                              <a href="{{url('products/large',$imagesGallery->image)}}" data-standard="{{url('products/small',$imagesGallery->image)}}">
                                                  <img class="changeImage"style="width:80px; cursor: pointer;" src="{{url('products/small',$imagesGallery->image)}}" alt="" >
                                              </a>
                                          @endforeach
                                      </div>
                                  </div>
                              </div>
                      </div><!-- col-sm-6 Ends -->

                              <div class="col-sm-6" ><!-- col-sm-6 Starts -->

                                  <div class="box" ><!-- box Starts -->

                                    <h2 class="text-left" >  {{$detail_product->product_label}} </h2>

                                      <form action="{{route('addToCart')}}" method="post" class="form-horizontal" ><!-- form-horizontal Starts -->

                                                @if(Session::has('message'))
                                                            <div class="alert alert-success text-center" role="alert">
                                                                <strong>!</strong> {{Session::get('message')}}
                                                            </div>
                                                        @endif
                                                        @if(Session::has('review'))
                                                            <div class="alert alert-success text-center" role="alert">
                                                                <strong>!</strong> {{Session::get('review')}}
                                                            </div>
                                                        @endif
                                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                            <input type="hidden" name="products_id" value="{{$detail_product->id}}">
                                                            <input type="hidden" name="product_name" value="{{$detail_product->product_title}}">
                                                            <input type="hidden" name="product_color" value="{{$detail_product->product_color}}">
                                                            <!-- <input type="text" name="product_code" value="{{$detail_product->product_code}}"> -->
                                                          
                                                            <input type="hidden" name="price" value="{{$detail_product->product_price}}" id="dynamicPriceInput">          
                                                            
                    
                                                        <div class="form-group"><!-- form-group Starts -->
                                                            <label class="col-md-5 control-label" >Product Quantity </label>
                                                                <div class="col-md-7" ><!-- col-md-7 Starts -->
                                                                    <span>
                                                                      <input type="text" class="form-control" name="quantity" value="{{$totalStock}}" id="inputStock" required/>
                                                                    </span>
                                                                </div><!-- col-md-7 Ends -->
                                                        </div><!-- form-group Ends -->

                                                      <div class="form-group" ><!-- form-group Starts -->
                                                          <label class="col-md-5 control-label" >Product Size</label>

                                                              <div class="col-md-7" ><!-- col-md-7 Starts -->

                                                                    <select name="size" id="idSize" class="form-control" >

                                                                                  <option value="">Select a Size</option>
                                                                                  <!-- <option value="{{$detail_product->id}}">Medium</option> -->
                                                                          @foreach($detail_product->attributes as $attrs)
                                                                                  <option value="{{$detail_product->id}}-{{$attrs->size}}">{{$attrs->size}}</option>
                                                                          @endforeach
                                                                      </select>

                                                              </div><!-- col-md-7 Ends -->
                                                      </div><!-- form-group Ends -->

                                                      <div class="form-group" ><!-- form-group Starts -->

                                                          <label class="col-md-5 control-label" >Product Color</label>

                                                              <div class="col-md-7" ><!-- col-md-7 Starts -->

                                                                  <select name="color" id="dynamicColor" class="form-control" >
                                                                      <option value="">Select a Color</option>
                                                                      @foreach($detail_product->attributes as $attrs)
                                                                              <option value="{{$detail_product->id}}-{{$attrs->color}}">{{$attrs->color}}</option>
                                                                      @endforeach
                                                                  </select>
                                                              </div><!-- col-md-7 Ends -->
                                                      </div><!-- form-group Ends -->
                  

                                                      <div class="form-group" ><!-- form-group Starts -->
                                                          <label class="col-md-5 control-label" >Product Avilability</label>
                                                                    <div class="col-md-7" ><!-- col-md-7 Starts -->
                                                                              <p class="form-control">
                                                                                @if($totalStock>0)
                                                                                      <span id="availableStock">In Stock</span>
                                                                                  @else
                                                                                      <span id="availableStock">Out of Stock</span>
                                                                                  @endif
                                                                            </p>
                                                                    </div><!-- col-md-7 Ends -->
                                                      </div><!-- form-group Ends -->

                                                      <center><!-- center Starts -->

                                                      </center><!-- center Ends -->


                                                      <p class='price'>
                                                          @if ($detail_product->status == "product")
                                                              <!-- Product Price : NRS<del>{{$detail_product->product_price}} </del><br> -->
                                                              <h3>Product Price : <span id="dynamic_price">${{$detail_product->product_price}}</span><br></h3>

                                                              <!-- Product sale Price : NRS {{$detail_product->psale_price}} -->

                                                          @else
                                                              Bundle Price : $<del>{{$detail_product->product_price}} </del><br>

                                                              Bundle sale Price : ${{$detail_product->psale_price}}

                                                          @endif
                                                      </p>


                                                        <p class="text-center buttons" ><!-- text-center buttons Starts -->
                                                            @if($totalStock>0)
                                                              <button class="btn btn-primary" type="submit" name="cartButton" id="cartButton" value="Add to Cart">

                                                                <i class="fa fa-shopping-cart" ></i> Add to Cart

                                                              </button>
                                                          
                                                            @endif
                                                        
                                                            <button class="btn btn-primary" type="submit" name="wishListButton" id="wishListButton" value="Wish List">

                                                                  <i class="fa fa-heart" ></i> Add to Wishlist

                                                          </button>
                                                        </p><!-- text-center buttons Ends -->

                                      </form><!-- form-horizontal Ends -->

                                  </div><!-- box Ends -->


                                  <div class="row" id="thumbs" ><!-- row Starts -->

                                          <div class="col-xs-4" ><!-- col-xs-4 Starts -->
                                                <a href="{{url('products/large',$detail_product->image)}}" data-standard="{{url('products/small',$detail_product->image)}}" class="thumbnails" >

                                                    <img src="{{url('products/small',$detail_product->image)}} " class="img-responsive" > 
                                                </a>
                                          </div><!-- col-xs-4 Ends -->

                                          <div class="col-xs-4" ><!-- col-xs-4 Starts -->
                                              <a href="{{url('products/large',$detail_product->image)}}" data-standard="{{url('products/small',$detail_product->image)}}" class="thumb" >
                                                  <img src="{{url('products/small',$detail_product->image)}} " class="img-responsive" >
                                              </a>
                                          </div><!-- col-xs-4 Ends -->

                                          <div class="col-xs-4" ><!-- col-xs-4 Starts -->
                                                <a href="{{url('products/large',$detail_product->image)}}" data-standard="{{url('products/small',$detail_product->image)}}" class="thumb" >
                                                      <img src="{{url('products/small',$detail_product->image)}}" class="img-responsive" >  
                                                </a>
                                          </div><!-- col-xs-4 Ends -->
                                  </div><!-- row Ends --> 
                              </div><!-- col-sm-6 Ends -->
                      </div><!-- row Ends -->

                      <div class="box" id="details"><!-- box Starts -->
                              <a class="btn btn-info tab" style="margin-bottom:10px;" href="#description" data-toggle="tab"><!-- btn btn-primary tab Starts -->
                                  @if ($detail_product->status == "product")
                                  Product Description

                                  @else
                                  Bundle Product

                                  @endif
                              </a><!-- btn btn-primary tab Ends -->

                            <a class="btn btn-info tab" style="margin-bottom:10px;" href="#features" data-toggle="tab"><!-- btn btn-primary tab Starts -->
                                Features
                            </a><!-- btn btn-primary tab Ends -->

                            <a class="btn btn-info tab" style="margin-bottom:10px;" href="#review" data-toggle="tab"><!-- btn btn-primary tab Starts -->
                                Reviews
                            </a><!-- btn btn-primary tab Ends -->

                          <hr style="margin-top:0px;">

                            <div class="tab-content"><!-- tab-content Starts -->

                                    <div id="description" class="tab-pane fade in active" style="margin-top:7px;" ><!-- description tab-pane fade in active Starts -->

                                              {{$detail_product->product_desc}}

                                    </div><!-- description tab-pane fade in active Ends -->

                                      <div id="features" class="tab-pane fade in" style="margin-top:7px;" ><!-- features tab-pane fade in  Starts -->
                                            {{$detail_product->product_feature}}
                                      </div><!-- features tab-pane fade in  Ends -->
                                      <div id="review" class="tab-pane fade in" style="margin-top:7px;" ><!-- features tab-pane fade in  Starts -->
                                            
                                      <div id="row same-height-row shadow"><!-- row same-height-row Starts -->

                            <div id="#" class="tab-pane fade in" style="margin-top:7px;" ><!-- video tab-pane fade in Starts -->
                              <div class="container">
                                  <div class="row">
                                  <h2>Feedback</h2>
                                  <table width="100%" border="0">
                                    <div class="col-md-9 col-md-offset-0">
                                      <tr><td width="70%">
                                      <div class="">
                                        <form class="form-horizontal" action="{{route('productReview')}}" method="post">

                                        
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <input type="hidden" name="product_id" value="{{$detail_product->id}}">
                                        <fieldset>
                                  
                                        
                                          <!-- Name input-->
                                          <div class="form-group">
                                            <label class="col-md-3 control-label" for="name">Full Name</label>
                                            <div class="col-md-9">
                                              <input id="name" name="name" type="text" placeholder="Your name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}">
                                              @if ($errors->has('name'))
                                                  <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $errors->first('name') }}</strong>
                                                  </span>
                                              @endif
                                            </div>
                                          </div>
                                  
                                          <!-- Email input-->
                                          <div class="form-group">
                                            <label class="col-md-3 control-label" for="email">Your E-mail</label>
                                            <div class="col-md-9">
                                              <input id="email" name="email" type="text" placeholder="Your email" class="form-control" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}">
                                              @if ($errors->has('email'))
                                                  <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $errors->first('email') }}</strong>
                                                  </span>
                                              @endif
                                            </div>
                                          </div>
                                  
                                          <!-- Message body -->
                                          <div class="form-group">
                                            <label class="col-md-3 control-label" for="message">Your message</label>
                                            <div class="col-md-9">
                                              <textarea class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" id="message" name="message" placeholder="Please enter your feedback here..." rows="5"></textarea>
                                              @if ($errors->has('message'))
                                                  <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $errors->first('message') }}</strong>
                                                  </span>
                                              @endif
                                            </div>
                                          </div>


                                          <!-- Rating -->
                                          <div class="form-group">
                                            <label class="col-md-3 control-label" for="rating">Your rating</label>
                                              <div class="col-md-9">
                                                <!-- <input id="input-21e" value="0" type="number" class="rating" min=0 max=5 step=0.5 data-size="xs" > -->
                                                <select name="rating" id="rating" class="form-control">
                                                  <option value="1">1</option>
                                                  <option value="2">2</option>
                                                  <option value="3">3</option>
                                                  <option value="4">4</option>
                                                  <option value="5">5</option>
                                                  
                                                </select>
                                                
                                              </div>

                                          </div>
                                          
                                          <div class="stars starrr" data-rating="0"></div>
                            <a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="display:none; margin-right: 10px;">
                                  </td>
                                  <td align="center" valign="top" width="30%">
                                          <!-- Form actions -->
                                          <div class="form-group">
                                            <div class="col-md-9 text-center">
                                              <img src="{{url('products/small',$detail_product->image)}}" height="140px" width="140px"/><br/>
                                              <button type="submit" class="btn btn-primary btn-md">Submit</button>
                                              <button type="reset" class="btn btn-default btn-md">Clear</button>
                                            </div>
                                          </div>
                                        </fieldset>
                                        </form>
                                      </div>
                                    </div>
                                    </td>
                                    </tr>
                                    </table>
                                  </div>

                              </div><!-- feedback form -->
                              <h3>Product Reviews</h3>
                              @foreach($reviews as $review)
                                <p> <strong>{{$review->name}}</strong> {{$review->message}} : {{$review->rating}}</p> <br>

                            @endforeach
                            </div>
                           </div><!-- features tab-pane fade in  Ends -->
                          </div><!-- tab-content Ends -->
                        </div><!-- box Ends -->
                    </div>
                   
                </div><!-- col-md-12 Ends -->


<div class="row">
<div class="col-md-12">

            <div class="recommended_items"><!--recommended_items-->
            <h2 class="title text-center">RECOMMENDED PRODUCTS</h2>

            <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <?php $count=1;?>
                    @foreach($relateProducts->chunk(3) as $chunk)
                       
                        <div <?php if($count==1){ ?> class="item active" <?php } else {
                              ?> class="item" <?php } ?>>
                            @foreach($chunk as $item)
                                <div class="col-md-4">
                                    <div class="product-image-wrapper">
                                        <div class="single-products">
                                            <div class="productinfo text-center">
                                                <img src="{{asset('/products/small/'.$item->image)}}" alt="No image" style="width: 150px;"/>
                                                <h2 class="text-muted">${{$item->product_price}}</h2>
                                                <p class="text-dark">{{$item->product_title}}</p>
                                                <a href="{{url('/product-detail',$item->id)}}">
                                                <button type="button" class="btn btn-default primary"><i class=""></i>View Details</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <?php $count++; ?>
                    @endforeach
                </div>
                <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
        </div><!--/recommended_items-->

        </div>


</div>

            </div><!-- container Ends -->
        </div><!-- content Ends -->
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"> </script>

<script>

$("#idSize").change(function () {
    var SizeAttr=$(this).val();
    if(SizeAttr!=""){
        $.ajax({
            type:'get',
            url:'/get-product-attr',
            data:{size:SizeAttr},
            success:function(resp){
                var arr=resp.split("#");
                $("#dynamic_price").html('$'+arr[0]);
                $("#dynamicPriceInput").val(arr[0]);
                $("#dynamicColor").val(arr[0]);
                if(arr[1]==0){
                    $("#cartButton").hide();
                    $("#availableStock").text("Out Of Stock");
                    $("#inputStock").val(0);
                }else{
                    $("#cartButton").show();
                    $("#availableStock").text("In Stock");
                    $("#inputStock").val(arr[1]);
                }
            },error:function () {
                alert("Error Select Size");
            }
        });
    }
});

</script>








@endsection



