@extends('frontend.layouts.master')
@section('title','Product Listing Page')
@section('content')


<div id="content" >
<!-- content Starts -->

<div class="container">
        <div class="col-md-12" ><!--- col-md-12 Starts -->

                <ul class="breadcrumb" ><!-- breadcrumb Starts -->

                <li>
                <a href="index.php">Home</a>
                </li>

                <li>Shop</li>

                </ul><!-- breadcrumb Ends -->



        </div><!--- col-md-12 Ends -->

        <div class="col-md-3"><!-- col-md-3 Starts -->

        @include('frontEnd.shop.sidebar')

        </div><!-- col-md-3 Ends -->

<div class="col-md-9" ><!-- col-md-9 Starts --->

        <div class='box shadow' >

            <center><h1>Product List</h1></center>

            <!-- <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using '</p> -->

        </div>



            <div class="row" id="Products" ><!-- row Starts -->


                    @foreach($products as $product)
                    <div id="content" class="container"><!-- container Starts -->

                        <div class='col-md-4 col-sm-5 single' >
                                <div class="row"><!-- row Starts -->

                                <a class='label sale' href='#' style='color:black;'>

                        <div class='thelabel'> 
                                {{$product->product_label}}
                        </div>

                        <div class='label-background'> </div>

                        </a>

                            <div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
                                    <div class='product' >
                                        <a href="">
										<img src="{{url('products/small',$product->image)}}" alt="" />
										</a>
                                        <div class='text' >
                

                                                            <center>
                                                        
                                                          <p class='btn btn-primary' style="color:white;">{{$product->manufacturer->manufacturer_name}} </p>

                                                        
                                                            </center>

                                            </div>
                                            <hr>

                                            <h3><a href='' >{{$product->product_title}}</a></h3>
                                            
                                            <p class='buttons' >

                                                    <a href="{{url('/product-detail',$product->id)}}" class='btn btn-info' >View details</a>

                                                    <a href="{{url('/product-detail',$product->id)}}" class='btn btn-primary'>

                                                    <i class='fa fa-shopping-cart'></i> Add to cart

                                                    </a>

                                                    </p>
									</div>
                                    
									<div class="product-overlay">
										<div class="overlay-content">
                                                                                <a href="{{url('/product-detail',$product->id)}}">
                                                                                        <img src="{{url('products/small',$product->image)}}" alt="" />
                                                                                </a>
                                        <hr>
                                        <h3>{{$product->product_title}}</h3>
                                            
                                        <p class='price'> $ {{$product->product_price}} </p>
                                        <p class='buttons' >
											<a href="{{url('/product-detail',$product->id)}}" class="btn btn-info"></i>View Details</a>
											<a href="{{url('/product-detail',$product->id)}}" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                        </p>
                                        </div>
									</div>
								</div>
                               </div>
								<!-- <div class="choose">
									<ul class="nav nav-pills nav-justified">
										<li><a href=""><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
										<li><a href=""><i class="fa fa-plus-square"></i>Add to compare</a></li>
									</ul>
								</div> -->
	

                        </div>

                    </div>

            </div><!-- row Ends -->
            @endforeach

</div><!-- container Ends -->

</div><!-- row Ends -->

<center><!-- center Starts -->

<ul class="pagination" ><!-- pagination Starts -->



</ul><!-- pagination Ends -->

</center><!-- center Ends -->



</div><!-- col-md-9 Ends --->

<div id="wait" style="position:absolute;top:40%;left:45%;padding:100px;padding-top:200px;"><!--- wait Starts -->

</div><!--- wait Ends -->

</div><!-- container Ends -->
</div><!-- content Ends -->

</div>
</div>
</div>
</div>







@endsection
