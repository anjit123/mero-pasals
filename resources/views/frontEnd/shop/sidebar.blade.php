   <?php use App\Product; ?>
   <form action="{{ url('/products-filter')}}" method="post">{{csrf_field() }}
   
   <input type="hidden" name="url" value={{ $url ?? ''}}>

  
    <div class="panel panel-default sidebar-menu"><!-- panel panel-default sidebar-menu Starts -->

        <div class="panel-heading"><!-- panel-heading Starts -->

            <h3 class="panel-title"><!-- panel-title Starts -->

                Manufacturers

                <div class="pull-right"><!-- pull-right Starts -->

                        <a href="#" style="color:black;">

                        <span class="nav-toggle hide-show">

                        Hide

                        </span>

                        </a>

                </div><!-- pull-right Ends -->

            </h3><!-- panel-title Ends -->

        </div><!-- panel-heading Ends -->

        <div class="panel-collapse collapse-data"><!-- panel-collapse collapse-data starts -->

            <div class="panel-body"><!-- panel-body Starts -->

                <div class="input-group"><!-- input-group Starts -->

                    <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-manufacturer" placeholder="Filter Manufacturers">


                    <a class="input-group-addon"> <i class="fa fa-search"></i> </a>

                </div><!-- input-group Ends -->

            </div><!-- panel-body Ends -->

            <div class="panel-body scroll-menu"><!-- panel-body scroll-menu Starts -->
                
                    <ul class="nav nav-pills nav-stacked category-menu" id="dev-manufacturer"><!-- nav nav-pills nav-stacked category-menu Starts -->

                    <?php $manufacturers = DB::table('manufacturers')->orderBy('manufacturer_name','ASC')->get(); ?>



                    @foreach($manufacturers as $manu)
                        @if(!empty($_GET['manu']))
                            <?php $manuArray = explode('-', $_GET['manu']); ?>
                            @if(in_array($manu, $manuArray))
                                <?php $manucheck="checked"; ?>
                            @else
                                <?php $manucheck=""; ?>
                            @endif
                        @else
                            <?php $manucheck=""; ?>
                        @endif
                    <li class="checkbox checkbox-primary">
                    <a >
                    <label for="manf" >
                    <input  type="checkbox"
                  
                     name="manufacturerFilter[]" id="{{$manu->id}}" onchange="javascript:this.form.submit();" class="brandId"  value="{{$manu->id}}" {{ $manucheck}} />

                    <span>

                    <img src="/storage/{{ $manu->manufacturer_image}}" width="20" height="20">

                        {{$manu->manufacturer_name}}
                    </span>

                    </label>
                    </a>
                    </li>

                    @endforeach
                    </ul><!-- nav nav-pills nav-stacked category-menu Ends -->

            </div><!-- panel-body scroll-menu Ends -->

        </div><!-- panel-collapse collapse-data Ends -->


    </div><!-- panel panel-default sidebar-menu Ends -->


    <div class="panel panel-default sidebar-menu"><!--- panel panel-default sidebar-menu Starts -->

            <div class="panel-heading"><!-- panel-heading Starts -->

            <h3 class="panel-title"><!-- panel-title Starts -->

            Products Categories

            <div class="pull-right"><!-- pull-right Starts -->

            <a href="#" style="color:black;">

            <span class="nav-toggle hide-show">

            Hide

            </span>

            </a>

            </div><!-- pull-right Ends -->

            </h3><!-- panel-title Ends -->

            </div><!-- panel-heading Ends -->

            <div class="panel-collapse collapse-data"><!-- panel-collapse collapse-data Starts -->

                <div class="panel-body"><!-- panel-body Starts -->

                    <div class="input-group"><!-- input-group Starts -->

                    <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-p-cats" placeholder="Filter Product Categories">

                    <a class="input-group-addon"> <i class="fa fa-search"></i> </a>

                    </div><!-- input-group Ends -->

                </div><!-- panel-body Ends -->

                    <div class="panel-body scroll-menu"><!-- panel-body scroll-menu Starts -->

                            <ul class="nav nav-pills nav-stacked category-menu" id="dev-p-cats"><!-- nav nav-pills nav-stacked category-menu Starts -->

                            <?php $productcategories = DB::table('product_categories')->orderBy('p_cat_name','ASC')->get(); ?>




                            @foreach($productcategories as $p_category)
                                @if(!empty($_GET['p_category']))
                                    <?php $p_categoryArray = explode('-', $_GET['p_category']); ?>
                                   
                                @endif
                            <li class="checkbox checkbox-primary">
                            <a href="">
                            <label>
                            <input  type="checkbox" 
                               class= "common_selector brand" name="pcatFilter[]" id="pcategory" onchange="javascript:this.form.submit();" value="{{$p_category->id}}"
                               @if(!empty($p_categoryArray) && in_array('$p_category->id', $p_categoryArray)) checked="" @endif/>

                            <span>

                            <img src="/storage/{{ $p_category->p_cat_image}}" width="20" height="20">

                                {{$p_category->p_cat_name}}
                            </span>

                            </label>

                            </a>
                            </li>

                            @endforeach

                            </ul><!-- nav nav-pills nav-stacked category-menu Ends -->

                     </div><!-- panel-body scroll-menu Ends -->

            </div><!-- panel-collapse collapse-data Ends -->

    </div><!--- panel panel-default sidebar-menu Ends -->



    <div class="panel panel-default sidebar-menu"><!--- panel panel-default sidebar-menu Starts -->

            <div class="panel-heading"><!-- panel-heading Starts -->

                <h3 class="panel-title"><!-- panel-title Starts -->

                    Categories

                    <div class="pull-right"><!-- pull-right Starts -->

                    <a href="#" style="color:black;">

                    <span class="nav-toggle hide-show">

                    Hide

                    </span>

                    </a>

                    </div><!-- pull-right Ends -->


                </h3><!-- panel-title Ends -->

            </div><!-- panel-heading Ends -->

        <div class="panel-collapse collapse-data"><!-- panel-collapse collapse-data Starts -->

        <div class="panel-body"><!-- panel-body Starts -->

            <div class="input-group"><!-- input-group Starts -->

                    <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-cats" placeholder="Filter Categories">

                    <a class="input-group-addon"> <i class="fa fa-search"> </i> </a>

            </div><!-- input-group Ends -->

        </div><!-- panel-body Ends -->

            <div class="panel-body scroll-menu"><!-- panel-body scroll-menu Starts -->

                    <ul class="nav nav-pills nav-stacked category-menu" id="dev-cats"><!-- nav nav-pills nav-stacked category-menu Starts -->


                    <?php $categories = DB::table('categories')->orderBy('name','ASC')->get(); ?>



                    @foreach($categories as $category)
                        @if(!empty($_GET['category']))
                            <?php $categoryArray = explode('-', $_GET['category']); ?>
                            @if(in_array($category, $categoryArray))
                                <?php $catcheck="checked"; ?>
                            @else
                                <?php $catcheck=""; ?>
                            @endif
                        @else
                            <?php $catcheck=""; ?>
                        @endif
                    <li class="checkbox checkbox-primary">
                    <a href="">
                    <label>
                    <input  type="checkbox"  name="catFilter[]" id="category" onchange="javascript:this.form.submit();" value="{{$category->id}}" {{ $catcheck }}/>

                    <span>

                    <img src="/storage/{{ $category->cat_image}}" width="20" height="20">

                        {{$category->name}}
                    </span>

                    </label>
                    </a>
                    </li>

                    @endforeach


                    </ul><!-- nav nav-pills nav-stacked category-menu Ends -->

                </div><!-- panel-body scroll-menu Ends -->

             </div><!-- panel-collapse collapse-data Ends -->

    </div><!--- panel panel-default sidebar-menu Ends -->

    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"> </script>