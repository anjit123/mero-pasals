 @extends('frontEnd.layouts.master')
@section('title', 'User Login Page')
@section('content')

@section('content')
<div class="container">
@if(Session::has('message'))
            <div class="alert alert-danger text-center" role="alert">
                {{Session::get('message')}}
            </div>
        @endif
<div class ="align-items-center pt-5">
<div class="box">
<div class="box-header">
   <center> <h1>Customer Login</h1></center>
</div>

<div class="row">

<div class="col-md-6">
<img src="/frontEnd/images/login.png" class="w-100 ml-5 mt-3" alt="">


<!-- <a href="{{url('/login/facebook')}}" class="fa fa-facebook">Facebook</a> -->

</div>
<div class="col-md-6">
    
                    <form method="POST" action="{{ route('u_login') }}" class="mt-3">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-form-label">{{ __('E-Mail Address') }}</label>

                            
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                           
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-form-label">{{ __('Password') }}</label>
                            <div class="input-group" id="show_hide_password">
                            
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div class="input-group-addon">
                                <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                             </div>
                              
                        </div>
                        </div>

                        <div class="form-group row pb-5">
                            
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> &nbsp &nbsp

                                    <label class="form-check-label" for="remember">
                                              {{ __('  Remember Me') }}
                                    </label>
                               
                            </div>
                        </div>
<br>
                        <div class="form-group row">
                           
                            
                            <button type="submit" class="btn btn-primary btn-lg btn-block">{{ __('Login') }}</button>
                            
                           


                             
                              @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                          
                          
                          
                        </div>
                    </form>

                    <center><!-- center Starts -->

                        <a href="{{ route('register') }}" >

                        <h3 class="ml-5">New ? Register Here</h3>

                        </a>

                        <a href="{{url('auth/google')}}" class="btn btn-lg btn-social btn-facebook">
                        <i class="fa fa-google"></i> Sign up with Google
                        </a>
                    </center><!-- center Ends -->




                    </div>
                    </div>

                </div>
           
                </div>
                </div>



@endsection