
<center><!-- center Starts -->

<h1>My Orders</h1>

<p class="lead"> Your orders on one place.</p>

<p class="text-muted" >

If you have any questions, please feel free to <a href="" > contact us,</a> our customer service center is working for you 24/7.


</p>


</center><!-- center Ends -->

<hr>

<div class="table-responsive" ><!-- table-responsive Starts -->

<table class="table table-bordered table-hover" ><!-- table table-bordered table-hover Starts -->

<thead><!-- thead Starts -->

<tr>

<th>O N:</th>
<th>Due Amount:</th>
<th>Invoice No:</th>
<th>Qty:</th>
<th>Size:</th>
<th>Order Date:</th>
<th>Paid/Unpaid:</th>
<th>Status:</th>


</tr>

</thead><!-- thead Ends -->

<tbody><!--- tbody Starts --->


<tr><!-- tr Starts -->

<th>1</th>

<td>$100</td>

<td>743207703</td>

<td>2</td>

<td>Medium</td>

<td>2020-02-08 </td>

<td>Unpaid</td>

<td>
<a href="confirm.php?order_id=22" target="blank" class="btn btn-primary btn-sm" > Confirm If Paid </a>
</td>


</tr><!-- tr Ends -->


<tr><!-- tr Starts -->

<th>2</th>

<td>$0</td>

<td>1214443709</td>

<td>0</td>

<td>Select a Size</td>

<td>2020-02-15 </td>

<td>Unpaid</td>

<td>
<a href="confirm.php?order_id=24" target="blank" class="btn btn-primary btn-sm" > Confirm If Paid </a>
</td>


</tr><!-- tr Ends -->


<tr><!-- tr Starts -->

<th>3</th>

<td>$150</td>

<td>1486505709</td>

<td>1</td>

<td>Medium</td>

<td>2020-02-21 </td>

<td>Unpaid</td>

<td>
<a href="confirm.php?order_id=25" target="blank" class="btn btn-primary btn-sm" > Confirm If Paid </a>
</td>


</tr><!-- tr Ends -->


<tr><!-- tr Starts -->

<th>4</th>

<td>$50</td>

<td>1486505709</td>

<td>1</td>

<td>Medium</td>

<td>2020-02-21 </td>

<td>Unpaid</td>

<td>
<a href="confirm.php?order_id=26" target="blank" class="btn btn-primary btn-sm" > Confirm If Paid </a>
</td>


</tr><!-- tr Ends -->


</tbody><!--- tbody Ends --->


</table><!-- table table-bordered table-hover Ends -->

</div><!-- table-responsive Ends -->




</div><!-- box Ends -->


</div><!--- col-md-9 Ends -->

</div><!-- container Ends -->
</div><!-- content Ends -->

