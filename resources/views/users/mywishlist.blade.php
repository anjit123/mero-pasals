@extends('frontEnd.layouts.master')
@section('title', 'User Account')
@section('content')
<div id="content" ><!-- content Starts -->
<div class="container" ><!-- container Starts -->

<div class="col-md-12" ><!--- col-md-12 Starts -->

<ul class="breadcrumb" ><!-- breadcrumb Starts -->

<li>
<a href="">Home</a>
</li>

<li> <a href="{{route('user_account')}}">My Account</a> 
</li>

</ul><!-- breadcrumb Ends -->
@if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                {{Session::get('message')}}
            </div>
        @endif


</div><!--- col-md-12 Ends -->

<div class="col-md-12"><!-- col-md-12 Starts -->

@if(!empty($accounts->email_verified_at))
    <div class="alert alert-success"><!-- alert alert-danger Starts -->

    <strong> Success! </strong> Thankyou for  Confirm Your Email 

    

    </div><!-- alert alert-danger Ends -->
@else{
<div class="alert alert-danger"><!-- alert alert-danger Starts -->

<strong> Warning! </strong> Please Confirm Your Email and if you have not received your confirmation email

<a href="" class="alert-link"> 

Send Email Again

</a>

</div><!-- alert alert-danger Ends -->
}
@endif
</div><!-- col-md-12 Ends -->

<div class="col-md-3"><!-- col-md-3 Starts -->

<div class="panel panel-default sidebar-menu"><!-- panel panel-default sidebar-menu Starts -->

<div class="panel-heading"><!-- panel-heading Starts -->



<center>

<img src='/storage/{{ $accounts->image }}' class='img-responsive'>

</center>

<br>

<h3 align='center' class='panel-title'> Name : {{ $accounts->name }} </h3>


</div><!-- panel-heading Ends -->

<div class="panel-body"><!-- panel-body Starts -->

<ul class="nav nav-pills nav-stacked"><!-- nav nav-pills nav-stacked Starts -->

<li class="">

<a href="{{route('user_account')}}"> <i class="fa fa-list"> </i> My Orders </a>

</li>

<li class="">

<a href="{{route('pay_offline')}}"> <i class="fa fa-bolt"></i> Pay Offline </a>

</li>

<li class="">

<a href="{{route('update_profile')}}"> <i class="fa fa-pencil"></i> Edit Account </a>

</li>

<li class="">

<a href="{{route('change_p')}}"> <i class="fa fa-user"></i> Change Password </a>

</li>

<li class="active">

<a href="{{route('wish_list')}}"> <i class="fa fa-heart"></i> My WishList </a>

</li>

<li class="">

<a href=""> <i class="fa fa-trash-o"></i> Delete Account </a>

</li>

<li>

<a href="{{ route('user_logout') }}"> <i class="fa fa-sign-out"></i> Logout

 </a>

</li>


</ul><!-- nav nav-pills nav-stacked Ends -->

</div><!-- panel-body Ends -->

</div><!-- panel panel-default sidebar-menu Ends -->
</div><!-- col-md-3 Ends -->

<div class="col-md-9" ><!--- col-md-9 Starts -->

<div class="box" ><!-- box Starts -->


<center><!-- center Starts -->

<h1> MY WISHLIST PRODUCT </h1>

<p class="lead"> Your all Wishlist Products on one place. </p>

</center><!-- center Ends -->

<hr>


<div class="table"><!-- table-responsive Starts -->

<table class="table table-bordered "><!-- table table-bordered table-hover Starts -->

<thead>

<tr>

<th> Wishlist No: </th>

<th> Wishlist Product </th>

<th> Delete Wishlist </th>

</tr>

</thead>

<tbody>

@foreach($userWishList as $key => $product)

<tr>

<td width="100"> {{$product->id}} </td>

<td>

<img src="{{ asset('/products/small/'.$product->image)}}" width="60" height="60">

&nbsp;&nbsp;&nbsp; 



</a>

</td>

<td>



<form action="{{route('addToCart')}}" method="post" class="form-horizontal" ><!-- form-horizontal Starts -->

@if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                <strong>!</strong> {{Session::get('message')}}
            </div>
        @endif
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="products_id" value="{{$product->products_id}}">
            <input type="hidden" name="product_name" value="{{$product->product_name}}">
            <input type="hidden" name="color" value="{{$product->color}}">
            <input type="hidden" name="size" value="{{$product->id}}-{{$product->size}}">
          
          
<input type="hidden" name="price" value="{{$product->price}}" id="dynamicPriceInput">          
            
<button class="btn btn-light" type="submit" name="cartButton" id="cartButton" value="Add to Cart">

<i class="fa fa-shopping-cart" ></i> Add to Cart

</button>

<a href="{{url('/wish-list/delete-product/'.$product->id)}}" class="btn btn-danger">

<i class="fa fa-trash-o"> </i> Delete

</a>
</form>
</td>

</tr>

@endforeach

</tbody>

</table><!-- table table-bordered table-hover Ends -->

</div><!-- table-responsive Ends -->


</div><!-- box Ends -->


</div><!--- col-md-9 Ends -->

</div><!-- container Ends -->
</div><!-- content Ends -->

@endsection



