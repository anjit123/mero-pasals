@extends('frontEnd.layouts.master')
@section('title', 'Upadate Profile Page')
@section('content')

<div class="container" ><!-- container Starts -->

<div class="col-md-12" ><!--- col-md-12 Starts -->

<ul class="breadcrumb" ><!-- breadcrumb Starts -->

<li>
<a href="">Home</a>
</li>

<li> <a href="{{route('user_account')}}">My Account</a> 
</li>

</ul><!-- breadcrumb Ends -->

@if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                {{Session::get('message')}}
            </div>
        @endif

</div><!--- col-md-12 Ends -->

<div class="col-md-12"><!-- col-md-12 Starts -->


@if(!empty($accounts->email_verified_at))
    <div class="alert alert-success"><!-- alert alert-danger Starts -->

    <strong> Success! </strong> Thankyou for  Confirm Your Email 

    

    </div><!-- alert alert-danger Ends -->
@else
<div class="alert alert-danger"><!-- alert alert-danger Starts -->

<strong> Warning! </strong> Please Confirm Your Email and if you have not received your confirmation email
<form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('Send Email Again') }}</button>.
                    </form>



</div><!-- alert alert-danger Ends -->

@endif


</div><!-- col-md-12 Ends -->

<div class="col-md-3"><!-- col-md-3 Starts -->

<div class="panel panel-default sidebar-menu"><!-- panel panel-default sidebar-menu Starts -->

<div class="panel-heading"><!-- panel-heading Starts -->


<center>

<img src='/storage/{{ $accounts->image }}' class='img-responsive'>

</center>

<br>

<h3 align='center' class='panel-title'> Name : {{ $accounts->name }} </h3>

</div><!-- panel-heading Ends -->

<div class="panel-body"><!-- panel-body Starts -->

<ul class="nav nav-pills nav-stacked"><!-- nav nav-pills nav-stacked Starts -->

<li class="">

<a href="{{route('user_account')}}"> <i class="fa fa-list"> </i> My Orders </a>

</li>

<li class="">

<a href="{{route('pay_offline')}}"> <i class="fa fa-bolt"></i> Pay Offline </a>

</li>

<li class="active">

<a href="{{route('update_profile')}}"> <i class="fa fa-pencil"></i> Edit Account </a>

</li>

<li class="">

<a href="{{route('change_p')}}"> <i class="fa fa-user"></i> Change Password </a>

</li>

<li class="">

<a href="{{route('wish_list')}}"> <i class="fa fa-heart"></i> My WishList </a>

</li>

<li class="">

<a href=""> <i class="fa fa-trash-o"></i> Delete Account </a>

</li>

<li>

<a href="{{route('user_logout')}}"> <i class="fa fa-sign-out"></i> Logout </a>

</li>


</ul><!-- nav nav-pills nav-stacked Ends -->

</div><!-- panel-body Ends -->

</div><!-- panel panel-default sidebar-menu Ends -->
</div><!-- col-md-3 Ends -->

<div class="col-md-9" ><!--- col-md-9 Starts -->

<div class="box" ><!-- box Starts -->


<h1 align="center" > Edit Your Account </h1>


<form action="{{url('/update-profile',$accounts->id)}}" method="post" enctype="multipart/form-data" ><!--- form Starts -->
@csrf

<div class="form-group" ><!-- form-group Starts -->

<label> Customer Name: </label>

<input type="text" name="name" class="form-control" required value='{{ $accounts->name }}'>


</div><!-- form-group Ends -->


<div class="form-group" ><!-- form-group Starts -->

<label> Customer Email: </label>

<input type="text" name="email" class="form-control" required value='{{ $accounts->email }}'>


</div><!-- form-group Ends -->



<div class="form-group" ><!-- form-group Starts -->

<label> Customer Country: </label>

<input type="text" name="country" class="form-control" required value='{{ $accounts->country }}'>


</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label> Customer City: </label>

<input type="text" name="city" class="form-control" required value='{{ $accounts->city }}'>


</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label> Customer Contact: </label>

<input type="text" name="contact" class="form-control" required value='{{ $accounts->contact }}'>


</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label> Customer Address: </label>

<input type="text" name="address" class="form-control" required value='{{ $accounts->address }}'>


</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label> Customer Image: </label>

<input type="file" name="image" class="form-control" required ><br>

<img src='/storage/{{ $accounts->image }}' width="100" height="100" class="img-responsive" >


</div><!-- form-group Ends -->



<div class="text-center" ><!-- text-center Starts -->

<button type="submit" class="btn btn-primary" >

<i class="fa fa-user-md" ></i> Update Now

</button>

</div>
</div><!-- text-center Ends -->

</form><!--- form Ends -->
</div>
</div><!--- col-md-9 Ends -->

</div><!-- container Ends -->
</div><!-- content Ends -->

@endsection




