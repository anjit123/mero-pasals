@extends('frontEnd.layouts.master')
@section('title', 'Upadate Profile Page')
@section('content')
<div id="content" ><!-- content Starts -->
<div class="container" ><!-- container Starts -->

<div class="col-md-12" ><!--- col-md-12 Starts -->

<ul class="breadcrumb" ><!-- breadcrumb Starts -->

<li>
<a href="">Home</a>
</li>

<li> <a href="{{route('user_account')}}">My Account</a> 
</li>

</ul><!-- breadcrumb Ends -->

@if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                {{Session::get('message')}}
            </div>
        @endif

</div><!--- col-md-12 Ends -->

<div class="col-md-12"><!-- col-md-12 Starts -->



@if(!empty($accounts->email_verified_at))
    <div class="alert alert-success"><!-- alert alert-danger Starts -->

    <strong> Success! </strong> Thankyou for  Confirm Your Email 

    

    </div><!-- alert alert-danger Ends -->
@else
<div class="alert alert-danger"><!-- alert alert-danger Starts -->

<strong> Warning! </strong> Please Confirm Your Email and if you have not received your confirmation email
<form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('Send Email Again') }}</button>.
                    </form>



</div><!-- alert alert-danger Ends -->

@endif


</div><!-- col-md-12 Ends -->

<div class="col-md-3"><!-- col-md-3 Starts -->

<div class="panel panel-default sidebar-menu"><!-- panel panel-default sidebar-menu Starts -->

<div class="panel-heading"><!-- panel-heading Starts -->

<center>

<img src='/storage/{{ $accounts->image }}' class='img-responsive'>

</center>

<br>

<h3 align='center' class='panel-title'> Name : {{ $accounts->name }} </h3>


</div><!-- panel-heading Ends -->

<div class="panel-body"><!-- panel-body Starts -->

<ul class="nav nav-pills nav-stacked"><!-- nav nav-pills nav-stacked Starts -->

<li class="">

<a href="{{route('user_account')}}"> <i class="fa fa-list"> </i> My Orders </a>

</li>

<li class="">

<a href="{{route('pay_offline')}}"> <i class="fa fa-bolt"></i> Pay Offline </a>

</li>

<li class="">

<a href="{{route('update_profile')}}"> <i class="fa fa-pencil"></i> Edit Account </a>

</li>

<li class="active">

<a href="{{route('change_p')}}"> <i class="fa fa-user"></i> Change Password </a>

</li>

<li class="">

<a href="{{route('wish_list')}}"> <i class="fa fa-heart"></i> My WishList </a>

</li>

<li class="">

<a href=""> <i class="fa fa-trash-o"></i> Delete Account </a>

</li>

<li>

<a href="{{ route('user_logout') }}"> <i class="fa fa-sign-out"></i> Logout </a>

</li>


</ul><!-- nav nav-pills nav-stacked Ends -->

</div><!-- panel-body Ends -->

</div><!-- panel panel-default sidebar-menu Ends -->
</div><!-- col-md-3 Ends -->

<div class="col-md-9" ><!--- col-md-9 Starts -->

<div class="box" ><!-- box Starts -->

<h1 align="center">Change Password </h1>

<form action="{{url('/update-password',$accounts->id)}}" method="post"><!-- form Starts -->
@csrf 

<div class="form-group"><!-- form-group Starts -->

<label>Enter Your Current Password</label>

<input type="password" name="password" id="password"  class="form-control" placeholder="Old Password" required>

</div><!-- form-group Ends -->


<div class="form-group"><!-- form-group Starts -->

<label>Enter Your New Password</label>

<input type="password" name="newPassword" id="newPassword" class="form-control" placeholder="New Password" required>

</div><!-- form-group Ends -->


<div class="form-group"><!-- form-group Starts -->

<label>Enter Your New Password Again</label>

<input type="password" name="newPassword_confirmation" id="newPassword_confirmation" class="form-control" Placeholder="Confirm Password" required>

</div><!-- form-group Ends -->

<div class="text-center"><!-- text-center Starts -->

<button type="submit" name="submit" class="btn btn-primary">

<i class="fa fa-user-md"> </i> Change Password

</button>

</div><!-- text-center Ends -->

</form><!-- form Ends -->
</div>
</div><!--- col-md-9 Ends -->

</div><!-- container Ends -->
</div><!-- content Ends -->
@endsection