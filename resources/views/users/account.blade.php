@extends('frontEnd.layouts.master')
@section('title', 'User Account')
@section('content')
<div id="content" ><!-- content Starts -->
<div class="container" ><!-- container Starts -->

<div class="col-md-12" ><!--- col-md-12 Starts -->

<ul class="breadcrumb" ><!-- breadcrumb Starts -->

<li>
<a href="">Home</a>
</li>

<li> <a href="{{route('user_account')}}">My Account</a> 
</li>

</ul><!-- breadcrumb Ends -->
@if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                {{Session::get('message')}}
            </div>
        @endif


</div><!--- col-md-12 Ends -->

<div class="col-md-12"><!-- col-md-12 Starts -->


@if(!empty($accounts->email_verified_at))
    <div class="alert alert-success"><!-- alert alert-danger Starts -->

    <strong> Success! </strong> Thankyou for  Confirm Your Email 

    

    </div><!-- alert alert-danger Ends -->
@else
<div class="alert alert-danger"><!-- alert alert-danger Starts -->

<strong> Warning! </strong> Please Confirm Your Email and if you have not received your confirmation email
<form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('Send Email Again') }}</button>.
                    </form>



</div><!-- alert alert-danger Ends -->

@endif


</div><!-- col-md-12 Ends -->

<div class="col-md-3"><!-- col-md-3 Starts -->

<div class="panel panel-default sidebar-menu"><!-- panel panel-default sidebar-menu Starts -->

<div class="panel-heading"><!-- panel-heading Starts -->



<center>

<img src='/storage/{{ $accounts->image }}' class='img-responsive'>

</center>

<br>

<h3 align='center' class='panel-title'> Name : {{ $accounts->name }} </h3>


</div><!-- panel-heading Ends -->

<div class="panel-body"><!-- panel-body Starts -->

<ul class="nav nav-pills nav-stacked"><!-- nav nav-pills nav-stacked Starts -->

<li class="active">

<a href="{{route('user_account')}}"> <i class="fa fa-list"> </i> My Orders </a>

</li>

<li class="">

<a href="{{route('pay_offline')}}"> <i class="fa fa-bolt"></i> Pay Offline </a>

</li>

<li class="">

<a href="{{route('update_profile')}}"> <i class="fa fa-pencil"></i> Edit Account </a>

</li>

<li class="">

<a href="{{route('change_p')}}"> <i class="fa fa-user"></i> Change Password </a>

</li>

<li class="">

<a href="{{route('wish_list')}}"> <i class="fa fa-heart"></i> My WishList </a>

</li>

<li class="">

<a href=""> <i class="fa fa-trash-o"></i> Delete Account </a>

</li>

<li>

<a href="{{ route('user_logout') }}"> <i class="fa fa-sign-out"></i> Logout

 </a>

</li>


</ul><!-- nav nav-pills nav-stacked Ends -->

</div><!-- panel-body Ends -->

</div><!-- panel panel-default sidebar-menu Ends -->
</div><!-- col-md-3 Ends -->

<div class="col-md-9" ><!--- col-md-9 Starts -->

<div class="box" ><!-- box Starts -->


<center><!-- center Starts -->

<h1>My Orders</h1>

<p class="lead"> Your orders on one place.</p>

<p class="text-muted" >

If you have any questions, please feel free to <a href="{{route('contact.create')}}" > contact us,</a> our customer service center is working for you 24/7.


</p>


</center><!-- center Ends -->

<hr>

<div class="table" ><!-- table-responsive Starts -->

<table class="table table-bordered table-hover" ><!-- table table-bordered table-hover Starts -->

<thead><!-- thead Starts -->

<tr>
<td>Order ID:</td>
<td>Ordered Products:</td>
<td>Payment Method</td>
<td>Grand Total</td>
<td>Created on</td>
<td>Action</td>

</tr>

</thead><!-- thead Ends -->

<tbody><!--- tbody Starts --->


@foreach($orders as $order)
<tr><!-- tr Starts -->

<td>{{$order->id}}</td>
<td>
    @foreach($order->orderss as $pro)
        <a href="{{url('/orders/'.$order->id) }}">{{$pro->product_code}}</a> <br>
        {{$pro->product_title}} <br>
        {{$pro->product_size}} <br>
        <!-- {{$pro->product_qty}} <br> -->
    @endforeach
</td>
<td>{{$order->payment_method}}</td>
<td>{{$order->grand_total}}</td>
<td>{{$order->created_at}}</td>


<td>
<a href="{{url('/orders/'.$order->id) }}" target="blank" class="btn btn-info btn-sm" > View Ordered Details </a>
</td>


</tr><!-- tr Ends -->
@endforeach

</tbody><!--- tbody Ends --->


</table><!-- table table-bordered table-hover Ends -->

</div><!-- table-responsive Ends -->



</div><!-- box Ends -->


</div><!--- col-md-9 Ends -->

</div><!-- container Ends -->
</div><!-- content Ends -->

@endsection



