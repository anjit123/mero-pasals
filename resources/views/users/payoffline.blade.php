@extends('frontEnd.layouts.master')
@section('title', 'Pay Offline Page')
@section('content')

<div class="container">



<div class="col-md-12" ><!--- col-md-12 Starts -->

<ul class="breadcrumb" ><!-- breadcrumb Starts -->

<li>
<a href="">Home</a>
</li>

<li> <a href="{{route('user_account')}}">My Account</a> 
</li>

</ul><!-- breadcrumb Ends -->

@if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                {{Session::get('message')}}
            </div>
        @endif

</div><!--- col-md-12 Ends -->

<div class="col-md-12"><!-- col-md-12 Starts -->



@if(!empty($accounts->email_verified_at))
    <div class="alert alert-success"><!-- alert alert-danger Starts -->

    <strong> Success! </strong> Thankyou for  Confirm Your Email 

    

    </div><!-- alert alert-danger Ends -->
@else
<div class="alert alert-danger"><!-- alert alert-danger Starts -->

<strong> Warning! </strong> Please Confirm Your Email and if you have not received your confirmation email
<form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('Send Email Again') }}</button>.
                    </form>



</div><!-- alert alert-danger Ends -->

@endif


</div><!-- col-md-12 Ends -->

<div class="col-md-3"><!-- col-md-3 Starts -->

<div class="panel panel-default sidebar-menu"><!-- panel panel-default sidebar-menu Starts -->

<div class="panel-heading"><!-- panel-heading Starts -->


<center>

<img src='/storage/{{ $accounts->image }}' class='img-responsive'>

</center>

<br>

<h3 align='center' class='panel-title'> Name : {{ $accounts->name }} </h3>

</div><!-- panel-heading Ends -->

<div class="panel-body"><!-- panel-body Starts -->

<ul class="nav nav-pills nav-stacked"><!-- nav nav-pills nav-stacked Starts -->

<li class="">

<a href="{{route('user_account')}}"> <i class="fa fa-list"> </i> My Orders </a>

</li>

<li class="active">

<a href="{{route('pay_offline')}}"> <i class="fa fa-bolt"></i> Pay Offline </a>

</li>

<li class="">

<a href="{{route('update_profile')}}"> <i class="fa fa-pencil"></i> Edit Account </a>

</li>

<li class="">

<a href="{{route('change_p')}}"> <i class="fa fa-user"></i> Change Password </a>

</li>

<li class="">

<a href="{{route('wish_list')}}"> <i class="fa fa-heart"></i> My WishList </a>

</li>

<li class="">

<a href=""> <i class="fa fa-trash-o"></i> Delete Account </a>

</li>

<li>

<a href="{{ route('user_logout') }}"> <i class="fa fa-sign-out"></i> Logout </a>

</li>


</ul><!-- nav nav-pills nav-stacked Ends -->

</div><!-- panel-body Ends -->

</div><!-- panel panel-default sidebar-menu Ends -->
</div><!-- col-md-3 Ends -->




<div class="col-md-9" ><!--- col-md-9 Starts -->

<center><!-- center Starts -->

<h1> Pay OffLine Using Method  </h1>

<p class="text-muted" >

If you have any questions, please feel free to <a href="../contact.php" >contact us,</a> our customer service center is working for you 24/7.

</p>

</center><!-- center Ends -->

<hr>


<div class="table-responsive" ><!-- table-responsive Starts -->

<table class="table table-bordered table-hover table-striped" ><!-- table table-bordered table-hover table-striped Starts -->

<thead><!-- thead Starts -->

<tr>

<th> Bank Account Details </th>

<th> Cash Details: </th>

<th> Western Union Details: </th>

</tr>

</thead><!-- thead Ends -->

<tbody><!-- tbody Starts -->

<tr>

<td> Bank Name:NIC ASIA	 </td>

<td> NIC#1234567 </td>

<td> Full Name:Anjit Shrestha
</td>


</tr>

</tbody><!-- tbody Ends -->


</table><!-- table table-bordered table-hover table-striped Ends -->

</div><!-- table-responsive Ends -->




</div>

</div><!-- container Ends -->

@endsection