-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2020 at 09:22 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mero-pasal`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `box_sections`
--

CREATE TABLE `box_sections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `box_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `box_desc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `products_id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `user_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_top` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `cat_top`, `cat_image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 0, 'Men', 'yes', 'CategoryUploads/0Euu9SZy3A2ARGew9MCL9okswID7ufHnvZ2M7RPP.jpeg', NULL, '2020-06-07 01:53:52', '2020-06-07 02:49:01'),
(2, 0, 'Women', 'yes', 'CategoryUploads/TjqjXoGJPhDIpggqxVHHv8jGEk0AB76qJj1kSggX.jpeg', NULL, '2020-06-07 02:48:39', '2020-06-07 02:48:39'),
(3, 0, 'Child', 'yes', 'CategoryUploads/e6XbrSjUe4TWCRpsv3eTwojzyrPpNxXBoBKwTJpu.jpeg', NULL, '2020-06-15 10:14:01', '2020-06-15 10:14:01');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enquiry_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `coupon_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `amount_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiry_date` date NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `coupon_code`, `amount`, `amount_type`, `expiry_date`, `status`, `created_at`, `updated_at`) VALUES
(1, '12345abcde', 10, 'Percentage', '2020-06-29', 1, '2020-06-15 10:55:49', '2020-06-15 10:55:49');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_address`
--

CREATE TABLE `delivery_address` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL,
  `users_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pincode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `delivery_address`
--

INSERT INTO `delivery_address` (`id`, `users_id`, `users_email`, `name`, `address`, `city`, `state`, `country`, `pincode`, `contact`, `created_at`, `updated_at`) VALUES
(1, 1, 'Shresthaanjit3@gmail.com', 'Anjit', 'Kapan', 'Kathmandu', '3', 'Nepal', '44600', '9810220552', NULL, NULL),
(2, 2, 'anjitshrestha5843@gmail.com', 'Anjit Shrestha', 'Kapan', 'Kathmandu', '3', 'Nepal', '44600', '9810220552', NULL, NULL),
(3, 3, 'rohit@gmail.com', 'Anjit', 'Kapan', 'Kathmandu', '3', 'Nepal', '44600', '9800112233', NULL, NULL),
(4, 4, 'romiya@gmail.com', 'romiya rai', 'Illam', 'Illam', '1', 'Nepal', '44600', '9810220598', NULL, NULL),
(5, 5, 'munna@gmail.com', 'Anjit', 'Kapan', 'Kathmandu', '3', 'Nepal', '44600', '9810220552', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `image_galleries`
--

CREATE TABLE `image_galleries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `products_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `image_galleries`
--

INSERT INTO `image_galleries` (`id`, `products_id`, `image`, `created_at`, `updated_at`) VALUES
(1, 1, '4311621591515826.jpg', '2020-06-07 01:58:46', '2020-06-07 01:58:46'),
(2, 1, '4100871591515834.jpg', '2020-06-07 01:58:54', '2020-06-07 01:58:54'),
(3, 1, '7011621591515839.jpg', '2020-06-07 01:58:59', '2020-06-07 01:58:59'),
(4, 2, '2796811591535686.jpg', '2020-06-07 07:29:47', '2020-06-07 07:29:47'),
(5, 2, '3879061592237000.jpg', '2020-06-15 10:18:20', '2020-06-15 10:18:20'),
(6, 2, '3581711592237011.jpg', '2020-06-15 10:18:31', '2020-06-15 10:18:31'),
(7, 4, '8897361592237350.jpg', '2020-06-15 10:24:11', '2020-06-15 10:24:11'),
(8, 4, '2349381592237357.jpg', '2020-06-15 10:24:17', '2020-06-15 10:24:17'),
(9, 5, '4367121592237390.jpg', '2020-06-15 10:24:50', '2020-06-15 10:24:50'),
(10, 5, '7844271592237400.jpg', '2020-06-15 10:25:00', '2020-06-15 10:25:00'),
(11, 6, '5510691592237645.jpg', '2020-06-15 10:29:05', '2020-06-15 10:29:05'),
(12, 7, '7115101592238313.jpg', '2020-06-15 10:40:13', '2020-06-15 10:40:13'),
(13, 8, '5903401592447167.jpg', '2020-06-17 20:41:07', '2020-06-17 20:41:07'),
(14, 8, '9795391592447212.jpg', '2020-06-17 20:41:52', '2020-06-17 20:41:52');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers`
--

CREATE TABLE `manufacturers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `manufacturer_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manufacturer_top` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manufacturer_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `manufacturers`
--

INSERT INTO `manufacturers` (`id`, `manufacturer_name`, `manufacturer_top`, `manufacturer_image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Nike', 'yes', 'ManufacturerUploads/6BEwQPM9V91bDOhvtudh5nOzSBgDTb5Y74VguZfK.jpeg', NULL, '2020-06-07 01:54:14', '2020-06-07 01:54:14'),
(2, 'Adidas', 'yes', 'ManufacturerUploads/2qFrWrRMVoJcgZj5RDzsNeOkSzDVAGsMIbWZsT0d.jpeg', NULL, '2020-06-15 10:12:45', '2020-06-15 10:12:45');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_02_01_134802_create_admins_table', 1),
(5, '2020_02_05_132023_create_categories_table', 1),
(6, '2020_02_23_134515_create_slides_table', 1),
(7, '2020_02_25_074359_create_box_sections_table', 1),
(8, '2020_03_01_114025_create_manufacturers_table', 1),
(9, '2020_03_01_122159_create_product_categories_table', 1),
(10, '2020_03_02_163930_create_products_table', 1),
(11, '2020_03_04_114657_create_product_atrrs_table', 1),
(12, '2020_03_04_123503_create_image_galleries_table', 1),
(13, '2020_03_09_090355_create_roles_table', 1),
(14, '2020_03_09_110350_create_role_users_table', 1),
(15, '2020_03_25_134052_create_product_bundle_relations_table', 1),
(16, '2020_04_04_080255_create_carts_table', 1),
(17, '2020_04_09_115851_create_orders_table', 1),
(18, '2020_04_09_145026_create_delivery_address_table', 1),
(19, '2020_04_11_084047_create_coupons_table', 1),
(20, '2020_04_11_192353_create_order_product_table', 1),
(21, '2020_04_26_223721_create_wish_list_table', 1),
(22, '2020_05_18_095006_create_product_reviews_table', 1),
(23, '2020_05_30_215538_create_contacts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL,
  `users_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pincode` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_charges` double(8,2) NOT NULL,
  `coupon_code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coupon_amount` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grand_total` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `users_id`, `users_email`, `name`, `address`, `city`, `state`, `pincode`, `country`, `contact`, `shipping_charges`, `coupon_code`, `coupon_amount`, `order_status`, `payment_method`, `grand_total`, `created_at`, `updated_at`) VALUES
(1, 1, 'Shresthaanjit3@gmail.com', 'Anjit Shrestha', 'Kapan', 'Kathmandu', '3', '44600', 'Nepal', '9810220552', 0.00, 'NO Coupon', '0', 'Delivered', 'COD', '50', '2020-06-07 04:45:46', '2020-06-15 10:53:24'),
(2, 1, 'Shresthaanjit3@gmail.com', 'Anjit Shrestha', 'Kapan', 'Kathmandu', '3', '44600', 'Nepal', '9810220552', 0.00, 'NO Coupon', '0', 'Pending', 'COD', '50', '2020-06-07 04:46:27', '2020-06-07 04:46:27'),
(6, 1, 'Shresthaanjit3@gmail.com', 'Anjit Shrestha', 'Kapan', 'Kathmandu', '3', '44600', 'Nepal', '9810220552', 0.00, 'NO Coupon', '0', 'Pending', 'COD', '50', '2020-06-07 05:10:55', '2020-06-07 05:10:55'),
(7, 1, 'Shresthaanjit3@gmail.com', 'Anjit Shrestha', 'Kapan', 'Kathmandu', '3', '44600', 'Nepal', '9810220552', 0.00, 'NO Coupon', '0', 'Pending', 'Paypal', '50', '2020-06-08 01:23:19', '2020-06-08 01:23:19'),
(8, 2, 'anjitshrestha5843@gmail.com', 'Anjit Shrestha', 'Kapan', 'Kathmandu', '3', '44600', 'Nepal', '9810220552', 0.00, '12345abcde', '5', 'Pending', 'Paypal', '45', '2020-06-16 04:18:24', '2020-06-16 04:18:24'),
(9, 1, 'Shresthaanjit3@gmail.com', 'Munna Rai', 'Kapan', 'Kathmandu', '3', '44600', 'Nepal', '9810220552', 0.00, 'NO Coupon', '0', 'Pending', 'COD', '50', '2020-06-16 06:10:25', '2020-06-16 06:10:25'),
(10, 1, 'Shresthaanjit3@gmail.com', 'Anjit', 'Kapan', 'Kathmandu', '3', '44600', 'Nepal', '9810220552', 0.00, '12345abcde', '5', 'Pending', 'Paypal', '45', '2020-06-16 12:20:37', '2020-06-16 12:20:37'),
(11, 3, 'rohit@gmail.com', 'Rohit Dhimal', 'Kapan', 'Kathmandu', '3', '44600', 'Nepal', '9800112233', 0.00, 'NO Coupon', '0', 'Pending', 'COD', '50', '2020-06-16 13:10:45', '2020-06-16 13:10:45'),
(12, 3, 'rohit@gmail.com', 'Rohit Dhimal', 'Kapan', 'Kathmandu', '3', '44600', 'Nepal', '9800112233', 0.00, 'NO Coupon', '0', 'Pending', 'COD', '50', '2020-06-16 13:12:50', '2020-06-16 13:12:50'),
(13, 3, 'rohit@gmail.com', 'Anjit', 'Kapan', 'Kathmandu', '3', '44600', 'Nepal', '9800112233', 0.00, '12345abcde', '5', 'Pending', 'Paypal', '45', '2020-06-17 02:22:42', '2020-06-17 02:22:42'),
(14, 4, 'romiya@gmail.com', 'romiya rai', 'Illam', 'Illam', '1', '44600', 'Nepal', '9810220598', 0.00, '12345abcde', '5', 'Pending', 'Paypal', '45', '2020-06-17 20:29:51', '2020-06-17 20:29:51'),
(15, 5, 'munna@gmail.com', 'Anjit', 'Kapan', 'Kathmandu', '3', '44600', 'Nepal', '9810220552', 0.00, '12345abcde', '5', 'Pending', 'Paypal', '45', '2020-06-17 23:53:45', '2020-06-17 23:53:45');

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` int(11) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`id`, `order_id`, `users_id`, `product_id`, `product_code`, `product_title`, `product_size`, `product_color`, `product_price`, `product_qty`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'sku-1000-c-l-b', 'Coats', 'Large', '1-Black', 50, 1, '2020-06-07 04:45:46', '2020-06-07 04:45:46'),
(2, 2, 1, 1, 'sku-1000-c-l-b', 'Coats', 'Large', '1-Black', 50, 1, '2020-06-07 04:46:28', '2020-06-07 04:46:28'),
(6, 6, 1, 1, 'sku-1000-c-l-b', 'Coats', 'Large', '1-Black', 50, 1, '2020-06-07 05:10:55', '2020-06-07 05:10:55'),
(7, 7, 1, 2, 'sku-1000-t-l-b', 'T-Shirt', 'Medium', '2-Black', 50, 1, '2020-06-08 01:23:19', '2020-06-08 01:23:19'),
(8, 8, 2, 5, 'sku-1002-j-l-b', 'Jacket', 'Large', '5-Black', 50, 1, '2020-06-16 04:18:24', '2020-06-16 04:18:24'),
(9, 9, 1, 6, 'sku-1003-l-t-b-l', 'Long-T-Shirt', 'Medium', '6-Blue', 50, 1, '2020-06-16 06:10:25', '2020-06-16 06:10:25'),
(10, 10, 1, 7, 'sku-1004-n-t-b-l', 'New-T-Shirt', 'Large', '7-Black', 50, 1, '2020-06-16 12:20:37', '2020-06-16 12:20:37'),
(11, 11, 3, 7, 'sku-1004-n-t-b-l', 'New-T-Shirt', 'Medium', '7-Blue', 50, 1, '2020-06-16 13:10:45', '2020-06-16 13:10:45'),
(12, 12, 3, 2, 'sku-1001-t-b-l', 'T-Shirt', 'Large', '2-Black', 50, 1, '2020-06-16 13:12:50', '2020-06-16 13:12:50'),
(13, 13, 3, 5, 'sku-1002-j-l-b', 'Jacket', 'Large', '5-Black', 50, 1, '2020-06-17 02:22:42', '2020-06-17 02:22:42'),
(14, 14, 4, 6, 'sku-1003-l-t-b-l', 'Long-T-Shirt', 'Large', '6-Black', 50, 1, '2020-06-17 20:29:52', '2020-06-17 20:29:52'),
(15, 15, 5, 2, 'sku-1001-t-b-l', 'T-Shirt', 'Large', '2-Black', 50, 1, '2020-06-17 23:53:46', '2020-06-17 23:53:46');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `p_cat_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `product_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` double(8,2) NOT NULL,
  `psale_price` double(8,2) NOT NULL,
  `product_keyword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_feature` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `manufacturer_id`, `p_cat_id`, `cat_id`, `product_title`, `product_color`, `image`, `product_price`, `psale_price`, `product_keyword`, `product_desc`, `product_feature`, `product_label`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 1, 2, 1, 'T-Shirt', 'Blue', '53713.jpg', 50.00, 45.00, 'T-Shirts', 'gsls', 'glmvbsf', 'New', 'product', NULL, '2020-06-07 02:48:08', '2020-06-07 03:13:46'),
(4, 1, 1, 2, 'Coats', 'Black', '87514.jpg', 50.00, 45.00, 'coats', 'This is a black woman coat.', 'This product is more comfortable.', 'New', 'product', NULL, '2020-06-07 03:14:25', '2020-06-15 10:08:37'),
(5, 2, 3, 3, 'Jacket', 'Red', '59142.jpg', 50.00, 45.00, 'jacket', 'This is a red color jacket.', 'This is fit for the child.', 'New', 'product', NULL, '2020-06-07 03:37:20', '2020-06-15 10:16:57'),
(6, 1, 2, 1, 'Long-T-Shirt', 'Red', '92557.jpg', 50.00, 45.00, 'T-Shirts', 'This is red in color and a long t-shirt.', 'This is a comfortable t-shirt.', 'New', 'product', NULL, '2020-06-07 03:39:44', '2020-06-15 10:18:00'),
(7, 1, 2, 1, 'New-T-Shirt', 'Red', '77339.jpg', 50.00, 45.00, 'T-Shirts', 'gjvkhbgk', 'cvhjhj', 'New', 'product', NULL, '2020-06-07 03:40:55', '2020-06-07 03:40:55'),
(8, 2, 3, 1, 'Jacket', 'Red', '23373.jpg', 50.00, 45.00, 'jacket', 'This is a red color jacket.', 'This is a comfortable jacket for the men', 'New', 'product', NULL, '2020-06-17 20:33:54', '2020-06-17 20:33:54');

-- --------------------------------------------------------

--
-- Table structure for table `product_atrrs`
--

CREATE TABLE `product_atrrs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `products_id` int(11) NOT NULL,
  `sku` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_atrrs`
--

INSERT INTO `product_atrrs` (`id`, `products_id`, `sku`, `size`, `price`, `color`, `stock`, `created_at`, `updated_at`) VALUES
(11, 2, 'sku-1001-t-b-l', 'Large', 50.00, 'Black', 3, '2020-06-07 07:30:24', '2020-06-17 23:53:46'),
(12, 2, 'sku-1001-c-m-b', 'Medium', 50.00, 'Blue', 5, '2020-06-07 07:30:45', '2020-06-15 10:22:32'),
(13, 4, 'sku-1000-c-l-b', 'Large', 50.00, 'Black', 5, '2020-06-15 10:19:27', '2020-06-15 10:20:47'),
(14, 4, 'sku-1000-c-l-b', 'Medium', 50.00, 'Blue', 5, '2020-06-15 10:19:47', '2020-06-15 10:20:47'),
(15, 4, 'sku-1000-c-l-b', 'Small', 50.00, 'Red', 5, '2020-06-15 10:20:01', '2020-06-15 10:20:47'),
(16, 2, 'sku-1001-t-r-s', 'Small', 50.00, 'Red', 5, '2020-06-15 10:23:06', '2020-06-15 10:23:06'),
(17, 5, 'sku-1002-j-l-b', 'Large', 50.00, 'Black', 3, '2020-06-15 10:25:53', '2020-06-17 02:22:42'),
(18, 5, 'sku-1002-j-m-b', 'Medium', 50.00, 'Blue', 5, '2020-06-15 10:26:44', '2020-06-15 10:32:00'),
(19, 5, 'sku-1002-j-s-r', 'Small', 50.00, 'Red', 5, '2020-06-15 10:27:35', '2020-06-15 10:32:00'),
(20, 6, 'sku-1003-l-t-b-l', 'Large', 50.00, 'Black', 5, '2020-06-15 10:35:20', '2020-06-17 20:46:38'),
(21, 6, 'sku-1003-l-t-b-m', 'Medium', 50.00, 'Blue', 5, '2020-06-15 10:36:38', '2020-06-17 20:46:38'),
(22, 6, 'sku-1003-l-t-r-s', 'Small', 50.00, 'Red', 5, '2020-06-15 10:39:51', '2020-06-17 20:46:38'),
(23, 7, 'sku-1004-n-t-b-l', 'Large', 50.00, 'Black', 3, '2020-06-15 10:41:35', '2020-06-16 13:10:45'),
(24, 7, 'sku-1004-n-t-b-m', 'Medium', 50.00, 'Blue', 5, '2020-06-15 10:43:02', '2020-06-15 10:43:02'),
(25, 7, 'sku-1004-n-t-r-s', 'Small', 50.00, 'Red', 5, '2020-06-15 10:43:34', '2020-06-15 10:43:34'),
(26, 8, 'sku-1005-j-r-l', 'Large', 50.00, 'Red', 0, '2020-06-17 20:38:10', '2020-06-17 20:39:56'),
(27, 8, 'sku-1005-j-b-m', 'Medium', 50.00, 'Blue', 0, '2020-06-17 20:38:59', '2020-06-17 20:39:56'),
(28, 8, 'sku-1005-j-b-s', 'Small', 50.00, 'Black', 0, '2020-06-17 20:39:21', '2020-06-17 20:39:56');

-- --------------------------------------------------------

--
-- Table structure for table `product_bundle_relations`
--

CREATE TABLE `product_bundle_relations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rel_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `bundle_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `p_cat_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_cat_top` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_cat_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `p_cat_name`, `p_cat_top`, `p_cat_image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Coats', 'yes', 'PCategoryUploads/4Z7G5kVSym9i5Oze4WJxz7zzuvB8aDVcGbJTdtIs.jpeg', NULL, '2020-06-07 01:54:35', '2020-06-07 01:54:35'),
(2, 'T-Shirt', 'yes', 'PCategoryUploads/YYJPCtcnWCrCra0KZNKALpnnxOT2pkGfkvcMRWAN.jpeg', NULL, '2020-06-07 03:13:27', '2020-06-07 03:13:27'),
(3, 'Jackets', 'yes', 'PCategoryUploads/e37qmXuFIIRTIBGSNptfEMh467bCd5dWgBCeg2LS.jpeg', NULL, '2020-06-15 10:10:46', '2020-06-15 10:12:20');

-- --------------------------------------------------------

--
-- Table structure for table `product_reviews`
--

CREATE TABLE `product_reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_reviews`
--

INSERT INTO `product_reviews` (`id`, `product_id`, `name`, `email`, `message`, `rating`, `created_at`, `updated_at`) VALUES
(1, 2, 'anjit', 'Shresthaanjit3@gmail.com', 'I love this product very much', 5, '2020-06-07 05:16:18', '2020-06-07 05:16:18'),
(2, 4, 'Anjit Shrestha', 'Shresthaanjit3@gmail.com', 'This is a good product.', 4, '2020-06-15 15:29:18', '2020-06-15 15:29:18'),
(3, 5, 'Anjit', 'anjitshrestha5843@gmail.com', 'I liked this jacket.', 3, '2020-06-16 04:29:37', '2020-06-16 04:29:37'),
(4, 2, 'Rohit Dhimal', 'rohit@gmail.com', 'This is a nice t-shirt.', 4, '2020-06-16 13:19:25', '2020-06-16 13:19:25'),
(5, 5, 'Rohit Dhimal', 'rohit@gmail.com', 'I like this product.', 4, '2020-06-17 02:21:32', '2020-06-17 02:21:32'),
(6, 6, 'romiya rai', 'romiya@gmail.com', 'I like this product very much.', 5, '2020-06-17 20:28:08', '2020-06-17 20:28:08'),
(7, 2, 'Muna Rai', 'munna@gmail.com', 'I like this product', 4, '2020-06-17 23:52:34', '2020-06-17 23:52:34');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'user', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

CREATE TABLE `role_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`id`, `role_id`, `user_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slide_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slide_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `slide_name`, `slide_image`, `created_at`, `updated_at`) VALUES
(1, 'slide-1', 'Slides/tYj1uwThNQ0KgJRPzA6syIdiYQv7D7g0FfzWef3g.jpeg', '2020-06-07 06:55:20', '2020-06-07 06:55:20'),
(2, 'slide-2', 'Slides/EjntlYr5V3yPaGddCmRh1iEiK3Yk5tbunFOtvLwa.jpeg', '2020-06-07 06:55:35', '2020-06-07 06:55:35');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` tinyint(4) DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_job` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_user` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `admin`, `country`, `city`, `address`, `contact`, `image`, `user_job`, `about_user`, `provider`, `provider_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Anjit', 'Shresthaanjit3@gmail.com', '2020-06-07 01:49:30', '$2y$10$z0h5onzt4Ohun.wLCnFeFuKvmiUgAtZRcU.qu90Ed20gPrZA6anGC', NULL, 'Nepal', 'Sankhuwasava', 'Kapan', '9810220552', 'uploads/7Hhfr6WbRQ5TnM69FZTgYeLyWWv6LOSduxGrzKF0.jpeg', NULL, NULL, NULL, NULL, NULL, '2020-06-07 01:49:03', '2020-06-18 00:31:19'),
(2, 'anjit', 'anjitshrestha3@gmail.com', '2020-06-16 04:13:46', '$2y$10$nPuILJA35te0X/2gwIxdB.ai92wlTCgN1tRam7OsfHmCSqG/fXdy6', NULL, 'Nepal', 'Kathmandu', 'Kapan', '9810220552', 'uploads/yq3cJKahBjNka5ZOXcIVnO5NjmVT74h0TV3skXNg.jpeg', NULL, NULL, 'GOOGLE', '104744697600980756215', 'yXoiZkpobmQEcd6B8TzjpBzfVCq8OyNAvvMDFYPv6Qsq7uBYv5dSCSzeppBm', '2020-06-16 04:09:52', '2020-06-16 05:10:13'),
(3, 'Rohit Dhimal', 'rohit@gmail.com', '2020-06-16 13:05:02', '$2y$10$of7INU96PaBzA4i6ddudTO2AipH.rwkpuo01593Z6yAOP4I0ivmnG', NULL, 'Nepal', 'Kathmandu', 'Kapan', '9800112233', 'uploads/2bKdCLkLrWt1aQLn8yCTx0GTuRTHhy8S4hwoAkca.jpeg', NULL, NULL, NULL, NULL, 'k2wsHydiOmUkHPZaYIHRxhCQXXxHdzJUa0cQ37waNJnQieD5xCQENJtHQj6Q', '2020-06-16 13:04:50', '2020-06-16 13:16:41'),
(4, 'romiya rai', 'romiya@gmail.com', '2020-06-17 20:26:59', '$2y$10$lUkAWRSJS/vwXIpxrqpmZ.VuBv8ZIVklysA6IYFEoGxG6vp03Vwfq', NULL, 'Nepal', 'Illam', 'Illam', '9810220598', 'uploads/zOwOxMaP8E9qA9SiL9ZlfXZEELOqdVMuax9NSKww.jpeg', NULL, NULL, NULL, NULL, NULL, '2020-06-17 20:25:57', '2020-06-17 20:26:59'),
(5, 'Muna Rai', 'munna@gmail.com', '2020-06-17 23:49:41', '$2y$10$XvTmIC87j3opBZl2AqNhZuMlfRsoEBU1J4vlDTu7N9I.bxOYA6/ne', NULL, 'Nepal', 'Kathmandu', 'Kapan', '9810220552', 'uploads/USrB2kME1UIaMgDPTcMEdHEWsD0TobsoRs0Mlv7f.jpeg', NULL, NULL, NULL, NULL, NULL, '2020-06-17 23:49:21', '2020-06-17 23:49:41');

-- --------------------------------------------------------

--
-- Table structure for table `wish_list`
--

CREATE TABLE `wish_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `products_id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `user_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `box_sections`
--
ALTER TABLE `box_sections`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`box_title`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`name`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_address`
--
ALTER TABLE `delivery_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_galleries`
--
ALTER TABLE `image_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manufacturers`
--
ALTER TABLE `manufacturers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`manufacturer_name`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_atrrs`
--
ALTER TABLE `product_atrrs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_bundle_relations`
--
ALTER TABLE `product_bundle_relations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`p_cat_name`);

--
-- Indexes for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_users_role_id_foreign` (`role_id`),
  ADD KEY `role_users_user_id_foreign` (`user_id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `wish_list`
--
ALTER TABLE `wish_list`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `box_sections`
--
ALTER TABLE `box_sections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `delivery_address`
--
ALTER TABLE `delivery_address`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `image_galleries`
--
ALTER TABLE `image_galleries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `manufacturers`
--
ALTER TABLE `manufacturers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product_atrrs`
--
ALTER TABLE `product_atrrs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `product_bundle_relations`
--
ALTER TABLE `product_bundle_relations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role_users`
--
ALTER TABLE `role_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wish_list`
--
ALTER TABLE `wish_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `role_users`
--
ALTER TABLE `role_users`
  ADD CONSTRAINT `role_users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
