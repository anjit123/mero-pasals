<?php

namespace App;
use App\Product;
use App\Bundle;
use Illuminate\Database\Eloquent\Model;

class ProductBundleRelation extends Model
{
    //
    protected $table='product_bundle_relations';
    protected $primaryKey='id';
    protected $fillable=['rel_title','product_id','bundle_id'];


    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');

    }

    public function bundle()
    {
        return $this->belongsTo(Bundle::class,'bundle_id','id');
        
    }
}
