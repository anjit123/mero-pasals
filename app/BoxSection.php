<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoxSection extends Model
{
    //
    protected $table='box_sections';
    protected $primaryKey='id';
    protected $fillable=['box_title', 'box_desc'];
}

