<?php

namespace App;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    //
    protected $table='product_categories';
    protected $primaryKey='id';
    protected $fillable=['p_cat_name','p_cat_top','p_cat_image'];


    public function products(){

        return $this->hasMany(Product::class);
    }

    public static function countProductCategory(){

        $countProductCategory = DB::table('product_categories')->count();

        return $countProductCategory;
    }
}
