<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request;
use Closure;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userRoles = Auth::user()->role->pluck('name');
        
        if(!$userRoles->contains('admin')) {
            return redirect('admin_home');
        }
        return $next($request);
    }
    
    // protected function sendLoginResponse(Request $request)
    // {
    //     $request->session()->regenerate();
    //     $this->clearLoginAttempts($request);

    //     foreach ($this->guard()->user()->role as $role){
    //         if($role->name == 'admin') {
    //             return redirect('admin_home');

    //         }
    //     }
    // }
}
