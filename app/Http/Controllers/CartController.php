<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Cart;
use App\User;
use App\Coupon;
use App\Product;
use App\ProductAtrr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // Session::forget('CouponAmount');
        // Session::forget('CouponCode');
        
      if(Auth::check()){
          $user_email = Auth::user()->email;
          $cart_datas = Cart::where(['user_email' => $user_email])->get();
      }else{
        $session_id=Session::get('session_id');
        $cart_datas=Cart::where('session_id',$session_id)->get();
      }
        

        $total_amount=0;
        foreach ($cart_datas as $cart_data){
            $total_amount+=$cart_data->price*$cart_data->quantity;
        }
        return view('frontEnd.cart',compact('cart_datas','total_amount'));
    }



    public function addToCart(Request $request){

        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        $inputToCart=$request->all();
        // echo "<pre>"; print_r($inputToCart);die;

    if(!empty($inputToCart['wishListButton']) && $inputToCart['wishListButton']=="Wish List"){
        // echo "Wish List is selected";die;

        //check user is login or not
        if(!Auth::check()){
            return redirect()->back()->with('message','Please login to add product in wish list');
        }
        //if size selected or not
        if(empty($inputToCart['size'])){
            return redirect()->back()->with('message','Please select size to add product in your wish list');
        }
        if(empty($inputToCart['color'])){
            return redirect()->back()->with('message','Please select color to add product in your wish list');
        }
        // if(empty($inputToCart['price'])){
        //     return redirect()->back()->with('message','This is not in the stock');
        // }
        $sizeAtrr = explode('-', $inputToCart['size']);
        $product_size = $sizeAtrr[1];
        $sizeAtrr1 = explode('-', $inputToCart['color']);
        $product_color = $sizeAtrr1[1];
        // $sizeAtrr2 = explode('-', $inputToCart['price']);
        // $product_price = $sizeAtrr2[1];

        //product price

        $proPrice = ProductAtrr::where(['products_id'=>$inputToCart['products_id'],'size'=>$product_size,'color'=>$product_color])->first();

        $product_price = $proPrice->price;
        $product_code = $proPrice->sku;

        //set user Email address
        $user_email = Auth::user()->email;

        //set quantity 1
        $quantity = 1;

        //get current date
        $created_at = Carbon::now();

        $wishListCount = DB::table('wish_list')->where(['user_email'=>$user_email, 'products_id'=>$inputToCart['products_id'],
            'size'=>$product_size,'color'=>$product_color])->count();

        if($wishListCount>0){
            return redirect()->back()->with('message','Product already in the wish list');

        }else{
            DB::table('wish_list')->insert(['products_id'=>$inputToCart['products_id'], 'product_name'=>$inputToCart['product_name'],'product_code'=>$product_code,
            'size'=>$product_size,'color'=>$product_color,'price'=>$inputToCart['price'],'quantity'=>$quantity,'user_email'=>$user_email,'created_at'=>$created_at]);
    
            return redirect()->back()->with('message','Product has been added to the wish list');

        }
       
        // echo "Working file";die;
    }else{
        // echo "Add to Cart is selected";die;

        //if product added from wish list
        if(!empty($inputToCart['cartButton']) && $inputToCart['cartButton'] == "Add to Cart"){
            // echo "test";die;
        // echo "<pre>"; print_r($inputToCart);die;
            $inputToCart['quantity']=1;

        }
        
        if($inputToCart['size']==""){
            return back()->with('message','Please select Size');
        }else{
            $stockAvailable=DB::table('product_atrrs')->select('stock','sku')->where(['products_id'=>$inputToCart['products_id'],
            'price'=>$inputToCart['price']])->first();
            // echo $stockAvailable->stock;die;
            if($stockAvailable->stock>=$inputToCart['quantity']){
                // $inputToCart['user_email']='Shresthaanjit3@gmail.com';
                if(empty(Auth::user()->email)){
                    $inputToCart['user_email'] = '';
                }else{
                    $inputToCart['user_email'] = Auth::user()->email;
                }
                $session_id=Session::get('session_id');
                if(empty($session_id)){
                    $session_id=str_random(40);
                    Session::put('session_id',$session_id);
                }
                $inputToCart['session_id']=$session_id;
                $sizeAtrr=explode("-",$inputToCart['size']);
                $inputToCart['size']=$sizeAtrr[1];
                $inputToCart['product_code']=$stockAvailable->sku;
                $count_duplicateItems=Cart::where(['products_id'=>$inputToCart['products_id'],
                'size'=>$inputToCart['size']])->count();
                if($count_duplicateItems>0){
                    return back()->with('message','This Item Added already');
                }else{
                    Cart::create($inputToCart);
                    return back()->with('message','Cart added Successfully');
                }
            }else{
                return back()->with('message','Stock is not Available!');
            }
        }
    }
}

//listing wish list of the user
    public function wishList(){
        $user_id = Auth::user()->id;
        $accounts=User::where('id',$user_id)->first();
        $user_email = Auth::user()->email;
        $userWishList = DB::table('wish_list')->where('user_email',$user_email)->get();
        foreach($userWishList as $key => $product){
            $productDetails = Product::where('id',$product->products_id)->first();
            $userWishList[$key]->image = $productDetails->image;
        }
        $meta_title = "Wish List - E-com Website";
        $meta_description = "View Wish List of E-com Website";
        $meta_keywords = "wish list, e-com website";

        return view('users.mywishlist')->with(compact('userWishList','meta_title','meta_description','meta_keywords','accounts'));

    }



    
    public function updateQuantity($id,$quantity){

        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        Session::forget('discount_amount_price');
        Session::forget('coupon_code');
        $sku_size=DB::table('carts')->select('product_code','size','quantity')->where('id',$id)->first();
        $stockAvailable=DB::table('product_atrrs')->select('stock')->where(['sku'=>$sku_size->product_code,
            'size'=>$sku_size->size])->first();
        $updated_quantity=$sku_size->quantity+$quantity;
        if($stockAvailable->stock>=$updated_quantity){
            DB::table('carts')->where('id',$id)->increment('quantity',$quantity);
            return back()->with('message','Update Quantity Successfully');
        }else{
            return back()->with('message','Stock is not Available!');
        }


    }

    public function deleteItem($id=null){
        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        $delete_item=Cart::findOrFail($id);
        Session::forget('discount_amount_price');
        Session::forget('coupon_code');
        $delete_item->delete();
        return back()->with('message','Item Deleted Successfully');
    }


    public function applyCoupon(Request $request){

        Session::forget('CouponAmount');
        Session::forget('CouponCode');

        $data = $request->all();

        $couponCount = Coupon::where('coupon_code',$data['coupon_code'])->count();
        if($couponCount == 0){
            return redirect()->back()->with('message','Coupon is not exists');
        }else{
            // echo "Success"; die;
            //with perform other checks like Active/Inactive, Expiry Date.

        $couponDetails = Coupon::where('coupon_code',$data['coupon_code'])->first();

        //if coupon is inactive
        if($couponDetails->status==0){
            return redirect()->back()->with('message','This coupon is not active');

        }

        //if coupon is expired
        $expiry_date = $couponDetails->expiry_date;
        $current_date = date('Y-m-d');
        if($expiry_date < $current_date){
            return redirect()->back()->with('message','This coupon is Expired');
            
        }

        //Coupon is valid for Discount

        //check if amount type is Fixed or Percentage

        //Get Cart Total Amount
 
        $session_id=Session::get('session_id');
        // $userCart=DB::table('carts')->where(['session_id' => $session_id])->get();
        
        if(Auth::check()){
        
            $user_email = Auth::user()->email;
            $userCart=DB::table('carts')->where(['user_email' => $user_email])->get();
            
        }else{
            $session_id=Session::get('session_id');
            $userCart=DB::table('carts')->where(['session_id' => $session_id])->get();
        }

        $total_amount=0;
        foreach ($userCart as $item){
            $total_amount = $total_amount + ($item->price * $item->quantity);
        }

        if($couponDetails->amount_type=="Fixed"){
            $couponAmount = $couponDetails->amount;
        }else{
            $couponAmount = $total_amount * ($couponDetails->amount/100);
        }

        // Add Coupon code & amount in session
        Session::put('CouponAmount',$couponAmount);
        Session::put('CouponCode',$data['coupon_code']);

        return redirect()->back()->with('message','Coupon code successfully applied. You are availing discount!');
        }
      
    }



   public function deleteWishListProducts($id){

        DB::table('wish_list')->where('id',$id)->delete();
        return redirect()->back()->with('message','Product has been deleted from Wish List');
   }




   


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
