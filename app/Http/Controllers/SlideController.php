<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;
use Intervention\Image\Facades\Image;
class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $menu_active=0;
        $slides=Slide::all();

        // $slides=Slide::all();
        return view('backEnd.slide.index',compact('menu_active','slides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $menu_active=2;

        return view('backEnd.slide.create',compact('menu_active'));

    }

    public function checkSlideName(Request $request){
        $data=$request->all();
        $slide_name=$data['slide_name'];
        $ch_slide_name_atDB=Slide::select('slide_name')->where('slide_name',$slide_name)->first();
        if($slide_name==$ch_slide_name_atDB['slide_name']){
            echo "true"; die();
        }else {
            echo "false"; die();
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'slide_name'=>'required|max:255|unique:slides,slide_name',
            // 'slide_url'=>'required',
        ]);
        $slide_imagePath = request('slide_image')->store('Slides','public');
        $slide_image = Image::make(public_path("storage/{$slide_imagePath}"));
        $slide_image->save();
        // dd($request->all());
        $data=$request->all();
        

        Slide::create([
            'slide_name'=> $data['slide_name'],
            'slide_image' => $slide_imagePath,
            // 'slide_url' => $data['slide_url'],
        ]);
        return redirect()->route('slide.index')->with('message','Added Success!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $menu_active=0;
        $edit_slide=Slide::findOrFail($id);
        return view('backEnd.slide.edit',compact('edit_slide','menu_active'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $menu_active=0;
        // $slides = Slide::all();

        $update_slides=Slide::findOrFail($id);

        $this->validate($request,[
            'slide_name'=>'required|max:255',
            // 'slide_url'=>'required',
        ]);
        $slide_imagePath = request('slide_image')->store('Slides','public');
        $slide_image = Image::make(public_path("storage/{$slide_imagePath}"));
        $slide_image->save();
        // dd($request->all());
       
        $data=$request->all();
        
        $update_slides->update([
            'slide_name'=> $data['slide_name'],
            'slide_image' => $slide_imagePath,
            // 'slide_url' => $data['slide_url'],
        ]);
        return redirect()->route('slide.index')->with('message','Updated Success!');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete=Slide::findOrFail($id);
        $delete->delete();
        return redirect()->route('slide.index')->with('message','Delete Success!');
    }
}
