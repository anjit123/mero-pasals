<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manufacturer;
use Intervention\Image\Facades\Image;
class ManufacturerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     ///view manufacturer
    public function index()
    {
        //
        $menu_active=0;
       
        $manufacturers = Manufacturer::all();
        return view('backEnd.manufacturers.index',compact('menu_active','manufacturers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
        $menu_active=2;

        return view('backEnd.manufacturers.create',compact('menu_active'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //store manufacturer
    public function store(Request $request)
    {
        //
        
        
        $data = request()->validate([
            'manufacturer_name'=>'required|max:255|unique:manufacturers,manufacturer_name',
            'manufacturer_top'=>'required',
            'manufacturer_image' => ['required','image'],
        ]);
        $manufacturer_imagePath = request('manufacturer_image')->store('ManufacturerUploads','public');
        $manufacturer_image = Image::make(public_path("storage/{$manufacturer_imagePath}"));
        $manufacturer_image->save();
        // $data=$request->all();

        Manufacturer::create([
            'manufacturer_name' => $data['manufacturer_name'],
            'manufacturer_top' => $data['manufacturer_top'],
            'manufacturer_image' => $manufacturer_imagePath,


        ]);
        return redirect()->route('manufacturer.index')->with('message','Manufacturer Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
     //edit manufacturer
    public function edit($id)
    {
        //
        $menu_active=0;
       
        $edit_manufacturer=Manufacturer::findOrFail($id);
        return view('backEnd.manufacturers.edit',compact('edit_manufacturer','menu_active'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    ///update manufacturer/brands
    public function update(Request $request, $id)
    {
        //
        $menu_active=0;

        $update_manufacturers=Manufacturer::findOrFail($id);
        $this->validate($request,[
        
            'manufacturer_name'=>'required|max:255|unique:categories,name',
            'manufacturer_top'=>'required',
            'manufacturer_image' => ['required','image'],
        ]);
        $manufacturer_imagePath = request('manufacturer_image')->store('ManufacturerUploads','public');
        $manufacturer_image = Image::make(public_path("storage/{$manufacturer_imagePath}"));
        $manufacturer_image->save();
        $data=$request->all();
        

        $update_manufacturers->update([
        
            'manufacturer_name' => $data['manufacturer_name'],
            'manufacturer_top' => $data['manufacturer_top'],
            'manufacturer_image' => $manufacturer_imagePath,


        ]);
        return redirect()->route('manufacturer.index')->with('message','Manufacturer Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     //remove manufacturer/brands
    public function destroy($id)
    {
        //
        $delete=Manufacturer::findOrFail($id);
        $delete->delete();
        return redirect()->route('manufacturer.index')->with('message',' Manufacturer Deleted Successfully');
    }
}
