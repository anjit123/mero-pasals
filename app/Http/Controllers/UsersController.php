<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Order;
use App\OrderProduct;
// use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

use Image;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //
        return view('users.user_login');


        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('users.user_register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = request()->validate([
            'name'=>'string|max:255',
            'email'=>'required|string|email|unique:users,email',
            'password'=>'required|min:6|confirmed',
            'country'=>'required|string|max:255',
            'city'=>'required|string|max:255',
            'address'=>'required|string|max:255',
            'contact'=>'required|min:11|numeric',
            'image' => ['required','image'],

        ]);
        // dd(request()->all());
        $imagePath = request('image')->store('uploads','public');
        $image = Image::make(public_path("storage/{$imagePath}"))->fit(500,500);
        $image->save();
        
        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password'=> Hash::make($data['password']),
            'country' => $data['country'],
            'city' => $data['city'],
            'address' => $data['address'],
            'contact' => $data['contact'],
            'image' => $imagePath,

        ]);

        return redirect('/');


        //dd(request()->all());
    }
    public function account(User $id){

        // $accounts=User::all();
        $user_id = Auth::user()->id;
        $accounts=User::where('id',$user_id)->first();
        
        $orders=Order::with('orderss')->where('users_id',$user_id)->get();

        $orders = json_decode(json_encode($orders));
        

        // echo "<pre>"; print_r($orders);die;
       
        
        //  $user_order = Order::all();
        return view('users.account',compact('accounts','orders'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
        if(Auth::User()){
            $users = User::find(Auth::user()->id);
            $users = User::all();
            // $accounts=User::all();
           $accounts= User::where('id',Auth::id())->first();
            if($users){
                return view('users.updateProfile',compact('users','accounts'));

            } else{
                return redirect()->back();

            }
        }else{
            return redirect()->back();
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateprofile(Request $request, $id)
    {
        //
        // $accounts=User::all();
        $accounts= User::where('id',Auth::id())->first();

        // $orders = Order::findOrFail($id);

        $this->validate($request,[
            'name'=>'required',
            'email'=>'required|unique:users,email',
            'country'=>'required',
            'city'=>'required',
            'address'=>'required',
            'contact'=>'required',
            'image' => ['required','image'],

        ]);
        // dd(request()->all());
        $imagePath = request('image')->store('uploads','public');
        $image = Image::make(public_path("storage/{$imagePath}"))->fit(500,500);
        $image->save();

        // dd(request()->all());
        $input_data=$request->all();
        
        // DB::table('users')->where('id',$id)->update([
        User::where('id',$id)->update([
            'name'=>$input_data['name'],
            'email'=>$input_data['email'],
            'country'=>$input_data['country'],
            'city'=>$input_data['city'],
            'address'=>$input_data['address'],
            'contact'=>$input_data['contact'],
            'image'=>$imagePath,
        ]);
        // return view('users.account',compact('accounts','orders'));
        return back()->with('message','Update Profile Successfully');


    }
    public function showpassword(){
        // $accounts=User::all();
        $accounts= User::where('id',Auth::id())->first();

        return view('users.changePassword',compact('accounts'));
    }

    public function updatepassword(Request $request,$id){

        $oldPassword=User::where('id',$id)->first();
        $input_data=$request->all();
        if(Hash::check($input_data['password'],$oldPassword->password)){
            $this->validate($request,[
               'newPassword'=>'required|min:6|max:12|confirmed'
            ]);
            $new_pass=Hash::make($input_data['newPassword']);
            User::where('id',$id)->update(['password'=>$new_pass]);
            return back()->with('message','Update Password Successfully');
            
        }else{
            return back()->with('oldPassword','Old Password is Inconrrect!');
        }
    }


    public function payoffline(User $id){

        $accounts= User::where('id',Auth::id())->first();

        return view('users.payoffline',compact('accounts'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
    }

    public function login(Request $request){
        $data = request()->validate([
            'email'=>'required|email',
            'password'=>'required',
        ]);

        $data = $request->all();
        if(Auth::attempt(['email'=>$data['email'],'password'=>$data['password']])){
            Session::put('frontSession',$data['email']);
            return redirect('/')->with('message','Login Successfully');
        }else{
            return back()->with('message','Email Address or Password is not Valid!');
        }

    }




    // public function forgotPassword(Request $request){

    //     if($request->isMethod('post')){
    //         $data = $request->all();
    //         // echo "<pre>"; print_r($data);die;

    //         $userCount = User::where('email',$data['email'])->count();
    //         if($userCount == 0){
    //             return redirect()->back()->with('message','Email does not exist');
    //         }

    //         //get user details
    //         $userDetails = User::where('email',$data['email'])->first();

    //         //generate random password
    //        $random_password = str_random(8);

    //        //encode/ secure password
    //     $new_password = bcrypt($random_password);
        
    //     //update password
    //     User::where('email',$data['email'])->update(['password'=>$new_password]);

    //     //send forgot password email code
    //     $email = $data['email'];
    //     $name = $userDetails->name;
    //     $messageData = [
    //         'email'=>$email,
    //         'name'=>$name,
    //         'password'=>$random_password
    //     ];

    //     Mail::send('users.forgotpassword',$messageData,function($message)use($email){
    //         $message->to($email)->subject('New Password - mero pasal');

    //     });
        
    // }
    // return view('users.forgot_password');
    // }


    public function logout(){
        Auth::logout();
        Session::forget('frontSession');
        return redirect('/');
    }



    
}
