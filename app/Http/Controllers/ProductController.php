<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Category;
use App\Manufacturer;
use App\ProductCategory;
use App\Product;
use App\ProductAtrr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
// use Image;
use Intervention\Image\Facades\Image;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $menu_active=2;
        $i=0;
        $categories=Category::all();
        $p_cats = ProductCategory::all();
        $products=Product::where('status','product')->orderBy('created_at','desc')->paginate(5);
        return view('backEnd.products.index',compact('menu_active','products','i','categories','p_cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $menu_active=2;
        $categories=Category::where('parent_id',0)->pluck('name','id')->all();
        $manufacturers=Manufacturer::pluck('manufacturer_name','id')->all();
        $product_category=ProductCategory::pluck('p_cat_name','id')->all();

        return view('backEnd.products.create',compact('menu_active','categories','manufacturers','product_category'));

    }
   


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $data = request()->validate([
            $this->validate($request,[
            // 'manufacture_id'=>'required',
            // 'p_cat_id'=>'required',
            // 'cat_id'=>'required',
            'product_title'=>'required|min:5',
            'product_color'=>'required',
            'image'=>'required|image|mimes:png,jpg,jpeg|max:1000',
            'product_price'=>'required',
            'psale_price'=>'required',
            'product_keyword'=>'required',
            'product_desc'=>'required',
            'product_feature'=>'required',
            'product_label'=>'required',
            'status'=>'required'
            

        ]);
        $formInput=$request->all();
        if($request->file('image')){
            $image=$request->file('image');
            if($image->isValid()){
                // $fileName=time().'-'.str_slug($formInput['product_title'],"-").'.'.$image->getClientOriginalExtension();
                $extension = $image->getClientOriginalExtension();
                $fileName = rand(111,99999).'.'.$extension;
                $large_image_path=public_path('products/large/'.$fileName);
                $medium_image_path=public_path('products/medium/'.$fileName);
                $small_image_path=public_path('products/small/'.$fileName);
                //Resize Image
                Image::make($image)->save($large_image_path);
                Image::make($image)->resize(600,600)->save($medium_image_path);
                Image::make($image)->resize(300,300)->save($small_image_path);
                $formInput['image']=$fileName;
            }
        }
        Product::create($formInput);
    //     Product::create([
    //         // 'manufacture_id'=> $data['manufacture_id'],
    //         // 'p_cat_id'=> $data['p_cat_id'],
    //         // 'cat_id'=> $data['cat_id'],
    //         'product_title' => $data['product_title'],
    //         'product_url' => $data['product_url'],
    // // 'image' => $imagePath,
    //         'product_price' => $data['product_price'],
    //         'psale_price' => $data['psale_price'],
    //         'product_keyword' => $data['product_keyword'],
    //         'product_desc' => $data['product_desc'],
    //         'product_feature' => $data['product_feature'],
    //         'product_label' => $data['product_label'],

            
    //     ]);
        return redirect()->route('product.index')->with('message','Products Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $menu_active=2;
        $categories=Category::where('parent_id',0)->pluck('name','id')->all();
        $manufacturers=Manufacturer::pluck('manufacturer_name','id')->all();
        $product_category=ProductCategory::pluck('p_cat_name','id')->all();
        $edit_product=Product::findOrFail($id);
        $edit_manufacturer=Manufacturer::findOrFail($edit_product->manufacturer_id);
        $edit_p_cats=ProductCategory::findOrFail($edit_product->p_cat_id);
        $edit_category=Category::findOrFail($edit_product->cat_id);

        return view('backEnd.products.edit',compact('edit_product','menu_active','categories','manufacturers','product_category','edit_category','edit_manufacturer','edit_p_cats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $update_product=Product::findOrFail($id);
        $this->validate($request,[
            // 'manufacture_id'=>'required',
            // 'p_cat_id'=>'required',
            // 'cat_id'=>'required',
            'product_title'=>'required|min:5',
            'product_color'=>'required',
            'image'=>'image|mimes:png,jpg,jpeg|max:1000',
            'product_price'=>'required',
            'psale_price'=>'required',
            'product_keyword'=>'required',
            'product_desc'=>'required',
            'product_feature'=>'required',
            'product_label'=>'required',
        ]);
        $formInput=$request->all();
        // if($update_product['image']==''){
            if($request->file('image')){
                $image=$request->file('image');
                if($image->isValid()){
                    // $fileName=time().'-'.str_slug($formInput['product_title'],"-").'.'.$image->getClientOriginalExtension();
                    $extension = $image->getClientOriginalExtension();
                    $fileName = rand(111,99999).'.'.$extension;
                    $large_image_path=public_path('products/large/'.$fileName);
                    $medium_image_path=public_path('products/medium/'.$fileName);
                    $small_image_path=public_path('products/small/'.$fileName);
                    //Resize Image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600,600)->save($medium_image_path);
                    Image::make($image)->resize(300,300)->save($small_image_path);
                    $formInput['image']=$fileName;
                }
            // }
        }else{
            $formInput['image']=$update_product['image'];
        }
        $update_product->update($formInput);
        return redirect()->route('product.index')->with('message','Update Products Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $delete=Product::findOrFail($id);
        $image_large=public_path().'/products/large/'.$delete->image;
        $image_medium=public_path().'/products/medium/'.$delete->image;
        $image_small=public_path().'/products/small/'.$delete->image;
        if($delete->delete()){
            unlink($image_large);
            unlink($image_medium);
            unlink($image_small);
        }
        return redirect()->route('product.index')->with('message','Delete Success!');

    }

    public function deleteImage($id){
        $delete_image=Product::findOrFail($id);
        $image_large=public_path().'/products/large/'.$delete_image->image;
        $image_medium=public_path().'/products/medium/'.$delete_image->image;
        $image_small=public_path().'/products/small/'.$delete_image->image;
        if($delete_image){
            $delete_image->image='';
            $delete_image->save();
            ////// delete image ///
            unlink($image_large);
            unlink($image_medium);
            unlink($image_small);
        }
        return back();
    }


    public function search(Request $request){
        $menu_active=0;

        $search = $request->get('search');
        $products = DB::table('products')->where('product_title','like', '%'.$search.'%')->paginate(5);
        return view('frontEnd.shop.index',['products' => $products],['menu_active' =>$menu_active]);
        
    }

//getting product attribute price function
    public function getProductPrice(Request $request){
        $data = $request->all();
        // echo "<pre>"; print_r($data); die;
        $proArr = explode("-",$data['idSize']);
        // echo $proArr[0]; echo $proArr[1]; die;
        $proArr = ProductAtrr::where(['products_id' => $proArr[0], 'size' => $proArr[1]])->first();
        echo $proArr->price;
        // echo $proArr->stock;
    }
    public function getProductQty(Request $request){
        $data = $request->all();
        // echo "<pre>"; print_r($data); die;
        $proArr = explode("-",$data['idColor']);
        // echo $proArr[0]; echo $proArr[1]; die;
        $proArr = ProductAtrr::where(['products_id' => $proArr[0], 'color' => $proArr[1]])->first();
        echo $proArr->stock;
    }




    
    public function searchProducts(Request $request){

        $menu_active=2;
        $i=0;

        if($request->isMethod('post')){
            $data = $request->all();
            // echo "<pre>"; print_r($data); die;
            // $bundles=Product::where('status','bundle')->get();
            $search_product = $data['product'];
            $products=Product::where('product_keyword','like','%'.$search_product.'%')->orwhere('product_title','like','%'.$search_product.'%')->where('status','product')->paginate(5);

            return view('backEnd.products.index',compact('menu_active','products','i'));
        }

    }

    
}
