<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BoxSection;
class BoxSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $menu_active=0;
        $box_sections=BoxSection::all();

        return view('backEnd.boxsection.index',compact('menu_active','box_sections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $menu_active=2;

        return view('backEnd.boxsection.create',compact('menu_active'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = request()->validate([
            'box_title'=>'required|max:255|unique:box_sections,box_title',
            'box_desc'=>'required',
            
        ]);
        BoxSection::create([
            'box_title' => $data['box_title'],
            'box_desc' => $data['box_desc'],

        ]);
        return redirect()->route('box.index')->with('message','Added Success!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $menu_active=0;
        $edit_boxs=BoxSection::findOrFail($id);
        return view('backEnd.boxsection.edit',compact('edit_boxs','menu_active'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $menu_active=0;
        $update_boxs=BoxSection::findOrFail($id);
        $this->validate($request,[
            'box_title'=>'required|max:255',
            'box_desc'=>'required',
            
        ]);

        $formInput=$request->all();
        $update_boxs->update($formInput);
        return redirect()->route('box.index')->with('message','Update Box Section Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete=BoxSection::findOrFail($id);
        $delete->delete();
        return redirect()->route('box.index')->with('message','Delete Success!');

    }
}
