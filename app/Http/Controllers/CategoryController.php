<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Intervention\Image\Facades\Image;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $menu_active=0;
        $categories=Category::paginate(5);
        return view('backEnd.categories.index',compact('menu_active','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $menu_active=2;
        $plucked=Category::where('parent_id',0)->pluck('name','id');
        $cate_levels=['0'=>'Main Category']+$plucked->all();

        return view('backEnd.categories.create',compact('menu_active','cate_levels'));
    }
    public function checkCateName(Request $request){
        $data=$request->all();
        $category_name=$data['name'];
        $ch_cate_name_atDB=Category::select('name')->where('name',$category_name)->first();
        if($category_name==$ch_cate_name_atDB['name']){
            echo "true"; die();
        }else {
            echo "false"; die();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $this->validate($request,
        $data = request()->validate([
            'name'=>'required|max:255|unique:categories,name',
            'cat_top'=>'required',
            'cat_image' => ['required','image'],
        ]);
        $cat_imagePath = request('cat_image')->store('CategoryUploads','public');
        $cat_image = Image::make(public_path("storage/{$cat_imagePath}"))->fit(500,500);
        $cat_image->save();
        // $data=$request->all();
        // Category::create($data);

        Category::create([
            'name' => $data['name'],
            'cat_top' => $data['cat_top'],
            'cat_image' => $cat_imagePath,


        ]);
        return redirect()->route('category.index')->with('message','Category Added Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        echo $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $menu_active=0;
        $plucked=Category::where('parent_id',0)->pluck('name','id');
        $cate_levels=['0'=>'Main Category']+$plucked->all();
        $edit_category=Category::findOrFail($id);
        return view('backEnd.categories.edit',compact('edit_category','menu_active','cate_levels'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $menu_active=0;

        $categories=Category::all();

        $update_categories=Category::findOrFail($id);
        $this->validate($request,[
            'name'=>'required|max:255|unique:categories,name,'.$update_categories->id,
            'cat_top'=>'required',
            'cat_image' => ['required','image'],
        ]);
        //dd($request->all());die();
        $cat_imagePath = request('cat_image')->store('CategoryUploads','public');
        $cat_image = Image::make(public_path("storage/{$cat_imagePath}"))->fit(500,500);
        $cat_image->save();
        $data=$request->all();
       
       
        $update_categories->update([
            'name' => $data['name'],
            'cat_top' => $data['cat_top'],
            'cat_image' => $cat_imagePath,
        ]);
        // return view('backEnd.categories.index', compact('menu_active','categories'));
        // return redirect()->route('backEnd.categories.index', compact('menu_active','categories'));

        return redirect()->route('category.index')->with('message','Category Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $delete=Category::findOrFail($id);
        $delete->delete();
        return redirect()->route('category.index')->with('message','Category Deleted Successfully');
    }
}
