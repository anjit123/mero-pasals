<?php

namespace App\Http\Controllers;
use App\Category;
use App\Manufacturer;
use App\ProductCategory;
use App\Product;
use Illuminate\Http\Request;

class BundleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $menu_active=3;
        $i=0;
        $categories=Category::all();
        $p_cats = ProductCategory::all();
        $products=Product::where('status','bundle')->orderBy('created_at','desc')->get();
        return view('backEnd.bundle.index',compact('menu_active','products','i','categories','p_cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create_bundle()
    {
        //
        $menu_active=3;
        $categories=Category::where('parent_id',0)->pluck('name','id')->all();
        $manufacturers=Manufacturer::pluck('manufacturer_name','id')->all();
        $product_category=ProductCategory::pluck('p_cat_name','id')->all();

        return view('backEnd.products.create_bundle',compact('menu_active','categories','manufacturers','product_category'));

    }








    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
