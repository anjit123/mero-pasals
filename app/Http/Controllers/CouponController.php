<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coupon;
class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $menu_active=0;
        $coupons = Coupon::get();
        return view('backEnd.coupon.index',compact('menu_active','coupons'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $menu_active=2;

        return view('backEnd.coupon.create',compact('menu_active'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $data = request()->validate([
        //     'coupon_code'=>'required',
        //     'amount'=>'required',
        //     'amount_type'=>'required',
        //     'expiry_date'=>'required',
        //     'status'=>'',
            
        // ]);
        // Coupon::create([
        //     'coupon_code' => $data['coupon_code'],
        //     'amount' => $data['amount'],
        //     'amount_type' => $data['amount_type'],
        //     'expiry_date' => $data['expiry_date'],
        //     'status' => $data['status'],

        // ]);
        $data = $request->all();
        $coupon = new Coupon;
        $coupon->coupon_code = $data['coupon_code'];
        $coupon->amount = $data['amount'];
        $coupon->amount_type = $data['amount_type'];
        $coupon->expiry_date = $data['expiry_date'];
        if(empty($data['status'])){
            $data['status'] = 0;
        }
        $coupon->status = $data['status'];
        $coupon->save();
        return redirect()->route('coupon.index')->with('message','Added Success!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $menu_active=0;

        $couponDetails = Coupon::find($id);

        return view('backEnd.coupon.edit',compact('menu_active','couponDetails'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $menu_active=0;

        $update_coupon=Coupon::findOrFail($id);
        $data = $request->all();

        $update_coupon->coupon_code = $data['coupon_code'];
        $update_coupon->amount = $data['amount'];
        $update_coupon->amount_type = $data['amount_type'];
        $update_coupon->expiry_date = $data['expiry_date'];
        if(empty($data['status'])){
            $data['status'] = 0;
        }
        $update_coupon->status = $data['status'];
        $update_coupon->save();
        return redirect()->route('coupon.index')->with('message','Updated Success!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $delete=Coupon::findOrFail($id);
        $delete->delete();
        return redirect()->route('coupon.index')->with('message','Delete Success!');
    }
}
