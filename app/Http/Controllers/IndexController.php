<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;
use App\BoxSection;
use App\Product;
use App\Manufacturer;
use App\ImageGallery;
use App\ProductAtrr;
use App\Cart;
class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     //fornt-End Index page/homepage
    public function index()
    {
        //
        $slides = Slide::get();
        $box_sections=BoxSection::all();
        $products=Product::where('status','product')->paginate(6);
        $manufacturer=Manufacturer::all();
        return view('frontEnd.index',compact('slides','box_sections','products','manufacturer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    


//   //product attrs
//   public function getAttrs(Request $request){
//     $all_attrs=$request->all();
//     //print_r($all_attrs);die();
//     $attr=explode('-',$all_attrs['size']);
//     //echo $attr[0].' <=> '. $attr[1];
//     $result_select=ProductAtrr::where(['products_id'=>$attr[0],'size'=>$attr[1]])->first();
//     echo $result_select->price."#".$result_select->stock."#".$result_select->color;
// }





    public function service(){
        return view('frontEnd.service');
    }

    public function about(){
        return view('frontEnd.aboutus');

    }











    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
