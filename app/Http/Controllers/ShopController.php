<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Manufacturer;
use App\ProductCategory;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use App\Category;
use App\ImageGallery;
use App\ProductAtrr;
use App\Cart;
use App\ProductReview;
class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $url=null)
    {

        // $products=Product::where('status','product')->paginate(3);
        $products=Product::where('status','product')->get();
        $bundles=Product::where('status','bundle')->get();
        

        if(!empty($_GET['manu'])){
            $manuArray = explode('-' ,$_GET['manu']);
            $products = $products->whereIn('manufacturer_id',$manuArray);
        }
        if(!empty($_GET['p_category'])){
            $pcatArray = explode('-' ,$_GET['p_category']);
            $products = $products->whereIn('p_cat_id',$pcatArray);
        }
        if(!empty($_GET['category'])){
            $catArray = explode('-' ,$_GET['category']);
            $products = $products->whereIn('cat_id',$catArray);
        }
       
        return view('frontEnd.shop.index',compact('products','bundles','url'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //// $productsAll = Product::search($s)->paginate(12);

        //product display in descending order
        
        // $productsAll = Product::orderBy('id','DESC')->search($s)->paginate(12);
        //product display in Randomly order
        // $productsAll = Product::inRandomOrder()->search($s)->paginate(12);
        
        //Display Categories or Sub Categories in left Sidebar of Home Page
        // $categories = Category::with('categories')->where(['parent_id'=>0])->get();
    }





    public function filterproduct(Request $request){
        $data = $request->all();
        // echo "<pre>"; print_r($data); die;

        $manufacturerUrl = "";
        if(!empty($data['manufacturerFilter'])){
            foreach($data['manufacturerFilter'] as $manu){
                if(empty($manufacturerUrl)){
                    $manufacturerUrl = "&manu=".$manu;
                }else{
                    $manufacturerUrl .= "-".$manu;
                }
            }

        }
        $pcatUrl = "";
        if(!empty($data['pcatFilter'])){
            foreach($data['pcatFilter'] as $p_category){
                if(empty($pcatUrl)){
                    $pcatUrl = "&p_category=".$p_category;
                }else{
                    $pcatUrl .= "-".$p_category;
                }
            }

        }
        $catUrl = "";
        if(!empty($data['catFilter'])){
            foreach($data['catFilter'] as $category){
                if(empty($catUrl)){
                    $catUrl = "&category=".$category;
                }else{
                    $catUrl .= "-".$category;
                }
            }

        }
        $finalUrl = "shop/".$data['url']."?".$manufacturerUrl.$pcatUrl.$catUrl;
        return redirect::to($finalUrl);
    }



    public function search(Request $request){

        if($request->isMethod('post')){
            $this->validate($request,[
                'product'=>'required|min:4',

            ]);

            $data = $request->all();
            // echo "<pre>"; print_r($data); die;
            // $bundles=Product::where('status','bundle')->get();
            $search_product = $data['product'];
            $products=Product::where('product_keyword','like','%'.$search_product.'%')->orwhere('product_title','like','%'.$search_product.'%')->where('status','product')->get();

            return view('frontEnd.shop.index',compact('products'));
        }

    }


     
     //Product Details Function
     public function detailproduct($id){
        $detail_product=Product::findOrFail($id);
        $imagesGalleries=ImageGallery::where('products_id',$id)->get();
        $totalStock=ProductAtrr::where('products_id',$id)->sum('stock');
        //for recommendation product//
        $relateProducts=Product::where([['id','!=',$id],['cat_id',$detail_product->cat_id]])->get();
        ////retriving the reviews///
        $reviews = ProductReview::where('product_id', $id)->get();
    
        return view('frontEnd.shop.productdetail',compact('detail_product','imagesGalleries','totalStock','relateProducts','reviews'));
    }



  //product attrs
  public function getAttrs(Request $request){
    $all_attrs=$request->all();
    //print_r($all_attrs);die();
    $attr=explode('-',$all_attrs['size']);
    //echo $attr[0].' <=> '. $attr[1];
    $result_select=ProductAtrr::where(['products_id'=>$attr[0],'size'=>$attr[1]])->first();
    echo $result_select->price."#".$result_select->stock."#".$result_select->color;
}








}
