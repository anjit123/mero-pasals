<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductReview;
use Illuminate\Support\Facades\Auth;

class ProductReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $menu_active = 14;
        $reviews = ProductReview::all();
        return view('backEnd.review.index',compact('menu_active','reviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
      
        
        if(!Auth::check()){
            $this->validate($request,[
        
                'product_id'=>'required',
                'name' => 'required',
                'email'=> 'string|email',
                'message'=>'required',
                'rating'=>'required',
                ]);
            return redirect()->back()->with('review','Please login to reviews or fill the details');
        }else{

            $this->validate($request,[
        
                'product_id'=>'required',
                'name' => 'required',
                'email'=> 'string|email',
                'message'=>'required',
                'rating'=>'required',
                ]);
         
                $data=$request->all();
                $user_email = Auth::user()->email;

                if($data['email']== $user_email){

                 ProductReview::create([
                    'product_id'=> $data['product_id'],
                    'name'=> $data['name'],
                    'email'=> $data['email'],
                    'message'=> $data['message'],
                    'rating'=> $data['rating'],
                 ]);
                 return redirect()->back()->with('review','Thankyou for rating the products');
                    

                }
                else{
                    return redirect()->back()->with('review','Please use authenticated email');

                }
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

   
    public function show($id)
    {
        $menu_active = 14;
        $sentiment = ProductReview::findOrFail($id);
        return view('backEnd.sentiment.sentiment',compact('menu_active','sentiment'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //

        $delete=ProductReview::findOrFail($id);
        $delete->delete();
        return redirect()->route('customer-reviews.index')->with('message',' Reviews Deleted Successfully');
    }
}
