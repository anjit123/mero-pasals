<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\Order;
use App\OrderProduct;
use App\User;
use App\Payment;
use App\ProductAtrr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;

use Carbon\Carbon;
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function orderReview()
    {
        $session_id=Session::get('session_id');
        $cart_datas=Cart::where('session_id',$session_id)->get();
        $total_price=0;
        $quantity=0;
        foreach ($cart_datas as $cart_data){
            $total_price+=$cart_data->price*$cart_data->quantity;
            $quantity= $cart_data->quantity;
        }
        $shipping_address=DB::table('delivery_address')->where('users_id',Auth::id())->first();
        return view('frontEnd.checkout.order_review',compact('shipping_address','cart_datas','total_price'));

    }

    public function order(Request $request){
        
        $input_data=$request->all();
        $users_id = Auth::user()->id;
        $user_email = Auth::user()->email;
        $payment_method=$input_data['payment_method'];
        Order::create($input_data);

        $order_id = DB::getPdo()->lastInsertId();
        
        $cartProducts = DB::table('carts')->where(['user_email'=>$user_email])->get();
        foreach($cartProducts as $pro){
            $cartPro = new OrderProduct;
            $cartPro->order_id = $order_id;
            $cartPro->users_id = $users_id;
            $cartPro->product_id = $pro->products_id;
            $cartPro->product_code = $pro->product_code;
            $cartPro->product_title = $pro->product_name;
            $cartPro->product_size = $pro->size;
            $cartPro->product_color = $pro->color;
            $cartPro->product_price = $pro->price;
            $cartPro->product_qty = $pro->quantity;
            $cartPro->save();

            //Reduce Stock Script starts
            $getProductStock = ProductAtrr::where('sku',$pro->product_code)->first();
            $getProductStock->stock;
            $pro->quantity;

            $newStock = $getProductStock->stock - $pro->quantity;
            if($newStock<0){
                $newStock = 0;
            }
            ProductAtrr::where('sku',$pro->product_code)->update(['stock'=>$newStock]);
         
        }

        Session::put('order_id',$order_id);
        Session::put('grand_total',$input_data['grand_total']);
       
        
        if($payment_method=="COD"){
            $user_id = Auth::user()->id;

            $productDetails = Order::with('orderss')->where('id',$order_id)->first();
            $shippingDetails=DB::table('delivery_address')->where('users_id',Auth::id())->first();
        
            $userDetails = User::where('id',$user_id)->first();
            $email = $user_email;
            $messageData = [
                'email'=> $email,
                'name'=> $shippingDetails->name,
                'order_id'=>$order_id,
                'productDetails'=>$productDetails,
                'userDetails'=>$userDetails

            ];
            //sending order mail//
            Mail::send('frontEnd.orders.order',$messageData, function($message) use($email){
                $message->to($email)->subject('Order Placed - mero-Pasal');
            });
            return redirect('/cod');


        }elseif($payment_method=="Paypal"){

            $user_id = Auth::user()->id;

            $productDetails = Order::with('orderss')->where('id',$order_id)->first();
        $shippingDetails=DB::table('delivery_address')->where('users_id',Auth::id())->first();
        
            $userDetails = User::where('id',$user_id)->first();
            $email = $user_email;
            $messageData = [
                'email'=> $email,
                'name'=> $shippingDetails->name,
                'order_id'=>$order_id,
                'productDetails'=>$productDetails,
                'userDetails'=>$userDetails

            ];
            //sending order mail to the customer///
            Mail::send('frontEnd.orders.order',$messageData, function($message) use($email){
                $message->to($email)->subject('Order Placed - mero-Pasal');
            });
            return redirect('/paypal');

        }else{
                ///Integrating the eSewa
            return redirect('/pay');
        }
       
     }
    public function cod(){
       
        $user_id = Auth::user()->id;
        $user_email = Auth::user()->email;
        //delete the carts items
        DB::table('carts')->where(['user_email'=>$user_email])->delete();
        $accounts=User::where('id',$user_id)->first();
        $orders=Order::with('orderss')->where('users_id',$user_id)->get();
        return view('users.account',compact('accounts','orders'));
    }


    public function paypal(Request $request){

        $user_id = Auth::user()->id;
        $user_email = Auth::user()->email;
        DB::table('carts')->where(['user_email'=>$user_email])->delete();
        $orders=Order::with('orderss')->where('users_id',$user_id)->get();
        return view('frontEnd.payment.paypal',compact('orders'));
    }

    public function pay(Request $request, Payment $payment){
        $user_id = Auth::user()->id;
        $orders=Order::with('orderss')->where('users_id',$user_id)->get();
        $cart = Cart::get();
        $data= $request->all();
          $data = json_decode(json_encode($data));
          echo "<pre>"; print_r($data); die;
        // $data= $orders[0]->grand_total;
        $data['PAYMENT_AMOUNT'] = $data['total'];  
        $payment->setData($data);
        return view('frontEnd.payment.pay',compact('orders',['data'=>$data]));
    }



    public function userOrderDetails($order_id){
        $user_id = Auth::user()->id;
        $accounts=User::where('id',$user_id)->first();

        $orderDetails = Order::with('orderss')->where('id',$order_id)->first();
        
        return view('frontEnd.orders.user_order_details',compact('accounts','orderDetails'));
    }

    public function viewOrderInvoice($order_id){
        $user_id = Auth::user()->id;
        $accounts=User::where('id',$user_id)->first();

        $orderDetails = Order::with('orderss')->where('id',$order_id)->first();

        $orderDetails = json_decode(json_encode($orderDetails));
        // echo "<pre>"; print_r($orderDetails);die;
        return view('backEnd.orders.order_invoice',compact('accounts','orderDetails'));
    }



    public function viewOrders(){

        $orders = Order::with('orderss')->orderBy('id','Desc')->get();
        return view('backEnd.orders.view_orders',compact('orders'));
    }
   

    public function thanksPaypal(){
        return view('frontEnd.payment.thanks_paypal');
    }
  
    public function cancelPaypal(){
        return view('frontEnd.payment.cancel_paypal');

    }
    
    public function edit($id)
    {
        //
        $menu_active = 0;

        $order = Order::with('orderss')->where('id',$id)->first();
        

        return view('backEnd.orders.edit',compact('menu_active','order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = $request->all();
        // echo "<pre>"; print_r($data); die;
        Order::where('id',$id)->update(['order_status' => $data['order_status']]);
        return redirect()->back()->with('message','Order Status has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $order = Order::findOrFail($id);
        $order->delete();
        return redirect()->back()->with('message','Order Deleted Successfully');

    }




    public function userOrder(){
        $menu_active=11;
        
        $orders=Order::with('orderss')->orderBy('id','Desc')->paginate(5);
        return view('backEnd.orders.view_orders',compact('menu_active','orders'));


    }



    public function searchOrder(Request $request){

        $menu_active=2;
      

        if($request->isMethod('post')){
            $data = $request->all();
            // echo "<pre>"; print_r($data); die;
            // $bundles=Product::where('status','bundle')->get();
            $search_order = $data['order'];
            $orders=Order::where('payment_method','like','%'.$search_order.'%')->orwhere('order_status','like','%'.$search_order.'%');

            return view('backEnd.orders.view_orders',compact('menu_active'))->with('orders',$orders->paginate(5));
        }

    }


    // public function viewOrderCharts(){

    //     $menu_active=11;

    //     $current_month_orders = Order::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->count();
    //     $last_month_orders = Order::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->subMonth(1))->count();
    //     $last_to_last_month_orders = Order::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->subMonth(2))->count();

    //     return view('backEnd.orders.view_orders_chart')->with(compact('current_month_orders','last_month_orders','last_to_last_month_orders','menu_active'));

    // }
}
