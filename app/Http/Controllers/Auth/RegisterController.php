<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;

use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Image;
use Illuminate\Support\Facades\Session;
use Illimuinate\Support\Facades\Mail;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest',['verify'=>true]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'min:5','max:50'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'country'=>'required|string|max:255',
            'city'=>'required|string|max:255',
            'address'=>'required|string|max:255',
            'contact'=>'required|min:11|numeric',
            'image' => ['required','image'],
        ]);

     
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
       
        $imagePath = request('image')->store('uploads','public');
        $image = Image::make(public_path("storage/{$imagePath}"))->fit(500,500);
        $image->save();
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'country' => $data['country'],
            'city' => $data['city'],
            'address' => $data['address'],
            'contact' => $data['contact'],
            'image' => $imagePath,

        ]);

        // Send Register Email
        // $email= $data['email'];
        // $messageData =['email'=>$data['email'], 'name'=>$dat['name'],'code'=>base64_encode($data['email'])];
        // Mail::send('users.confirmation_mail', $messageData,function($message) use($email){
        //     $message->to($email)->subject('Registration with mero-Pasal');
        // });
        // return redirect()->back()->with('message','Please confirm your email to activate your account');
        // if(Auth::attempt(['email'=>$data['email'],'password'=>$data['password']])){
        //     Session::put('frontSession',$data['email']);

        //     if(!empty(Session::get('session_id'))){
        //         $session_id = Session::get('session_id');
        //         DB::table('')
        //     }
        // }
            return redirect('/')->with('message','Register Successfully');
        
        
    }
}
