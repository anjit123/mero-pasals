<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
// use Illuminate\Support\Facades\Auth;

// use Laravel\Socialite\Facades\Socialite;
use App\User;
use Socialite;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        // return Socialite::with('facebook')->redirect();
        return Socialite::with($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        // $userSocial = Socialite::driver('facebook')->user();

        // $findUser = User::where('email',$userSocial->email)->first();

        // if($findUser){
        //     // Auth::login($findUser);
        //     return redirect('/u/login');

        // }else{
        //     $user = new User;

        //     $user->name = $userSocial->name;
        //     $user->email = $userSocial->email;
        //     $user->password = bcrypt(12345678);
        //     $user->country = 'Nepal';
        //     $user->city = 'Kapan';
        //     $user->address = 'Kathmandu';
        //     $user->contact = 9810220552;
        //     $user->image = 'image';
        //     $user->save();
        //     // Auth::login($findUser);
        //     // return 'new user';
        //     return redirect('/u/login');


        // }
      
        // return $user->name;

        // return Socialite::driver('facebook')
        // ->with(['hd' => 'example.com'])
        // ->redirect();

        $user = Socialite::driver($provider)->user();
        // return $user->token;
        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        return redirect($this->redirectTo);
        
    }


    public function findOrCreateUser($user, $provider){
        $authUser = User::where('provider_id', $user->id)->first();
        if($authUser){
            return $authUser;
        }
        return User::create([
            'name' => $user->name,
            'email'=> $user->email,
            'password' => bcrypt('9810220552'),
            'country' => 'Nepal',
            'city' => 'Kathmandu',
            'address' => 'Kapan',
            'contact' => 9810220552,
            'image' => 'Image Error',
            'provider'=> strtoupper($provider),
            'provider_id'=> $user->id
        ]);
    }
}
