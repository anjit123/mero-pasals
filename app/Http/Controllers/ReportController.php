<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $menu_active = 13;
        return view('backEnd.reports.index',compact('menu_active'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function show(Request $request)
    {
        //
        $menu_active = 13;

        $request->validate([
            'dateStart' => 'required',
            'dateEnd' => 'required',
        ]);
        $dateStart = date("Y-m-d H:i:s", strtotime($request->dateStart.' 00:00:00'));
        $dateEnd = date("Y-m-d H:i:s", strtotime($request->dateEnd.' 23:59:59'));

        $orders = Order::with('orderss')->whereBetween('updated_at', [$dateStart, $dateEnd]);

        return view('backEnd.reports.showReport',compact('menu_active'))->with('dateStart', date("m/d/Y H:i:s", strtotime($request->dateStart.' 00:00:00')))->with('dateEnd',
            date("m/d/Y H:i:s", strtotime($request->dateEnd.' 23:59:59')))->with('totalOrder',$orders->sum('grand_total'))->with('orders',$orders->paginate(10));
        


    }



    public function viewOrderCharts(){

        $menu_active=13;

        $current_month_orders = Order::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->count();
        $last_month_orders = Order::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->subMonth(1))->count();
        $last_to_last_month_orders = Order::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->subMonth(2))->count();

        return view('backEnd.orders.view_orders_chart')->with(compact('current_month_orders','last_month_orders','last_to_last_month_orders','menu_active'));

    }



    public function checkReport(Request $request){
        if ($request->status){
            $menu_active=13;
            // $date = date('d-m-Y',strtotime($request->date));
            // $date = date("Y-m-d H:i:s", strtotime($request->date.' 00:00:00'));

            // $dateEnd = date("Y-m-d H:i:s", strtotime($request->date.' 23:59:59'));


            // $orders = DB::table('orders')->where('updated_at',$date)->get();
            // $orders = Order::with('orderss')->whereBetween('created_at', [$date, $dateEnd]);

            // $sum =  DB::table('orders')->where('updated_at',$date)->sum('grand_total');
            // return view('backEnd.reports.results',compact('menu_active','orders','sum','date'));
            $status = $request->status;
            $orders = Order::with('orderss')->where('order_status', $status)->paginate(10);
            return view('backEnd.reports.results',compact('menu_active'))->with('grand_total',$orders->sum('grand_total'))->with('orders',$orders);
        
        }
        else if($request->payment){
            $menu_active=13;

            $payment = $request->payment;
            $orders = Order::with('orderss')->where('payment_method', $payment)->paginate(10);
            return view('backEnd.reports.select_payment',compact('menu_active'))->with('grand_total',$orders->sum('grand_total'))->with('orders',$orders);
        

        }
        else if($request->month && $request->year){

        }
        else{

        }
    }









    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
