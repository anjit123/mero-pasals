<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory;
use Intervention\Image\Facades\Image;
class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $menu_active=0;
       
        $p_cats = ProductCategory::all();
        return view('backEnd.productcategories.index',compact('menu_active','p_cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $menu_active=2;

        return view('backEnd.productcategories.create',compact('menu_active'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = request()->validate([
            'p_cat_name'=>'required|max:255|unique:product_categories,p_cat_name',
            'p_cat_top'=>'required',
            'p_cat_image' => ['required','image'],
        ]);
        $p_cat_imagePath = request('p_cat_image')->store('PCategoryUploads','public');
        $p_cat_image = Image::make(public_path("storage/{$p_cat_imagePath}"));
        $p_cat_image->save();
        // $data=$request->all();

        ProductCategory::create([
            'p_cat_name' => $data['p_cat_name'],
            'p_cat_top' => $data['p_cat_top'],
            'p_cat_image' => $p_cat_imagePath,


        ]);
        return redirect()->route('productcategory.index')->with('message','Product Category Added Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $menu_active=0;
       
        $edit_p_cat=ProductCategory::findOrFail($id);
        return view('backEnd.productcategories.edit',compact('edit_p_cat','menu_active'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $menu_active=0;

        $update_p_cat = ProductCategory::findOrFail($id);

        $this->validate($request,[
        
            'p_cat_name'=>'required|max:255',
            'p_cat_top'=>'required',
            'p_cat_image' => ['required','image'],
        ]);

        // dd($request->all());

        $p_cat_imagePath = request('p_cat_image')->store('PCategoryUploads','public');
        $p_cat_image = Image::make(public_path("storage/{$p_cat_imagePath}"));
        $p_cat_image->save();
        $data=$request->all();

        $update_p_cat->update([
            'p_cat_name' => $data['p_cat_name'],
            'p_cat_top' => $data['p_cat_top'],
            'p_cat_image' => $p_cat_imagePath,
        ]);
        return redirect()->route('productcategory.index')->with('message','Product Category Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete=ProductCategory::findOrFail($id);
        $delete->delete();
        return redirect()->route('productcategory.index')->with('message','Product Category Deleted Successfully');
    }
}
