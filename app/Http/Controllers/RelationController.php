<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductBundleRelation;
class RelationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $menu_active=0;
        $products=Product::where('status','product')->get();
        $bundles=Product::where('status','bundle')->get();
        $p_b_rels=ProductBundleRelation::orderBy('created_at','asc')->get();
        return view('backEnd.relation.index',compact('menu_active','p_b_rels','products','bundles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $menu_active=2;
        $products=Product::where('status','product')->pluck('product_title','id')->all();
        $bundles=Product::where('status','bundle')->pluck('product_title','id')->all();
        return view('backEnd.relation.create',compact('menu_active','products','bundles'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[

            'rel_title'=>'required',

        ]);
        $formInput=$request->all();

        ProductBundleRelation::create($formInput);
        return redirect()->route('relation.index')->with('message','Add Products Successfully!');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $menu_active=0;
        $products=Product::where('status','product')->pluck('product_title','id')->all();
        $bundles=Product::where('status','bundle')->pluck('product_title','id')->all();
        $edit_relation=ProductBundleRelation::findOrFail($id);
        $edit_products=Product::where('status','product')->findOrFail($edit_relation->product_id);
        $edit_bundles=Product::where('status','bundle')->findOrFail($edit_relation->bundle_id);

        return view('backEnd.relation.edit',compact('edit_relation','menu_active','products','bundles','edit_products','edit_bundles'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $update_relation=ProductBundleRelation::findOrFail($id);
        $this->validate($request,[

            'rel_title'=>'required',

        ]);
        $formInput=$request->all();

        $update_relation->update($formInput);

        return redirect()->route('relation.index')->with('message','Add Relations Successfully!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         
        $delete=ProductBundleRelation::findOrFail($id);
        $delete->delete();
        return redirect()->route('relation.index')->with('message','Delete Success!');
    }
}
