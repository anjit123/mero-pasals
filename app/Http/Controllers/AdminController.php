<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

use Image;
use App\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    // $this->middleware(['auth'=>'verified']);


   
    public function index(User $id)
    {
        //
        $user_id = Auth::user()->id;
        $accounts=User::where('id',$user_id)->first();
        $orders=Order::with('orderss')->paginate(5);
        $menu_active=1;
        return view('backEnd.index',compact('menu_active','accounts','orders'));

       
    }

    

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function userOrder(){
    //     $menu_active=11;
        
    //     $orders=Order::with('orderss')->paginate(5);
    //     return view('backEnd.orders.view_orders',compact('menu_active','orders'));


    // }



    public function create()
    {
        //
        $menu_active=0;

        return view('backEnd.profile.create_user',compact('menu_active'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     
    public function store(Request $request)
    {
    
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required|string|email|unique:users,email',
            

        ]);

        $imagePath = request('image')->store('uploads','public');
        $image = Image::make(public_path("storage/{$imagePath}"))->fit(500,500);
        $image->save();
        $data=$request->all();
        
        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password'=> Hash::make($data['password']),
            'country' => $data['country'],
            'city' => $data['city'],
            'address' => $data['address'],
            'contact' => $data['contact'],
            'image' => $imagePath,
            'user_job' => $data['user_job'],
            'about_user' => $data['about_user'],

        ]);
        return redirect()->route('createuser.create')->with('message','Successfully Create User');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $menu_active=9;
        $users = User::all();
        

        return view('backEnd.profile.show_user',compact('menu_active','users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $id)
    {
        //
        $menu_active=0;
        $edit_user= User::where('id',Auth::id())->first();
        return view('backEnd.profile.edit',compact('menu_active','edit_user'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $id)
    {
        //
        // dd(request()->all());
        $menu_active=1;

        $accounts= User::where('id',Auth::id())->first();


        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'country'=>'required',
            'city'=>'required',
            'address'=>'required',
            'contact'=>'required',
            'image' => ['required','image'],
            'user_job'=>'required',
            'about_user'=>'required',


        ]);
        // dd(request()->all());
        $imagePath = request('image')->store('uploads','public');
        $image = Image::make(public_path("storage/{$imagePath}"))->fit(500,500);
        $image->save();

        // dd(request()->all());
        $input_data=$request->all();
        
        // DB::table('users')->where('id',$id)->update([
        // User::where('id',$id)->update([
            $accounts->update([
            'name'=>$input_data['name'],
            'email'=>$input_data['email'],
            'country'=>$input_data['country'],
            'city'=>$input_data['city'],
            'address'=>$input_data['address'],
            'contact'=>$input_data['contact'],
            'image'=>$imagePath,
            'user_job'=>$input_data['user_job'],
            'about_user'=>$input_data['about_user'],


        ]);
        // return view('backEnd.index',compact('menu_active'));
        return redirect()->back()->with('message','Delete Success!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $menu_active=9;
        
        $delete=User::findOrFail($id);
        $delete->delete();
        // return view('backEnd.index',compact('menu_active'));
        return redirect()->back()->with('message','Delete Success!');


        
    }


    public function showallusers(){

        //
        $menu_active=9;
        $users = User::all();
        

        return view('backEnd.profile.show_user',compact('menu_active','users'));
    }



    public function userSearch(Request $request){

        $menu_active=9;
        if($request->isMethod('post')){
            $data = $request->all();
            // echo "<pre>"; print_r($data); die;
            // $bundles=Product::where('status','bundle')->get();
            $search_user = $data['user'];
            $users=User::where('name','like','%'.$search_user.'%')->get();

            return view('backEnd.profile.show_user',compact('menu_active','users'));
        }

    }


    public function viewUserCharts(){
        $menu_active=9;

        $current_month_users = User::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->count();
        $last_month_users = User::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->subMonth(1))->count();
        $last_to_last_month_users = User::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->subMonth(2))->count();

        return view('backEnd.profile.view_users_chart')->with(compact('current_month_users','last_month_users','last_to_last_month_users','menu_active'));

    }



    
}
