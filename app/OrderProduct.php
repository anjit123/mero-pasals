<?php

namespace App;
use App\Order;
use App\User;
use App\Product;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    //
    protected $table='order_product';
    protected $primaryKey='id';
    protected $fillable=['order_id','users_id', 'product_id','product_code','product_title','product_size','product_color','product_price','product_qty'];


    // public function orders(){
    //     return $this->hasMany('App\Order');
    // }

    public function users(){
        return $this->belongsTo(User::class,'users_id','id');
    }
    
    public function product(){
        return $this->belongsTo(Product::class,'product_id','id');
    }

    public function orders(){
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }
  
   

}
