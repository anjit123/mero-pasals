<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    //
    protected $table='manufacturers';
    protected $primaryKey='id';
    protected $fillable=['manufacturer_name', 'manufacturer_top','manufacturer_image'];


    public function products(){

        return $this->hasMany(Product::class);
    }
}
