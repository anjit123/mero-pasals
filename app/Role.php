<?php

namespace App;
use App\User;
use App\RoleUser;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //

    protected $table='roles';
    protected $primaryKey='id';
    protected $fillable=['name'];

    // public function users(){
    //     return $this->belongsToMany('App\User');
    // }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
