<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
class ProductAtrr extends Model
{
    //
    protected $table='product_atrrs';
    protected $primaryKey='id';
    protected $fillable=['products_id','sku','size','price','color','stock'];


    public function products(){
        return $this->belongsTo(Product::class,'products_id','id');

    }
}

