<?php

namespace App;
use App\Role;
use App\User;
use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    //
    protected $table='role_users';
    protected $primaryKey='id';
    protected $fillable=['name','role_id','user_id'];

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');

    }

    public function role(){
        return $this->belongsTo(Role::class,'role_id','id');

    }
    

}
