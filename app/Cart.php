<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    //
    protected $table='carts';
    protected $primaryKey='id';
    protected $fillable=['products_id','product_name','product_code','size','color','price','quantity','user_email','session_id'];

}
