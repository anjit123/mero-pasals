<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Cart;
use App\OrderProduct;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    //
    protected $table='orders';
    protected $primaryKey='id';
    protected $fillable=['users_id',
        'users_email','name','address','city','state','pincode','country','contact','shipping_charges','coupon_code','coupon_amount',
        'order_status','payment_method','grand_total'];


        // public function user(){

        //     return $this->belongsTo(User::Class);
        // }
        public function orderss(){
            return $this->hasMany('App\OrderProduct','order_id');
        }



        public function ordersproduct(){
            return $this->belongsTo('App\OrderProduct');
        }

        public function orders(){
            return $this->belongsTo(Order::class, 'order_id', 'id');
        }

        public static function getOrderDetails($order_id){
            $getOrderDetails = Order::where('id',$order_id)->first();
            return $getOrderDetails;
              
        }

        public static function countOrders(){
            $countOrders = DB::table('orders')->count();
            return $countOrders;
        }

       

        protected $dates = [
            'created_at'
        ];

        
        public static function orderSum(){

            $orderSum = DB::table('orders')->sum('grand_total');
    
            return $orderSum;
        }
        
       

        
}

