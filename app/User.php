<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Role;
use App\RoleUser;
use App\Order;
use App\Crypt;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $table='users';
    protected $primaryKey='id';
    protected $fillable = [
        'name', 'email', 'password','admin','country','city','address','contact','image','user_job','about_user','provider','provider_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    //user model
    // public function getEmailAttribute($email)
    // {
    //     return Crypt::decrypt($email);
    // }

    // public function setEmailAttribute($email)
    // {
    //     $this->attributes['email'] = Crypt::encrypt($email);
    // }


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin(){
        return ($this->admin == 1);
    }

    public function role(){
        return $this->belongsToMany(Role::class,'role_users');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
    


    public static function userCount(){
        $userCount = DB::table('users')->count();
        return $userCount;
    }

   
}
