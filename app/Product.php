<?php

namespace App;
// use category;
use Manufacture;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
// use ProductCategory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //


    protected $table='products';
    protected $primaryKey='id';
    protected $fillable=['manufacturer_id','p_cat_id','cat_id','product_title','product_color','image','product_price','psale_price','product_keyword','product_desc','product_feature','product_label','status'];



    public function category()
    {
       
        return $this->belongsTo(Category::class,'cat_id','id');

    }

    public function manufacturer(){
        // $this-belongsTo(Manufacturer::class);
        return $this->belongsTo(Manufacturer::class,'manufacturer_id','id');

    }

    public function productcategory()
    {
        return $this->belongsTo(ProductCategory::class,'p_cat_id','id');

    }

    public function attributes(){
        return $this->hasMany(ProductAtrr::class,'products_id','id');
    }
    public function orders(){
        // $this-belongsTo(Manufacturer::class);
        return $this->belongsTo(Order::class,'id','id');

    }
    public function scopeSearch($query , $s){
        return $query->where('product_title','like','%'.$s.'%')->orWhere('product_desc','like','%'.$s.'%');

    }
    public function product(){
        return $this->belongsTo(Product::class,'product_id','id');
    }

    public static function cartCount(){
        if(Auth::check()){
            //user is logged in; we will use Auth
            $user_email = Auth::user()->email;
            $cartCount = DB::table('carts')->where('user_email',$user_email)->sum('quantity');
        }else{
            //user is not logged in. we will use session
            $session_id = Session::get('session_id');
            $cartCount = DB::table('carts')->where('session_id',$session_id)->sum('quantity');

        }
        return $cartCount;

    }


    public static function productCount(){

        $productCount = DB::table('products')->count();

        return $productCount;
    }

    
}
