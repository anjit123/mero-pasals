<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('manufacturer_id');
            $table->integer('p_cat_id');
            $table->integer('cat_id');
            $table->string('product_title');
            $table->string('product_color');
            $table->string('image');
            $table->float('product_price');
            $table->float('psale_price');
            $table->string('product_keyword');
            $table->text('product_desc');
            $table->text('product_feature');
            $table->string('product_label');
            $table->string('status')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
