<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::truncate();
        
        $adminRole = Role::where('name', 'admin')->first();

        $admin = User::create([
            'name'=> 'Admin',
            'email' =>'admin@gmail.com',
            'password'=> bcrypt('admin'),
            'country'=>NULL,
            'city'=>NULL,
            'address'=>NULL,
            'contact'=>NULL,
            'image'=>NULL

        ]);

        $admin->roles()->attach($adminRole);
    }
}
