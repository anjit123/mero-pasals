<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/////Start of Customer Location //////
//Front End homepage 
Route::get('/', 'IndexController@index')->name('home_page');
///About us page///
Route::get('/about', 'IndexController@about')->name('about');
//Service page
Route::get('/service', 'IndexController@service')->name('service_page');


////->middleware('verified')

////User Login and Register///
Auth::routes(['verify' => true]);
Route::get('/u/register', 'UsersController@create')->name('user_register');
Route::post('/u_register', 'UsersController@store',['verify' => true])->name('u_register')->middleware('verified');

Route::get('/u/login', 'UsersController@Index')->name('user_login');
Route::post('/u_login', 'UsersController@login',['verify' => true])->name('u_login');
Route::get('/u/logout', 'UsersController@logout')->name('user_logout');

// Route::match(['get','post'],'forget-password','UsersController@forgotPassword')->name('password.request');
//social login / facebook socialite
Route::get('login/facebook', 'Auth\LoginController@redirectToProvider',['verify' => true]);
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');

//social login with google socialite
Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

//Shop Area////////////
Route::resource('/shop', 'ShopController');
Route::get('/product-detail/{id}','ShopController@detailproduct');
Route::get('/get-product-attr','ShopController@getAttrs');

///Search Product Area///
Route::post('/search','ShopController@search')->name('search');

/////Product Filter of Shop Sidebar////
Route::match(['get','post'],'/products-filter','ShopController@filterproduct');

//Get product Attribute price /////
Route::get('/get-product-price','ProductController@getProductPrice');
Route::get('/get-product-qty','ProductController@getProductQty');


//product review///
Route::post('/productReview','ProductReviewController@store')->name('productReview');


/////Cart Area /////
Route::post('/addToCart','CartController@addToCart')->name('addToCart');
Route::get('/viewcart','CartController@index');
Route::get('/resetCart','CartController@resetCart')->name('resetCart');
Route::get('/cart/update-quantity/{id}/{quantity}','CartController@updateQuantity');
Route::get('/cart/deleteItem/{id}','CartController@deleteItem');

//////Apply Coupon /////
Route::post('/cart/apply-coupon','CartController@applyCoupon');


Route::group(['middleware'=>['frontLogin']],function (){

    ////Customer Account Area //////
    Route::get('/account', 'UsersController@account')->name('user_account');
    Route::get('/update-profile', 'UsersController@edit')->name('update_profile');
    Route::post('/update-profile/{id}', 'UsersController@updateprofile')->name('update');
    Route::get('/u/update-password/', 'UsersController@showpassword')->name('change_p');
    Route::post('/update-password/{id}', 'UsersController@updatepassword')->name('update_p');

    //Wish list Area ////
    Route::get('/my-wish-list/', 'CartController@wishList')->name('wish_list');
    Route::get('/wish-list/delete-product/{id}','CartController@deleteWishListProducts');
    
    ///Contact Us Page ///
    Route::resource('/contact','ContactController');
    //Order Area///
    Route::get('/pay_offline', 'UsersController@payoffline')->name('pay_offline');
    Route::get('/check-out','CheckOutController@index');
    Route::post('/submit-checkout','CheckOutController@submitcheckout');
    Route::get('/order-review','OrderController@orderReview');
    Route::post('/submit-order','OrderController@order');
    Route::get('/cod','OrderController@cod');
    Route::get('/paypal','OrderController@paypal');
    Route::get('/pay','OrderController@pay');
    //paypal thanks page
    Route::get('/paypal/thanks','OrderController@thanksPaypal');
    //paypal cancel page
    Route::get('/paypal/cancel','OrderController@cancelPaypal');
    ////User Ordered Product Page
    Route::get('/orders/{id}','OrderController@userOrderDetails');
    

});


////End of Customer Location //////////


///////* Admin Location *///////
Auth::routes(['u_register'=>false,'verify'=>true]);
// Route::get('/home', 'HomeController@index')->name('home');
// Route::resource('admin/product','ProductController');

Route::group(['prefix'=>'admin','middleware'=>['auth','verified','checkAdmin']],function (){

    // Dashboard Area////
    Route::get('/', 'AdminController@index')->name('admin_home');
    Route::resource('/createuser', 'AdminController');
    Route::get('/showuser/', 'AdminController@showallusers')->name('show_user');
    Route::post('/showuser','AdminController@userSearch')->name('user.search');
    Route::get('/profile_update', 'AdminController@edit')->name('profile_update');
    Route::get('/order', 'OrderController@userOrder')->name('order');
    Route::post('/order', 'OrderController@searchOrder')->name('admin.search.order');

    //Admin Order Charts Route
    Route::get('/view-user-charts','AdminController@viewUserCharts');
    Route::get('delete-user/{id}','AdminController@destroy');

    ///Roles Area//////
    Route::resource('/role','RoleController');
    Route::resource('/roleuser','RoleUserController');

    
    
    
    //category Area
    Route::resource('/category','CategoryController');
    Route::get('delete-category/{id}','CategoryController@destroy');
    Route::get('/check_category_name','CategoryController@checkCateName');
    
    ///slider Area
    Route::resource('/slide','SlideController');
    Route::get('delete-slide/{id}','SlideController@destroy');
    Route::get('/check_slide_name','SlideController@checkSlideName');
    
    //Manufacturer or brands Area////
    Route::resource('/manufacturer','ManufacturerController');
    Route::get('delete-brand/{id}','ManufacturerController@destroy');
    
    //Product Category Area///
    Route::resource('/productcategory','ProductCategoryController');
    Route::get('delete-productcategory/{id}','ProductCategoryController@destroy');
    
    ///Box Section Area/////
    Route::resource('/box','BoxSectionController');
    Route::get('delete-box/{id}','BoxSectionController@destroy');
    
    ///Products Area///
    Route::resource('/product','ProductController');
    Route::get('delete-product/{id}','ProductController@destroy');
    Route::post('/products','ProductController@searchProducts')->name('product.search');
    
    
    ///Bundles Area///
    Route::get('/bundle','BundleController@create_bundle');
    Route::get('/bundles','BundleController@index');
    
    
    /// Product Attribute Area/////
    Route::resource('/product_attr','ProductAtrrController');
    Route::get('delete-attribute/{id}','ProductAtrrController@deleteAttr');
    
    /// Product Images Gallery///
    Route::resource('/image-gallery','ImagesController');
    Route::get('delete-imageGallery/{id}','ImagesController@destroy');
    
    ////Relation Area///
    Route::resource('/relation','RelationController');
    Route::get('delete-relation/{id}','RelationController@destroy');
    
    ///Coupon Area ////
    Route::resource('/coupon','CouponController');
    Route::get('delete-coupon/{id}','CouponController@destroy');
    
    ///View Orders Area////
    Route::resource('/orders','OrderController');
    Route::get('/admin/view-orders', 'OrderController@viewOrders');
    Route::get('/orders-invoice/{id}','OrderController@viewOrderInvoice')->name('order.invoice');
    Route::get('delete-order/{id}','OrderController@destroy');
    
    
    ///////Report Area /////// 
    Route::resource('/report','ReportController');
    Route::get('admin/reprot/show','ReportController@show');
    Route::get('/view-order-charts','ReportController@viewOrderCharts');
    Route::post('/report/check','ReportController@checkReport');

    ///Review Area/////
    Route::resource('/customer-reviews','ProductReviewController');
    Route::get('delete-customer-reviews/{id}','ProductReviewController@destroy');











    




});

