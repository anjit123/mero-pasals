



		// Instantiate EasyZoom instances
		var $easyzoom = $('.easyzoom').easyZoom();

		// Setup thumbnails example
		var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

		$('.thumbnails').on('click', 'a', function(e) {
			var $this = $(this);

			e.preventDefault();

			// Use EasyZoom's `swap` method
			api1.swap($this.data('standard'), $this.attr('href'));
		});

		// Setup toggles example
		var api2 = $easyzoom.filter('.easyzoom--with-toggle').data('easyZoom');

		$('.toggle').on('click', function() {
			var $this = $(this);

			if ($this.data("active") === true) {
				$this.text("Switch on").data("active", false);
				api2.teardown();
			} else {
				$this.text("Switch off").data("active", true);
				api2._init();
			}
        });
        



///////////Size of Product

$("#idSize").change(function () {
    var SizeAttr=$(this).val();
    if(SizeAttr!=""){
        $.ajax({
            type:'get',
            url:'/get-product-attr',
            data:{size:SizeAttr},
            success:function(resp){
                var arr=resp.split("#");
                $("#dynamic_price").html('US $'+arr[0]);
                $("#dynamicPriceInput").val(arr[0]);
                if(arr[1]==0){
                    $("#buttonAddToCart").hide();
                    $("#availableStock").text("Out Of Stock");
                    $("#inputStock").val(0);
                }else{
                    $("#buttonAddToCart").show();
                    $("#availableStock").text("In Stock");
                    $("#inputStock").val(arr[1]);
                }
            },error:function () {
                alert("Error Select Size");
            }
        });
    }
});

///////////// Thumnail Image
$(".changeImage").click(function () {
    newImage=$(this).attr('src');
    $("#dynamicImage").attr('src',newImage);
});


	

